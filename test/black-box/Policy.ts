import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policies', () => {

    const keys = [
        'tenantId', 'policyId', 'label', 'transactionType', 'holdKnownFraudster', 'systemDefault', 'status'
    ];

    const testData = {
        tenantId: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        label: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        holdKnownFraudster: undefined, // TODO: Add test value
        systemDefault: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        tenantId: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        label: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        holdKnownFraudster: undefined, // TODO: Add test value
        systemDefault: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policies        Should create a new policy', async () => {
        const res = await api('POST', '/api/policies', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policies        Should fail because we want to create a empty policy', async () => {
        const res = await api('POST', '/api/policies', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policies        Should list of policys with our new create one', async () => {
        const res = await api('GET', '/api/policies', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.label).toBe(testData.label);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.holdKnownFraudster).toBe(testData.holdKnownFraudster);
        expect(result.systemDefault).toBe(testData.systemDefault);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policies/:id    Should return one policy', async () => {
        const res = await api('GET', `/api/policies/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.label).toBe(testData.label);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.holdKnownFraudster).toBe(testData.holdKnownFraudster);
        expect(result.systemDefault).toBe(testData.systemDefault);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policies/:id    Should update the policy', async () => {
        const res = await api('PUT', `/api/policies/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.policyId).toBe(testDataUpdated.policyId);
        expect(result.label).toBe(testDataUpdated.label);
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.holdKnownFraudster).toBe(testDataUpdated.holdKnownFraudster);
        expect(result.systemDefault).toBe(testDataUpdated.systemDefault);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policies/:id    Should fail because we want to update the policy with a invalid email', async () => {
        const res = await api('PUT', `/api/policies/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policies/:id    Should delete the policy', async () => {
        const res = await api('DELETE', `/api/policies/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policies/:id    Should return with a 404, because we just deleted the policy', async () => {
        const res = await api('GET', `/api/policies/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policies/:id    Should return with a 404, because we just deleted the policy', async () => {
        const res = await api('DELETE', `/api/policies/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policies/:id    Should return with a 404, because we just deleted the policy', async () => {
        const res = await api('PUT', `/api/policies/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
