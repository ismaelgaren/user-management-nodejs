import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/queues', () => {

    const keys = [
        'payload', 'transactionType', 'status'
    ];

    const testData = {
        payload: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        payload: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /queues        Should create a new queue', async () => {
        const res = await api('POST', '/api/queues', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /queues        Should fail because we want to create a empty queue', async () => {
        const res = await api('POST', '/api/queues', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /queues        Should list of queues with our new create one', async () => {
        const res = await api('GET', '/api/queues', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.payload).toBe(testData.payload);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /queues/:id    Should return one queue', async () => {
        const res = await api('GET', `/api/queues/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.payload).toBe(testData.payload);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /queues/:id    Should update the queue', async () => {
        const res = await api('PUT', `/api/queues/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.payload).toBe(testDataUpdated.payload);
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /queues/:id    Should fail because we want to update the queue with a invalid email', async () => {
        const res = await api('PUT', `/api/queues/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /queues/:id    Should delete the queue', async () => {
        const res = await api('DELETE', `/api/queues/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /queues/:id    Should return with a 404, because we just deleted the queue', async () => {
        const res = await api('GET', `/api/queues/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /queues/:id    Should return with a 404, because we just deleted the queue', async () => {
        const res = await api('DELETE', `/api/queues/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /queues/:id    Should return with a 404, because we just deleted the queue', async () => {
        const res = await api('PUT', `/api/queues/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
