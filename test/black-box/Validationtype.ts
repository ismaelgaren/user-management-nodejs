import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/validationtypes', () => {

    const keys = [
        'label', 'description', 'structure', 'status'
    ];

    const testData = {
        label: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        structure: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        label: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        structure: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /validationtypes        Should create a new validationtype', async () => {
        const res = await api('POST', '/api/validationtypes', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /validationtypes        Should fail because we want to create a empty validationtype', async () => {
        const res = await api('POST', '/api/validationtypes', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /validationtypes        Should list of validationtypes with our new create one', async () => {
        const res = await api('GET', '/api/validationtypes', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.label).toBe(testData.label);
        expect(result.description).toBe(testData.description);
        expect(result.structure).toBe(testData.structure);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /validationtypes/:id    Should return one validationtype', async () => {
        const res = await api('GET', `/api/validationtypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testData.label);
        expect(result.description).toBe(testData.description);
        expect(result.structure).toBe(testData.structure);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /validationtypes/:id    Should update the validationtype', async () => {
        const res = await api('PUT', `/api/validationtypes/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testDataUpdated.label);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.structure).toBe(testDataUpdated.structure);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /validationtypes/:id    Should fail because we want to update the validationtype with a invalid email', async () => {
        const res = await api('PUT', `/api/validationtypes/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /validationtypes/:id    Should delete the validationtype', async () => {
        const res = await api('DELETE', `/api/validationtypes/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /validationtypes/:id    Should return with a 404, because we just deleted the validationtype', async () => {
        const res = await api('GET', `/api/validationtypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /validationtypes/:id    Should return with a 404, because we just deleted the validationtype', async () => {
        const res = await api('DELETE', `/api/validationtypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /validationtypes/:id    Should return with a 404, because we just deleted the validationtype', async () => {
        const res = await api('PUT', `/api/validationtypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
