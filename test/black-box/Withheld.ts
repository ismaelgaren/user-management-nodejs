import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/withhelds', () => {

    const keys = [
        'walletId', 'withheldAmount', 'status'
    ];

    const testData = {
        walletId: undefined, // TODO: Add test value
        withheldAmount: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        walletId: undefined, // TODO: Add test value
        withheldAmount: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /withhelds        Should create a new withheld', async () => {
        const res = await api('POST', '/api/withhelds', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /withhelds        Should fail because we want to create a empty withheld', async () => {
        const res = await api('POST', '/api/withhelds', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /withhelds        Should list of withhelds with our new create one', async () => {
        const res = await api('GET', '/api/withhelds', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.walletId).toBe(testData.walletId);
        expect(result.withheldAmount).toBe(testData.withheldAmount);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /withhelds/:id    Should return one withheld', async () => {
        const res = await api('GET', `/api/withhelds/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testData.walletId);
        expect(result.withheldAmount).toBe(testData.withheldAmount);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /withhelds/:id    Should update the withheld', async () => {
        const res = await api('PUT', `/api/withhelds/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.withheldAmount).toBe(testDataUpdated.withheldAmount);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /withhelds/:id    Should fail because we want to update the withheld with a invalid email', async () => {
        const res = await api('PUT', `/api/withhelds/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /withhelds/:id    Should delete the withheld', async () => {
        const res = await api('DELETE', `/api/withhelds/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /withhelds/:id    Should return with a 404, because we just deleted the withheld', async () => {
        const res = await api('GET', `/api/withhelds/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /withhelds/:id    Should return with a 404, because we just deleted the withheld', async () => {
        const res = await api('DELETE', `/api/withhelds/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /withhelds/:id    Should return with a 404, because we just deleted the withheld', async () => {
        const res = await api('PUT', `/api/withhelds/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
