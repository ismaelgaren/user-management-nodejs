import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/scheduledemailrecipients', () => {

    const keys = [
        'scheduleId', 'emailGroupId', 'status'
    ];

    const testData = {
        scheduleId: undefined, // TODO: Add test value
        emailGroupId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        scheduleId: undefined, // TODO: Add test value
        emailGroupId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /scheduledemailrecipients        Should create a new scheduledemailrecipient', async () => {
        const res = await api('POST', '/api/scheduledemailrecipients', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /scheduledemailrecipients        Should fail because we want to create a empty scheduledemailrecipient', async () => {
        const res = await api('POST', '/api/scheduledemailrecipients', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /scheduledemailrecipients        Should list of scheduledemailrecipients with our new create one', async () => {
        const res = await api('GET', '/api/scheduledemailrecipients', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.scheduleId).toBe(testData.scheduleId);
        expect(result.emailGroupId).toBe(testData.emailGroupId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /scheduledemailrecipients/:id    Should return one scheduledemailrecipient', async () => {
        const res = await api('GET', `/api/scheduledemailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.scheduleId).toBe(testData.scheduleId);
        expect(result.emailGroupId).toBe(testData.emailGroupId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /scheduledemailrecipients/:id    Should update the scheduledemailrecipient', async () => {
        const res = await api('PUT', `/api/scheduledemailrecipients/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.scheduleId).toBe(testDataUpdated.scheduleId);
        expect(result.emailGroupId).toBe(testDataUpdated.emailGroupId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /scheduledemailrecipients/:id    Should fail because we want to update the scheduledemailrecipient with a invalid email', async () => {
        const res = await api('PUT', `/api/scheduledemailrecipients/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /scheduledemailrecipients/:id    Should delete the scheduledemailrecipient', async () => {
        const res = await api('DELETE', `/api/scheduledemailrecipients/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /scheduledemailrecipients/:id    Should return with a 404, because we just deleted the scheduledemailrecipient', async () => {
        const res = await api('GET', `/api/scheduledemailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /scheduledemailrecipients/:id    Should return with a 404, because we just deleted the scheduledemailrecipient', async () => {
        const res = await api('DELETE', `/api/scheduledemailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /scheduledemailrecipients/:id    Should return with a 404, because we just deleted the scheduledemailrecipient', async () => {
        const res = await api('PUT', `/api/scheduledemailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
