import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/merchantwithdrawalaccounts', () => {

    const keys = [
        'alias', 'merchantId', 'accHolderFname', 'accHolderMname', 'accHolderLname', 'accHolderEmail', 'accHolderPhone', 'accHolderAddress', 'accHolderCity', 'accState', 'country', 'zip', 'bankAccountNo', 'accCurrency', 'bankId', 'acctType', 'tenantId'
    ];

    const testData = {
        alias: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        accHolderFname: undefined, // TODO: Add test value
        accHolderMname: undefined, // TODO: Add test value
        accHolderLname: undefined, // TODO: Add test value
        accHolderEmail: undefined, // TODO: Add test value
        accHolderPhone: undefined, // TODO: Add test value
        accHolderAddress: undefined, // TODO: Add test value
        accHolderCity: undefined, // TODO: Add test value
        accState: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        zip: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        accCurrency: undefined, // TODO: Add test value
        bankId: undefined, // TODO: Add test value
        acctType: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        alias: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        accHolderFname: undefined, // TODO: Add test value
        accHolderMname: undefined, // TODO: Add test value
        accHolderLname: undefined, // TODO: Add test value
        accHolderEmail: undefined, // TODO: Add test value
        accHolderPhone: undefined, // TODO: Add test value
        accHolderAddress: undefined, // TODO: Add test value
        accHolderCity: undefined, // TODO: Add test value
        accState: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        zip: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        accCurrency: undefined, // TODO: Add test value
        bankId: undefined, // TODO: Add test value
        acctType: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /merchantwithdrawalaccounts        Should create a new merchantwithdrawalaccount', async () => {
        const res = await api('POST', '/api/merchantwithdrawalaccounts', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /merchantwithdrawalaccounts        Should fail because we want to create a empty merchantwithdrawalaccount', async () => {
        const res = await api('POST', '/api/merchantwithdrawalaccounts', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /merchantwithdrawalaccounts        Should list of merchantwithdrawalaccounts with our new create one', async () => {
        const res = await api('GET', '/api/merchantwithdrawalaccounts', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.alias).toBe(testData.alias);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.accHolderFname).toBe(testData.accHolderFname);
        expect(result.accHolderMname).toBe(testData.accHolderMname);
        expect(result.accHolderLname).toBe(testData.accHolderLname);
        expect(result.accHolderEmail).toBe(testData.accHolderEmail);
        expect(result.accHolderPhone).toBe(testData.accHolderPhone);
        expect(result.accHolderAddress).toBe(testData.accHolderAddress);
        expect(result.accHolderCity).toBe(testData.accHolderCity);
        expect(result.accState).toBe(testData.accState);
        expect(result.country).toBe(testData.country);
        expect(result.zip).toBe(testData.zip);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.accCurrency).toBe(testData.accCurrency);
        expect(result.bankId).toBe(testData.bankId);
        expect(result.acctType).toBe(testData.acctType);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('GET       /merchantwithdrawalaccounts/:id    Should return one merchantwithdrawalaccount', async () => {
        const res = await api('GET', `/api/merchantwithdrawalaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.alias).toBe(testData.alias);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.accHolderFname).toBe(testData.accHolderFname);
        expect(result.accHolderMname).toBe(testData.accHolderMname);
        expect(result.accHolderLname).toBe(testData.accHolderLname);
        expect(result.accHolderEmail).toBe(testData.accHolderEmail);
        expect(result.accHolderPhone).toBe(testData.accHolderPhone);
        expect(result.accHolderAddress).toBe(testData.accHolderAddress);
        expect(result.accHolderCity).toBe(testData.accHolderCity);
        expect(result.accState).toBe(testData.accState);
        expect(result.country).toBe(testData.country);
        expect(result.zip).toBe(testData.zip);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.accCurrency).toBe(testData.accCurrency);
        expect(result.bankId).toBe(testData.bankId);
        expect(result.acctType).toBe(testData.acctType);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('PUT       /merchantwithdrawalaccounts/:id    Should update the merchantwithdrawalaccount', async () => {
        const res = await api('PUT', `/api/merchantwithdrawalaccounts/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.alias).toBe(testDataUpdated.alias);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.accHolderFname).toBe(testDataUpdated.accHolderFname);
        expect(result.accHolderMname).toBe(testDataUpdated.accHolderMname);
        expect(result.accHolderLname).toBe(testDataUpdated.accHolderLname);
        expect(result.accHolderEmail).toBe(testDataUpdated.accHolderEmail);
        expect(result.accHolderPhone).toBe(testDataUpdated.accHolderPhone);
        expect(result.accHolderAddress).toBe(testDataUpdated.accHolderAddress);
        expect(result.accHolderCity).toBe(testDataUpdated.accHolderCity);
        expect(result.accState).toBe(testDataUpdated.accState);
        expect(result.country).toBe(testDataUpdated.country);
        expect(result.zip).toBe(testDataUpdated.zip);
        expect(result.bankAccountNo).toBe(testDataUpdated.bankAccountNo);
        expect(result.accCurrency).toBe(testDataUpdated.accCurrency);
        expect(result.bankId).toBe(testDataUpdated.bankId);
        expect(result.acctType).toBe(testDataUpdated.acctType);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
    });

    test('PUT       /merchantwithdrawalaccounts/:id    Should fail because we want to update the merchantwithdrawalaccount with a invalid email', async () => {
        const res = await api('PUT', `/api/merchantwithdrawalaccounts/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /merchantwithdrawalaccounts/:id    Should delete the merchantwithdrawalaccount', async () => {
        const res = await api('DELETE', `/api/merchantwithdrawalaccounts/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /merchantwithdrawalaccounts/:id    Should return with a 404, because we just deleted the merchantwithdrawalaccount', async () => {
        const res = await api('GET', `/api/merchantwithdrawalaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /merchantwithdrawalaccounts/:id    Should return with a 404, because we just deleted the merchantwithdrawalaccount', async () => {
        const res = await api('DELETE', `/api/merchantwithdrawalaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /merchantwithdrawalaccounts/:id    Should return with a 404, because we just deleted the merchantwithdrawalaccount', async () => {
        const res = await api('PUT', `/api/merchantwithdrawalaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
