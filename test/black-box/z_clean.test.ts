import {encrypt} from '../../src/api/helpers/encryption';
import * as knex from 'knex';
import {Tables} from '../../src/constants/Tables';
import {api} from './lib/api';
import * as dotenv from 'dotenv';
dotenv.config();

describe('/clean_test_data', () => {
  let knexConstant = null;
  beforeAll(async () => {
      knexConstant = knex( {
        client: process.env.DB_CLIENT,
        connection: process.env.DB_CONNECTION,
      });
  });

  test(`Removing: test data from Suspect Transaction`, async () => {
    const res =  await knexConstant(Tables.SuspectTransaction).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from SMS History`, async () => {
    const res =  await knexConstant(Tables.SmsHistory).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Disbursement Notification`, async () => {
    const res =  await knexConstant(Tables.DisbursementNotification).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Addendum`, async () => {
    const res =  await knexConstant(Tables.Addendum).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Details Response`, async () => {
    const res =  await knexConstant(Tables.DetailsResponse).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Request Log`, async () => {
    const res =  await knexConstant(Tables.RequestLog).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Details Request`, async () => {
    const res =  await knexConstant(Tables.DetailsRequest).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Disbursement History`, async () => {
    const res =  await knexConstant(Tables.DisbursementHistory).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Response Processor`, async () => {
    const res =  await knexConstant(Tables.ResponseProcessor).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Header Request`, async () => {
    const res =  await knexConstant(Tables.HeaderRequest).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Processor Balance Logs`, async () => {
    const res =  await knexConstant(Tables.ProcessorBalanceLogs).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Merchat Transaction Fee`, async () => {
    const res =  await knexConstant(Tables.MerchatTransactionFee).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from UBP Accounts`, async () => {
    const res =  await knexConstant(Tables.UBPAccounts).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Response Processor Code`, async () => {
    const res =  await knexConstant(Tables.ResponseProcessorCode).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Processor Fee`, async () => {
    const res =  await knexConstant(Tables.ProcessorFee).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Processor Business Case`, async () => {
    const res =  await knexConstant(Tables.ProcessorBusinessCase).where({/* add condition here*/}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Merchant Settings`, async () => {
    const res =  await knexConstant(Tables.MerchantSettings).where({meta_item: 'meta01'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Withdraw History`, async () => {
    const res =  await knexConstant(Tables.WithdrawHistory).where({wallet_id: 'Wallet ID here'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Wallet Data`, async () => {
    const res =  await knexConstant(Tables.WalletData).where({owner_id: 'OWNER01'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from UBP Status URL`, async () => {
    const res =  await knexConstant(Tables.UBPStatusURL).where({batch_code: 'Batch code here'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from UBP Enroll Info`, async () => {
    const res =  await knexConstant(Tables.UBPEnrollInfo).where({batch_name: 'Batch name here'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Processor`, async () => {
    const res =  await knexConstant(Tables.Processor).where({processor_id: '88888888'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Header Response`, async () => {
    const res =  await knexConstant(Tables.HeaderResponse).where({response_id: '1234567890'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Merchant`, async () => {
    const res =  await knexConstant(Tables.Merchant).where({merchant_id: 'MER00001'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Load History`, async () => {
    const res =  await knexConstant(Tables.LoadHistory).where({wallet_id: 'w-001'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from State`, async () => {
    const res =  await knexConstant(Tables.State).where({state: 'TEST NCR'}).delete();
    expect(res).toBe(1);
  });
  test(`Removing: test data from Response Codes`, async () => {
    const res =  await knexConstant(Tables.ResponseCodes).where({code: '123'}).delete();
    expect(res).toBe(1);
  });

  test(`Removing: test data from Lookups`, async () => {
    const res =  await knexConstant(Tables.Lookups).where({group: 'TEST_GROUP'}).delete();
    expect(res).toBeGreaterThanOrEqual(1);
  });

  afterAll(async () => {
    await knexConstant.destroy();
  });

});
