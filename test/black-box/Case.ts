import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/cases', () => {

    const keys = [
        'merchantId', 'flaggedId', 'transactionId', 'status', 'violation', 'policyId', 'escalatedBy', 'riskLevel'
    ];

    const testData = {
        merchantId: undefined, // TODO: Add test value
        flaggedId: undefined, // TODO: Add test value
        transactionId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        violation: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        escalatedBy: undefined, // TODO: Add test value
        riskLevel: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        merchantId: undefined, // TODO: Add test value
        flaggedId: undefined, // TODO: Add test value
        transactionId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        violation: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        escalatedBy: undefined, // TODO: Add test value
        riskLevel: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /cases        Should create a new case', async () => {
        const res = await api('POST', '/api/cases', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /cases        Should fail because we want to create a empty case', async () => {
        const res = await api('POST', '/api/cases', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /cases        Should list of cases with our new create one', async () => {
        const res = await api('GET', '/api/cases', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.flaggedId).toBe(testData.flaggedId);
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.status).toBe(testData.status);
        expect(result.violation).toBe(testData.violation);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.escalatedBy).toBe(testData.escalatedBy);
        expect(result.riskLevel).toBe(testData.riskLevel);
    });

    test('GET       /cases/:id    Should return one case', async () => {
        const res = await api('GET', `/api/cases/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.flaggedId).toBe(testData.flaggedId);
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.status).toBe(testData.status);
        expect(result.violation).toBe(testData.violation);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.escalatedBy).toBe(testData.escalatedBy);
        expect(result.riskLevel).toBe(testData.riskLevel);
    });

    test('PUT       /cases/:id    Should update the case', async () => {
        const res = await api('PUT', `/api/cases/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.flaggedId).toBe(testDataUpdated.flaggedId);
        expect(result.transactionId).toBe(testDataUpdated.transactionId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.violation).toBe(testDataUpdated.violation);
        expect(result.policyId).toBe(testDataUpdated.policyId);
        expect(result.escalatedBy).toBe(testDataUpdated.escalatedBy);
        expect(result.riskLevel).toBe(testDataUpdated.riskLevel);
    });

    test('PUT       /cases/:id    Should fail because we want to update the case with a invalid email', async () => {
        const res = await api('PUT', `/api/cases/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /cases/:id    Should delete the case', async () => {
        const res = await api('DELETE', `/api/cases/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /cases/:id    Should return with a 404, because we just deleted the case', async () => {
        const res = await api('GET', `/api/cases/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /cases/:id    Should return with a 404, because we just deleted the case', async () => {
        const res = await api('DELETE', `/api/cases/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /cases/:id    Should return with a 404, because we just deleted the case', async () => {
        const res = await api('PUT', `/api/cases/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
