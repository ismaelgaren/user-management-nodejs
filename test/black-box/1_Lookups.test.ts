import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import {
    // createAdminUser,
    getToken,
} from './lib/auth';


describe('/lookups', () => {

    const keys = [
        'group', 'label', 'isActive', 'isReadOnly',
    ];

    const testData = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test Lookup', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

    const testDataUpdated = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test Lookup Updated', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

    const testCurrency = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test_PHP', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

    const testCountry = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test_Philippines', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

    const testStatusActive = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test_Active', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

    const testStatusInactive = {
        group: 'TEST_GROUP', // TODO: Add test value
        label: 'Test_Inactive', // TODO: Add test value
        isActive: true, // TODO: Add test value
        isReadOnly: false, // TODO: Add test value
    };

        const testState = {
            group: 'TEST_GROUP', // TODO: Add test value
            label: 'Test_State', // TODO: Add test value
            isActive: true, // TODO: Add test value
            isReadOnly: false, // TODO: Add test value
        };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        // const command = new DatabaseResetCommand();
        // await command.run();
        // await createAdminUser();
        const res = await api('POST', '/api/authenticate', {
        headers: {
            'Content-Type': 'application/json',
        },
        body: {
            username: 'administrator',
            password: '123@123',
        },
        });
        token = res.getData()['token'];
        auth = {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
        };
        return true;
    });

    test('POST      /lookups        Should create a new lookups', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: testData,
        });
        createdId = res.getData()['Id'];
        res.expectJson();
        res.expectStatusCode(201);
    });

    test('POST      /lookups        Should fail because we want to create a empty lookups', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: {},
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /lookups        Should list of lookups with our new create one', async () => {
        const res = await api('GET', '/api/lookups', {headers: auth});
        res.expectJson();
        res.expectStatusCode(200);
    });

    test('GET       /lookups/:id    Should return one lookups', async () => {
        const res = await api('GET', `/api/lookups/${createdId}`, {headers: auth});
        res.expectJson();
        res.expectStatusCode(200);

        const result: any = res.getData();
        expect(result.group).toBe(testData.group);
        expect(result.label).toBe(testData.label);
        expect(result.isActive).toBe(testData.isActive);
        expect(result.isReadOnly).toBe(testData.isReadOnly);
    });

    test('PUT       /lookups/:id    Should update the lookups', async () => {
        const res = await api('PUT', `/api/lookups/${createdId}`, {
            headers: auth,
            body: testDataUpdated,
        });
        res.expectJson();
        res.expectStatusCode(200);

        const result: any = res.getData();
        expect(result.group).toBe(testDataUpdated.group);
        expect(result.label).toBe(testDataUpdated.label);
        expect(result.isActive).toBe(testDataUpdated.isActive);
        expect(result.isReadOnly).toBe(testDataUpdated.isReadOnly);
    });

    test('PUT       /lookups/:id    Should fail because we want to update the lookups with a invalid email', async () => {
        const res = await api('PUT', `/api/lookups/${createdId}`, {
            headers: auth,
            body: {
                email: 'abc',
            },
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('POST      /lookups        Should create test country lookup \'Philippines\' ', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: testCountry,
        });
        res.expectJson();
        res.expectStatusCode(201);
    });

    test('POST      /lookups        Should create test currency lookup \'PHP\' ', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: testCurrency,
        });
        res.expectJson();
        res.expectStatusCode(201);
    });

    test('POST      /lookups        Should create test status lookup \'Active\' ', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: testStatusActive,
        });
        res.expectJson();
        res.expectStatusCode(201);
    });

    test('POST      /lookups        Should create test status lookup \'Inactive\' ', async () => {
        const res = await api('POST', '/api/lookups', {
            headers: auth,
            body: testStatusInactive,
        });
        res.expectJson();
        res.expectStatusCode(201);
    });


    // test('DELETE    /lookups/:id    Should delete the lookups', async () => {
    //     const res = await api('DELETE', `/api/lookups/${createdId}`, {headers: auth});
    //     res.expectStatusCode(200);
    // });

    /**
     * 404 - NotFound Testing
     */
    // test('GET       /lookups/:id    Should return with a 404, because we just deleted the lookups', async () => {
    //     const res = await api('GET', `/api/lookups/${createdId}`, {headers: auth});
    //     res.expectJson();
    //     res.expectStatusCode(404);
    // });

    // test('DELETE    /lookups/:id    Should return with a 404, because we just deleted the lookups', async () => {
    //     const res = await api('DELETE', `/api/lookups/${createdId}`, {headers: auth});
    //     res.expectJson();
    //     res.expectStatusCode(404);
    // });

    test('PUT       /lookups/:id    Should return with a 404, because we just deleted the lookups', async () => {
        const res = await api('PUT', `/api/lookups/${createdId}`, {headers: auth});
        res.expectJson();
        res.expectStatusCode(400);
    });

});
