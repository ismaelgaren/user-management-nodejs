import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policyfrequencyitems', () => {

    const keys = [
        'policyItemId', 'status', 'parameter', 'limit', 'duration', 'frequency'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        limit: undefined, // TODO: Add test value
        duration: undefined, // TODO: Add test value
        frequency: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        limit: undefined, // TODO: Add test value
        duration: undefined, // TODO: Add test value
        frequency: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policyfrequencyitems        Should create a new policyfrequencyitem', async () => {
        const res = await api('POST', '/api/policyfrequencyitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policyfrequencyitems        Should fail because we want to create a empty policyfrequencyitem', async () => {
        const res = await api('POST', '/api/policyfrequencyitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policyfrequencyitems        Should list of policyfrequencyitems with our new create one', async () => {
        const res = await api('GET', '/api/policyfrequencyitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.limit).toBe(testData.limit);
        expect(result.duration).toBe(testData.duration);
        expect(result.frequency).toBe(testData.frequency);
    });

    test('GET       /policyfrequencyitems/:id    Should return one policyfrequencyitem', async () => {
        const res = await api('GET', `/api/policyfrequencyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.limit).toBe(testData.limit);
        expect(result.duration).toBe(testData.duration);
        expect(result.frequency).toBe(testData.frequency);
    });

    test('PUT       /policyfrequencyitems/:id    Should update the policyfrequencyitem', async () => {
        const res = await api('PUT', `/api/policyfrequencyitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.parameter).toBe(testDataUpdated.parameter);
        expect(result.limit).toBe(testDataUpdated.limit);
        expect(result.duration).toBe(testDataUpdated.duration);
        expect(result.frequency).toBe(testDataUpdated.frequency);
    });

    test('PUT       /policyfrequencyitems/:id    Should fail because we want to update the policyfrequencyitem with a invalid email', async () => {
        const res = await api('PUT', `/api/policyfrequencyitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policyfrequencyitems/:id    Should delete the policyfrequencyitem', async () => {
        const res = await api('DELETE', `/api/policyfrequencyitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policyfrequencyitems/:id    Should return with a 404, because we just deleted the policyfrequencyitem', async () => {
        const res = await api('GET', `/api/policyfrequencyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policyfrequencyitems/:id    Should return with a 404, because we just deleted the policyfrequencyitem', async () => {
        const res = await api('DELETE', `/api/policyfrequencyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policyfrequencyitems/:id    Should return with a 404, because we just deleted the policyfrequencyitem', async () => {
        const res = await api('PUT', `/api/policyfrequencyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
