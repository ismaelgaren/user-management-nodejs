import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policyvalidationitems', () => {

    const keys = [
        'policyItemId', 'status', 'parameter'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policyvalidationitems        Should create a new policyvalidationitem', async () => {
        const res = await api('POST', '/api/policyvalidationitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policyvalidationitems        Should fail because we want to create a empty policyvalidationitem', async () => {
        const res = await api('POST', '/api/policyvalidationitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policyvalidationitems        Should list of policyvalidationitems with our new create one', async () => {
        const res = await api('GET', '/api/policyvalidationitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
    });

    test('GET       /policyvalidationitems/:id    Should return one policyvalidationitem', async () => {
        const res = await api('GET', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
    });

    test('PUT       /policyvalidationitems/:id    Should update the policyvalidationitem', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.parameter).toBe(testDataUpdated.parameter);
    });

    test('PUT       /policyvalidationitems/:id    Should fail because we want to update the policyvalidationitem with a invalid email', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policyvalidationitems/:id    Should delete the policyvalidationitem', async () => {
        const res = await api('DELETE', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitem', async () => {
        const res = await api('GET', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitem', async () => {
        const res = await api('DELETE', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitem', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
