import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/tempbalances', () => {

    const keys = [
        'queueId', 'walletId', 'balance'
    ];

    const testData = {
        queueId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        balance: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        queueId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        balance: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /tempbalances        Should create a new tempbalance', async () => {
        const res = await api('POST', '/api/tempbalances', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /tempbalances        Should fail because we want to create a empty tempbalance', async () => {
        const res = await api('POST', '/api/tempbalances', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /tempbalances        Should list of tempbalances with our new create one', async () => {
        const res = await api('GET', '/api/tempbalances', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.queueId).toBe(testData.queueId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.balance).toBe(testData.balance);
    });

    test('GET       /tempbalances/:id    Should return one tempbalance', async () => {
        const res = await api('GET', `/api/tempbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.queueId).toBe(testData.queueId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.balance).toBe(testData.balance);
    });

    test('PUT       /tempbalances/:id    Should update the tempbalance', async () => {
        const res = await api('PUT', `/api/tempbalances/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.queueId).toBe(testDataUpdated.queueId);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.balance).toBe(testDataUpdated.balance);
    });

    test('PUT       /tempbalances/:id    Should fail because we want to update the tempbalance with a invalid email', async () => {
        const res = await api('PUT', `/api/tempbalances/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /tempbalances/:id    Should delete the tempbalance', async () => {
        const res = await api('DELETE', `/api/tempbalances/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /tempbalances/:id    Should return with a 404, because we just deleted the tempbalance', async () => {
        const res = await api('GET', `/api/tempbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /tempbalances/:id    Should return with a 404, because we just deleted the tempbalance', async () => {
        const res = await api('DELETE', `/api/tempbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /tempbalances/:id    Should return with a 404, because we just deleted the tempbalance', async () => {
        const res = await api('PUT', `/api/tempbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
