import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { getToken } from './lib/auth';
import * as knex from 'knex';
import {Tables} from '../../src/constants/Tables';
import * as dotenv from 'dotenv';
dotenv.config();

describe('/states', () => {

    const keys = [
        'state', 'fkCountry',
    ];

    let token;
    let auth;
    let createdId;
    let fkCountryTest;
    let testData;
    // let testDataUpdated;
    const knexConstant = knex( {
        client: process.env.DB_CLIENT,
        connection: process.env.DB_CONNECTION,
        });

    beforeAll(async () => {


        const country = await knexConstant(Tables.Lookups).where({label: 'Test_Philippines'});
        fkCountryTest =  country[0].id;

        // await knexConstant.destroy();

        const res = await api('POST', '/api/authenticate', {
        headers: {
            'Content-Type': 'application/json',
        },
        body: {
            username: 'administrator',
            password: '123@123',
        },
        });
        token = res.getData()['token'];
        auth = {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
        };

        testData = {
            state: 'TEST NCR', // TODO: Add test value
            fkCountry: fkCountryTest, // TODO: Add test value
        };

        // testDataUpdated = {
        //     state: 'TEST NCR', // TODO: Add test value
        //     fkCountry: fkCountryTest, // TODO: Add test value
        // };

        return true;
    });



    test('POST      /states        Should create a new state', async () => {
        const res = await api('POST', '/api/states', {
            headers: auth,
            body: testData,
        });
        res.expectJson();
        res.expectStatusCode(201);
        // res.expectData(keys);
        createdId = res.getData()['Id'];
    });

    test('POST      /states        Should fail because we want to create a empty state', async () => {
        const res = await api('POST', '/api/states', {
            headers: auth,
            body: {},
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /states        Should list of states with our new create one', async () => {
        const res = await api('GET', '/api/states', {headers: auth});
        res.expectJson();
        res.expectStatusCode(200);
    });

    test('GET       /states/:id    Should return one state', async () => {
        const res = await api('GET', `/api/states/${createdId}`, {headers: auth});
        res.expectJson();
        res.expectStatusCode(200);
        // res.expectData(keys);

        const result: any = res.getData();
        expect(result.state).toBe(testData.state);
        expect(result.fkCountry).toBe(testData.fkCountry);
    });

    afterAll(async () => {
        await knexConstant.destroy();
      });

    // test('PUT       /states/:id    Should update the state', async () => {
    //     const res = await api('PUT', `/api/states/${createdId}`, {
    //         token,
    //         body: testDataUpdated,
    //     });
    //     res.expectJson();
    //     res.expectStatusCode(200);
    //     // res.expectData(keys);

    //     const result: any = res.getData();
    //     expect(result.state).toBe(testDataUpdated.state);
    //     expect(result.fkCountry).toBe(testDataUpdated.fkCountry);
    // });

    // test('PUT       /states/:id    Should fail because we want to update the state with a invalid email', async () => {
    //     const res = await api('PUT', `/api/states/${createdId}`, {
    //         token,
    //         body: {
    //             email: 'abc',
    //         },
    //     });
    //     res.expectJson();
    //     res.expectStatusCode(400);
    // });

    // test('DELETE    /states/:id    Should delete the state', async () => {
    //     const res = await api('DELETE', `/api/states/${createdId}`, auth);
    //     res.expectStatusCode(200);
    // });

    // /**
    //  * 404 - NotFound Testing
    //  */
    // test('GET       /states/:id    Should return with a 404, because we just deleted the state', async () => {
    //     const res = await api('GET', `/api/states/${createdId}`, auth);
    //     res.expectJson();
    //     res.expectStatusCode(404);
    // });

    // test('DELETE    /states/:id    Should return with a 404, because we just deleted the state', async () => {
    //     const res = await api('DELETE', `/api/states/${createdId}`, auth);
    //     res.expectJson();
    //     res.expectStatusCode(404);
    // });

    // test('PUT       /states/:id    Should return with a 404, because we just deleted the state', async () => {
    //     const res = await api('PUT', `/api/states/${createdId}`, auth);
    //     res.expectJson();
    //     res.expectStatusCode(404);
    // });

});
