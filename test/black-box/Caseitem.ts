import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/caseitems', () => {

    const keys = [
        'caseId', 'status', 'flagGroupId', 'escalateDate'
    ];

    const testData = {
        caseId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        flagGroupId: undefined, // TODO: Add test value
        escalateDate: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        caseId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        flagGroupId: undefined, // TODO: Add test value
        escalateDate: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /caseitems        Should create a new caseitem', async () => {
        const res = await api('POST', '/api/caseitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /caseitems        Should fail because we want to create a empty caseitem', async () => {
        const res = await api('POST', '/api/caseitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /caseitems        Should list of caseitems with our new create one', async () => {
        const res = await api('GET', '/api/caseitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.caseId).toBe(testData.caseId);
        expect(result.status).toBe(testData.status);
        expect(result.flagGroupId).toBe(testData.flagGroupId);
        expect(result.escalateDate).toBe(testData.escalateDate);
    });

    test('GET       /caseitems/:id    Should return one caseitem', async () => {
        const res = await api('GET', `/api/caseitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.caseId).toBe(testData.caseId);
        expect(result.status).toBe(testData.status);
        expect(result.flagGroupId).toBe(testData.flagGroupId);
        expect(result.escalateDate).toBe(testData.escalateDate);
    });

    test('PUT       /caseitems/:id    Should update the caseitem', async () => {
        const res = await api('PUT', `/api/caseitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.caseId).toBe(testDataUpdated.caseId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.flagGroupId).toBe(testDataUpdated.flagGroupId);
        expect(result.escalateDate).toBe(testDataUpdated.escalateDate);
    });

    test('PUT       /caseitems/:id    Should fail because we want to update the caseitem with a invalid email', async () => {
        const res = await api('PUT', `/api/caseitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /caseitems/:id    Should delete the caseitem', async () => {
        const res = await api('DELETE', `/api/caseitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /caseitems/:id    Should return with a 404, because we just deleted the caseitem', async () => {
        const res = await api('GET', `/api/caseitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /caseitems/:id    Should return with a 404, because we just deleted the caseitem', async () => {
        const res = await api('DELETE', `/api/caseitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /caseitems/:id    Should return with a 404, because we just deleted the caseitem', async () => {
        const res = await api('PUT', `/api/caseitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
