import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/dailywalletbalances', () => {

    const keys = [
        'walletId', 'balanceWoWithheld', 'balanceWWithheld'
    ];

    const testData = {
        walletId: undefined, // TODO: Add test value
        balanceWoWithheld: undefined, // TODO: Add test value
        balanceWWithheld: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        walletId: undefined, // TODO: Add test value
        balanceWoWithheld: undefined, // TODO: Add test value
        balanceWWithheld: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /dailywalletbalances        Should create a new dailywalletbalance', async () => {
        const res = await api('POST', '/api/dailywalletbalances', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /dailywalletbalances        Should fail because we want to create a empty dailywalletbalance', async () => {
        const res = await api('POST', '/api/dailywalletbalances', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /dailywalletbalances        Should list of dailywalletbalances with our new create one', async () => {
        const res = await api('GET', '/api/dailywalletbalances', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.walletId).toBe(testData.walletId);
        expect(result.balanceWoWithheld).toBe(testData.balanceWoWithheld);
        expect(result.balanceWWithheld).toBe(testData.balanceWWithheld);
    });

    test('GET       /dailywalletbalances/:id    Should return one dailywalletbalance', async () => {
        const res = await api('GET', `/api/dailywalletbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testData.walletId);
        expect(result.balanceWoWithheld).toBe(testData.balanceWoWithheld);
        expect(result.balanceWWithheld).toBe(testData.balanceWWithheld);
    });

    test('PUT       /dailywalletbalances/:id    Should update the dailywalletbalance', async () => {
        const res = await api('PUT', `/api/dailywalletbalances/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.balanceWoWithheld).toBe(testDataUpdated.balanceWoWithheld);
        expect(result.balanceWWithheld).toBe(testDataUpdated.balanceWWithheld);
    });

    test('PUT       /dailywalletbalances/:id    Should fail because we want to update the dailywalletbalance with a invalid email', async () => {
        const res = await api('PUT', `/api/dailywalletbalances/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /dailywalletbalances/:id    Should delete the dailywalletbalance', async () => {
        const res = await api('DELETE', `/api/dailywalletbalances/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /dailywalletbalances/:id    Should return with a 404, because we just deleted the dailywalletbalance', async () => {
        const res = await api('GET', `/api/dailywalletbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /dailywalletbalances/:id    Should return with a 404, because we just deleted the dailywalletbalance', async () => {
        const res = await api('DELETE', `/api/dailywalletbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /dailywalletbalances/:id    Should return with a 404, because we just deleted the dailywalletbalance', async () => {
        const res = await api('PUT', `/api/dailywalletbalances/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
