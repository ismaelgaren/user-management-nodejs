import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/reconcentinelreports', () => {

    const keys = [
        'reconId', 'postingDate', 'transactionDate', 'description', 'amount', 'transactionType', 'remarks'
    ];

    const testData = {
        reconId: undefined, // TODO: Add test value
        postingDate: undefined, // TODO: Add test value
        transactionDate: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        remarks: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        reconId: undefined, // TODO: Add test value
        postingDate: undefined, // TODO: Add test value
        transactionDate: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        remarks: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /reconcentinelreports        Should create a new reconcentinelreport', async () => {
        const res = await api('POST', '/api/reconcentinelreports', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /reconcentinelreports        Should fail because we want to create a empty reconcentinelreport', async () => {
        const res = await api('POST', '/api/reconcentinelreports', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /reconcentinelreports        Should list of reconcentinelreports with our new create one', async () => {
        const res = await api('GET', '/api/reconcentinelreports', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.reconId).toBe(testData.reconId);
        expect(result.postingDate).toBe(testData.postingDate);
        expect(result.transactionDate).toBe(testData.transactionDate);
        expect(result.description).toBe(testData.description);
        expect(result.amount).toBe(testData.amount);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.remarks).toBe(testData.remarks);
    });

    test('GET       /reconcentinelreports/:id    Should return one reconcentinelreport', async () => {
        const res = await api('GET', `/api/reconcentinelreports/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testData.reconId);
        expect(result.postingDate).toBe(testData.postingDate);
        expect(result.transactionDate).toBe(testData.transactionDate);
        expect(result.description).toBe(testData.description);
        expect(result.amount).toBe(testData.amount);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.remarks).toBe(testData.remarks);
    });

    test('PUT       /reconcentinelreports/:id    Should update the reconcentinelreport', async () => {
        const res = await api('PUT', `/api/reconcentinelreports/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testDataUpdated.reconId);
        expect(result.postingDate).toBe(testDataUpdated.postingDate);
        expect(result.transactionDate).toBe(testDataUpdated.transactionDate);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.remarks).toBe(testDataUpdated.remarks);
    });

    test('PUT       /reconcentinelreports/:id    Should fail because we want to update the reconcentinelreport with a invalid email', async () => {
        const res = await api('PUT', `/api/reconcentinelreports/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /reconcentinelreports/:id    Should delete the reconcentinelreport', async () => {
        const res = await api('DELETE', `/api/reconcentinelreports/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /reconcentinelreports/:id    Should return with a 404, because we just deleted the reconcentinelreport', async () => {
        const res = await api('GET', `/api/reconcentinelreports/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /reconcentinelreports/:id    Should return with a 404, because we just deleted the reconcentinelreport', async () => {
        const res = await api('DELETE', `/api/reconcentinelreports/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /reconcentinelreports/:id    Should return with a 404, because we just deleted the reconcentinelreport', async () => {
        const res = await api('PUT', `/api/reconcentinelreports/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
