import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/customers', () => {

    const keys = [
        'name', 'address', 'email', 'mobileNumber'
    ];

    const testData = {
        name: undefined, // TODO: Add test value
        address: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        mobileNumber: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        name: undefined, // TODO: Add test value
        address: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        mobileNumber: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /customers        Should create a new customer', async () => {
        const res = await api('POST', '/api/customers', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /customers        Should fail because we want to create a empty customer', async () => {
        const res = await api('POST', '/api/customers', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /customers        Should list of customers with our new create one', async () => {
        const res = await api('GET', '/api/customers', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.name).toBe(testData.name);
        expect(result.address).toBe(testData.address);
        expect(result.email).toBe(testData.email);
        expect(result.mobileNumber).toBe(testData.mobileNumber);
    });

    test('GET       /customers/:id    Should return one customer', async () => {
        const res = await api('GET', `/api/customers/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testData.name);
        expect(result.address).toBe(testData.address);
        expect(result.email).toBe(testData.email);
        expect(result.mobileNumber).toBe(testData.mobileNumber);
    });

    test('PUT       /customers/:id    Should update the customer', async () => {
        const res = await api('PUT', `/api/customers/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.address).toBe(testDataUpdated.address);
        expect(result.email).toBe(testDataUpdated.email);
        expect(result.mobileNumber).toBe(testDataUpdated.mobileNumber);
    });

    test('PUT       /customers/:id    Should fail because we want to update the customer with a invalid email', async () => {
        const res = await api('PUT', `/api/customers/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /customers/:id    Should delete the customer', async () => {
        const res = await api('DELETE', `/api/customers/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /customers/:id    Should return with a 404, because we just deleted the customer', async () => {
        const res = await api('GET', `/api/customers/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /customers/:id    Should return with a 404, because we just deleted the customer', async () => {
        const res = await api('DELETE', `/api/customers/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /customers/:id    Should return with a 404, because we just deleted the customer', async () => {
        const res = await api('PUT', `/api/customers/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
