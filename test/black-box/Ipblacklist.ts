import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/ipblacklists', () => {

    const keys = [
        'ipAddress', 'country', 'status'
    ];

    const testData = {
        ipAddress: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        ipAddress: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /ipblacklists        Should create a new ipblacklist', async () => {
        const res = await api('POST', '/api/ipblacklists', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /ipblacklists        Should fail because we want to create a empty ipblacklist', async () => {
        const res = await api('POST', '/api/ipblacklists', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /ipblacklists        Should list of ipblacklists with our new create one', async () => {
        const res = await api('GET', '/api/ipblacklists', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.ipAddress).toBe(testData.ipAddress);
        expect(result.country).toBe(testData.country);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /ipblacklists/:id    Should return one ipblacklist', async () => {
        const res = await api('GET', `/api/ipblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.ipAddress).toBe(testData.ipAddress);
        expect(result.country).toBe(testData.country);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /ipblacklists/:id    Should update the ipblacklist', async () => {
        const res = await api('PUT', `/api/ipblacklists/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.ipAddress).toBe(testDataUpdated.ipAddress);
        expect(result.country).toBe(testDataUpdated.country);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /ipblacklists/:id    Should fail because we want to update the ipblacklist with a invalid email', async () => {
        const res = await api('PUT', `/api/ipblacklists/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /ipblacklists/:id    Should delete the ipblacklist', async () => {
        const res = await api('DELETE', `/api/ipblacklists/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /ipblacklists/:id    Should return with a 404, because we just deleted the ipblacklist', async () => {
        const res = await api('GET', `/api/ipblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /ipblacklists/:id    Should return with a 404, because we just deleted the ipblacklist', async () => {
        const res = await api('DELETE', `/api/ipblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /ipblacklists/:id    Should return with a 404, because we just deleted the ipblacklist', async () => {
        const res = await api('PUT', `/api/ipblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
