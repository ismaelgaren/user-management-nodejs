import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/inventories', () => {

    const keys = [
        'merchantId', 'itemCode', 'itemDescription', 'itemAmount', 'quantity'
    ];

    const testData = {
        merchantId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        itemDescription: undefined, // TODO: Add test value
        itemAmount: undefined, // TODO: Add test value
        quantity: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        merchantId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        itemDescription: undefined, // TODO: Add test value
        itemAmount: undefined, // TODO: Add test value
        quantity: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /inventories        Should create a new inventory', async () => {
        const res = await api('POST', '/api/inventories', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /inventories        Should fail because we want to create a empty inventory', async () => {
        const res = await api('POST', '/api/inventories', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /inventories        Should list of inventorys with our new create one', async () => {
        const res = await api('GET', '/api/inventories', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.itemDescription).toBe(testData.itemDescription);
        expect(result.itemAmount).toBe(testData.itemAmount);
        expect(result.quantity).toBe(testData.quantity);
    });

    test('GET       /inventories/:id    Should return one inventory', async () => {
        const res = await api('GET', `/api/inventories/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.itemDescription).toBe(testData.itemDescription);
        expect(result.itemAmount).toBe(testData.itemAmount);
        expect(result.quantity).toBe(testData.quantity);
    });

    test('PUT       /inventories/:id    Should update the inventory', async () => {
        const res = await api('PUT', `/api/inventories/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.itemCode).toBe(testDataUpdated.itemCode);
        expect(result.itemDescription).toBe(testDataUpdated.itemDescription);
        expect(result.itemAmount).toBe(testDataUpdated.itemAmount);
        expect(result.quantity).toBe(testDataUpdated.quantity);
    });

    test('PUT       /inventories/:id    Should fail because we want to update the inventory with a invalid email', async () => {
        const res = await api('PUT', `/api/inventories/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /inventories/:id    Should delete the inventory', async () => {
        const res = await api('DELETE', `/api/inventories/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /inventories/:id    Should return with a 404, because we just deleted the inventory', async () => {
        const res = await api('GET', `/api/inventories/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /inventories/:id    Should return with a 404, because we just deleted the inventory', async () => {
        const res = await api('DELETE', `/api/inventories/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /inventories/:id    Should return with a 404, because we just deleted the inventory', async () => {
        const res = await api('PUT', `/api/inventories/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
