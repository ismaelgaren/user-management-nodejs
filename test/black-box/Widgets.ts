import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/widgets', () => {

    const keys = [
        'title', 'description', 'script'
    ];

    const testData = {
        title: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        script: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        title: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        script: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /widgets        Should create a new widgets', async () => {
        const res = await api('POST', '/api/widgets', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /widgets        Should fail because we want to create a empty widgets', async () => {
        const res = await api('POST', '/api/widgets', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /widgets        Should list of widgetss with our new create one', async () => {
        const res = await api('GET', '/api/widgets', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.title).toBe(testData.title);
        expect(result.description).toBe(testData.description);
        expect(result.script).toBe(testData.script);
    });

    test('GET       /widgets/:id    Should return one widgets', async () => {
        const res = await api('GET', `/api/widgets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.title).toBe(testData.title);
        expect(result.description).toBe(testData.description);
        expect(result.script).toBe(testData.script);
    });

    test('PUT       /widgets/:id    Should update the widgets', async () => {
        const res = await api('PUT', `/api/widgets/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.title).toBe(testDataUpdated.title);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.script).toBe(testDataUpdated.script);
    });

    test('PUT       /widgets/:id    Should fail because we want to update the widgets with a invalid email', async () => {
        const res = await api('PUT', `/api/widgets/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /widgets/:id    Should delete the widgets', async () => {
        const res = await api('DELETE', `/api/widgets/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /widgets/:id    Should return with a 404, because we just deleted the widgets', async () => {
        const res = await api('GET', `/api/widgets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /widgets/:id    Should return with a 404, because we just deleted the widgets', async () => {
        const res = await api('DELETE', `/api/widgets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /widgets/:id    Should return with a 404, because we just deleted the widgets', async () => {
        const res = await api('PUT', `/api/widgets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
