import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/merchants', () => {

    const keys = [
        'tenantId', 'merchantId', 'externalId', 'prefix', 'name', 'mkey', 'status'
    ];

    const testData = {
        tenantId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        externalId: undefined, // TODO: Add test value
        prefix: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        mkey: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        tenantId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        externalId: undefined, // TODO: Add test value
        prefix: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        mkey: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /merchants        Should create a new merchant', async () => {
        const res = await api('POST', '/api/merchants', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /merchants        Should fail because we want to create a empty merchant', async () => {
        const res = await api('POST', '/api/merchants', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /merchants        Should list of merchants with our new create one', async () => {
        const res = await api('GET', '/api/merchants', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.externalId).toBe(testData.externalId);
        expect(result.prefix).toBe(testData.prefix);
        expect(result.name).toBe(testData.name);
        expect(result.mkey).toBe(testData.mkey);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /merchants/:id    Should return one merchant', async () => {
        const res = await api('GET', `/api/merchants/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.externalId).toBe(testData.externalId);
        expect(result.prefix).toBe(testData.prefix);
        expect(result.name).toBe(testData.name);
        expect(result.mkey).toBe(testData.mkey);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /merchants/:id    Should update the merchant', async () => {
        const res = await api('PUT', `/api/merchants/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.externalId).toBe(testDataUpdated.externalId);
        expect(result.prefix).toBe(testDataUpdated.prefix);
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.mkey).toBe(testDataUpdated.mkey);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /merchants/:id    Should fail because we want to update the merchant with a invalid email', async () => {
        const res = await api('PUT', `/api/merchants/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /merchants/:id    Should delete the merchant', async () => {
        const res = await api('DELETE', `/api/merchants/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /merchants/:id    Should return with a 404, because we just deleted the merchant', async () => {
        const res = await api('GET', `/api/merchants/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /merchants/:id    Should return with a 404, because we just deleted the merchant', async () => {
        const res = await api('DELETE', `/api/merchants/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /merchants/:id    Should return with a 404, because we just deleted the merchant', async () => {
        const res = await api('PUT', `/api/merchants/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
