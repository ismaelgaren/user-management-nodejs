import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/pos', () => {

    const keys = [
        'deviceId', 'deviceName', 'deviceBalance', 'walletId', 'status'
    ];

    const testData = {
        deviceId: undefined, // TODO: Add test value
        deviceName: undefined, // TODO: Add test value
        deviceBalance: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        deviceId: undefined, // TODO: Add test value
        deviceName: undefined, // TODO: Add test value
        deviceBalance: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /pos        Should create a new pos', async () => {
        const res = await api('POST', '/api/pos', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /pos        Should fail because we want to create a empty pos', async () => {
        const res = await api('POST', '/api/pos', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /pos        Should list of poss with our new create one', async () => {
        const res = await api('GET', '/api/pos', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.deviceId).toBe(testData.deviceId);
        expect(result.deviceName).toBe(testData.deviceName);
        expect(result.deviceBalance).toBe(testData.deviceBalance);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /pos/:id    Should return one pos', async () => {
        const res = await api('GET', `/api/pos/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.deviceId).toBe(testData.deviceId);
        expect(result.deviceName).toBe(testData.deviceName);
        expect(result.deviceBalance).toBe(testData.deviceBalance);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /pos/:id    Should update the pos', async () => {
        const res = await api('PUT', `/api/pos/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.deviceId).toBe(testDataUpdated.deviceId);
        expect(result.deviceName).toBe(testDataUpdated.deviceName);
        expect(result.deviceBalance).toBe(testDataUpdated.deviceBalance);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /pos/:id    Should fail because we want to update the pos with a invalid email', async () => {
        const res = await api('PUT', `/api/pos/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /pos/:id    Should delete the pos', async () => {
        const res = await api('DELETE', `/api/pos/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /pos/:id    Should return with a 404, because we just deleted the pos', async () => {
        const res = await api('GET', `/api/pos/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /pos/:id    Should return with a 404, because we just deleted the pos', async () => {
        const res = await api('DELETE', `/api/pos/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /pos/:id    Should return with a 404, because we just deleted the pos', async () => {
        const res = await api('PUT', `/api/pos/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
