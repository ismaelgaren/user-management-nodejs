import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/paymentrequestpayloads', () => {

    const keys = [
        'paymentRequestId', 'itemCode', 'quantity'
    ];

    const testData = {
        paymentRequestId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        quantity: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        paymentRequestId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        quantity: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /paymentrequestpayloads        Should create a new paymentrequestpayload', async () => {
        const res = await api('POST', '/api/paymentrequestpayloads', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /paymentrequestpayloads        Should fail because we want to create a empty paymentrequestpayload', async () => {
        const res = await api('POST', '/api/paymentrequestpayloads', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /paymentrequestpayloads        Should list of paymentrequestpayloads with our new create one', async () => {
        const res = await api('GET', '/api/paymentrequestpayloads', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.paymentRequestId).toBe(testData.paymentRequestId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.quantity).toBe(testData.quantity);
    });

    test('GET       /paymentrequestpayloads/:id    Should return one paymentrequestpayload', async () => {
        const res = await api('GET', `/api/paymentrequestpayloads/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.paymentRequestId).toBe(testData.paymentRequestId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.quantity).toBe(testData.quantity);
    });

    test('PUT       /paymentrequestpayloads/:id    Should update the paymentrequestpayload', async () => {
        const res = await api('PUT', `/api/paymentrequestpayloads/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.paymentRequestId).toBe(testDataUpdated.paymentRequestId);
        expect(result.itemCode).toBe(testDataUpdated.itemCode);
        expect(result.quantity).toBe(testDataUpdated.quantity);
    });

    test('PUT       /paymentrequestpayloads/:id    Should fail because we want to update the paymentrequestpayload with a invalid email', async () => {
        const res = await api('PUT', `/api/paymentrequestpayloads/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /paymentrequestpayloads/:id    Should delete the paymentrequestpayload', async () => {
        const res = await api('DELETE', `/api/paymentrequestpayloads/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /paymentrequestpayloads/:id    Should return with a 404, because we just deleted the paymentrequestpayload', async () => {
        const res = await api('GET', `/api/paymentrequestpayloads/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /paymentrequestpayloads/:id    Should return with a 404, because we just deleted the paymentrequestpayload', async () => {
        const res = await api('DELETE', `/api/paymentrequestpayloads/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /paymentrequestpayloads/:id    Should return with a 404, because we just deleted the paymentrequestpayload', async () => {
        const res = await api('PUT', `/api/paymentrequestpayloads/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
