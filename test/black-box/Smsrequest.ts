import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/smsrequests', () => {

    const keys = [
        'sendTo', 'content', 'otp', 'requestId'
    ];

    const testData = {
        sendTo: undefined, // TODO: Add test value
        content: undefined, // TODO: Add test value
        otp: undefined, // TODO: Add test value
        requestId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        sendTo: undefined, // TODO: Add test value
        content: undefined, // TODO: Add test value
        otp: undefined, // TODO: Add test value
        requestId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /smsrequests        Should create a new smsrequest', async () => {
        const res = await api('POST', '/api/smsrequests', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /smsrequests        Should fail because we want to create a empty smsrequest', async () => {
        const res = await api('POST', '/api/smsrequests', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /smsrequests        Should list of smsrequests with our new create one', async () => {
        const res = await api('GET', '/api/smsrequests', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.sendTo).toBe(testData.sendTo);
        expect(result.content).toBe(testData.content);
        expect(result.otp).toBe(testData.otp);
        expect(result.requestId).toBe(testData.requestId);
    });

    test('GET       /smsrequests/:id    Should return one smsrequest', async () => {
        const res = await api('GET', `/api/smsrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.sendTo).toBe(testData.sendTo);
        expect(result.content).toBe(testData.content);
        expect(result.otp).toBe(testData.otp);
        expect(result.requestId).toBe(testData.requestId);
    });

    test('PUT       /smsrequests/:id    Should update the smsrequest', async () => {
        const res = await api('PUT', `/api/smsrequests/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.sendTo).toBe(testDataUpdated.sendTo);
        expect(result.content).toBe(testDataUpdated.content);
        expect(result.otp).toBe(testDataUpdated.otp);
        expect(result.requestId).toBe(testDataUpdated.requestId);
    });

    test('PUT       /smsrequests/:id    Should fail because we want to update the smsrequest with a invalid email', async () => {
        const res = await api('PUT', `/api/smsrequests/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /smsrequests/:id    Should delete the smsrequest', async () => {
        const res = await api('DELETE', `/api/smsrequests/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /smsrequests/:id    Should return with a 404, because we just deleted the smsrequest', async () => {
        const res = await api('GET', `/api/smsrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /smsrequests/:id    Should return with a 404, because we just deleted the smsrequest', async () => {
        const res = await api('DELETE', `/api/smsrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /smsrequests/:id    Should return with a 404, because we just deleted the smsrequest', async () => {
        const res = await api('PUT', `/api/smsrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
