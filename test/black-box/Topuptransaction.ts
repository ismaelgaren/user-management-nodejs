import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/topuptransactions', () => {

    const keys = [
        'requestId', 'merchantId', 'walletId', 'krakenReqId', 'refNo', 'pchannel', 'pmethod', 'merchantName', 'amount', 'fee', 'vat', 'deductFee', 'finalAmount', 'mode', 'email', 'statusCode', 'statusText', 'tenantId', 'paymentActionInfo', 'directOtcInfo', 'paymentDate', 'expiryDate'
    ];

    const testData = {
        requestId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        krakenReqId: undefined, // TODO: Add test value
        refNo: undefined, // TODO: Add test value
        pchannel: undefined, // TODO: Add test value
        pmethod: undefined, // TODO: Add test value
        merchantName: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        fee: undefined, // TODO: Add test value
        vat: undefined, // TODO: Add test value
        deductFee: undefined, // TODO: Add test value
        finalAmount: undefined, // TODO: Add test value
        mode: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        statusCode: undefined, // TODO: Add test value
        statusText: undefined, // TODO: Add test value
        tenantId: undefined, // TODO: Add test value
        paymentActionInfo: undefined, // TODO: Add test value
        directOtcInfo: undefined, // TODO: Add test value
        paymentDate: undefined, // TODO: Add test value
        expiryDate: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        requestId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        krakenReqId: undefined, // TODO: Add test value
        refNo: undefined, // TODO: Add test value
        pchannel: undefined, // TODO: Add test value
        pmethod: undefined, // TODO: Add test value
        merchantName: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        fee: undefined, // TODO: Add test value
        vat: undefined, // TODO: Add test value
        deductFee: undefined, // TODO: Add test value
        finalAmount: undefined, // TODO: Add test value
        mode: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        statusCode: undefined, // TODO: Add test value
        statusText: undefined, // TODO: Add test value
        tenantId: undefined, // TODO: Add test value
        paymentActionInfo: undefined, // TODO: Add test value
        directOtcInfo: undefined, // TODO: Add test value
        paymentDate: undefined, // TODO: Add test value
        expiryDate: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /topuptransactions        Should create a new topuptransaction', async () => {
        const res = await api('POST', '/api/topuptransactions', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /topuptransactions        Should fail because we want to create a empty topuptransaction', async () => {
        const res = await api('POST', '/api/topuptransactions', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /topuptransactions        Should list of topuptransactions with our new create one', async () => {
        const res = await api('GET', '/api/topuptransactions', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.requestId).toBe(testData.requestId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.krakenReqId).toBe(testData.krakenReqId);
        expect(result.refNo).toBe(testData.refNo);
        expect(result.pchannel).toBe(testData.pchannel);
        expect(result.pmethod).toBe(testData.pmethod);
        expect(result.merchantName).toBe(testData.merchantName);
        expect(result.amount).toBe(testData.amount);
        expect(result.fee).toBe(testData.fee);
        expect(result.vat).toBe(testData.vat);
        expect(result.deductFee).toBe(testData.deductFee);
        expect(result.finalAmount).toBe(testData.finalAmount);
        expect(result.mode).toBe(testData.mode);
        expect(result.email).toBe(testData.email);
        expect(result.statusCode).toBe(testData.statusCode);
        expect(result.statusText).toBe(testData.statusText);
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.paymentActionInfo).toBe(testData.paymentActionInfo);
        expect(result.directOtcInfo).toBe(testData.directOtcInfo);
        expect(result.paymentDate).toBe(testData.paymentDate);
        expect(result.expiryDate).toBe(testData.expiryDate);
    });

    test('GET       /topuptransactions/:id    Should return one topuptransaction', async () => {
        const res = await api('GET', `/api/topuptransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testData.requestId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.krakenReqId).toBe(testData.krakenReqId);
        expect(result.refNo).toBe(testData.refNo);
        expect(result.pchannel).toBe(testData.pchannel);
        expect(result.pmethod).toBe(testData.pmethod);
        expect(result.merchantName).toBe(testData.merchantName);
        expect(result.amount).toBe(testData.amount);
        expect(result.fee).toBe(testData.fee);
        expect(result.vat).toBe(testData.vat);
        expect(result.deductFee).toBe(testData.deductFee);
        expect(result.finalAmount).toBe(testData.finalAmount);
        expect(result.mode).toBe(testData.mode);
        expect(result.email).toBe(testData.email);
        expect(result.statusCode).toBe(testData.statusCode);
        expect(result.statusText).toBe(testData.statusText);
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.paymentActionInfo).toBe(testData.paymentActionInfo);
        expect(result.directOtcInfo).toBe(testData.directOtcInfo);
        expect(result.paymentDate).toBe(testData.paymentDate);
        expect(result.expiryDate).toBe(testData.expiryDate);
    });

    test('PUT       /topuptransactions/:id    Should update the topuptransaction', async () => {
        const res = await api('PUT', `/api/topuptransactions/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testDataUpdated.requestId);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.krakenReqId).toBe(testDataUpdated.krakenReqId);
        expect(result.refNo).toBe(testDataUpdated.refNo);
        expect(result.pchannel).toBe(testDataUpdated.pchannel);
        expect(result.pmethod).toBe(testDataUpdated.pmethod);
        expect(result.merchantName).toBe(testDataUpdated.merchantName);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.fee).toBe(testDataUpdated.fee);
        expect(result.vat).toBe(testDataUpdated.vat);
        expect(result.deductFee).toBe(testDataUpdated.deductFee);
        expect(result.finalAmount).toBe(testDataUpdated.finalAmount);
        expect(result.mode).toBe(testDataUpdated.mode);
        expect(result.email).toBe(testDataUpdated.email);
        expect(result.statusCode).toBe(testDataUpdated.statusCode);
        expect(result.statusText).toBe(testDataUpdated.statusText);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.paymentActionInfo).toBe(testDataUpdated.paymentActionInfo);
        expect(result.directOtcInfo).toBe(testDataUpdated.directOtcInfo);
        expect(result.paymentDate).toBe(testDataUpdated.paymentDate);
        expect(result.expiryDate).toBe(testDataUpdated.expiryDate);
    });

    test('PUT       /topuptransactions/:id    Should fail because we want to update the topuptransaction with a invalid email', async () => {
        const res = await api('PUT', `/api/topuptransactions/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /topuptransactions/:id    Should delete the topuptransaction', async () => {
        const res = await api('DELETE', `/api/topuptransactions/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /topuptransactions/:id    Should return with a 404, because we just deleted the topuptransaction', async () => {
        const res = await api('GET', `/api/topuptransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /topuptransactions/:id    Should return with a 404, because we just deleted the topuptransaction', async () => {
        const res = await api('DELETE', `/api/topuptransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /topuptransactions/:id    Should return with a 404, because we just deleted the topuptransaction', async () => {
        const res = await api('PUT', `/api/topuptransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
