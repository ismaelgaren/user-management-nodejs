import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/recons', () => {

    const keys = [
        'reconId', 'uploadDate', 'status', 'bankAccountNo', 'transactionStartDate', 'transactionEndDate'
    ];

    const testData = {
        reconId: undefined, // TODO: Add test value
        uploadDate: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        transactionStartDate: undefined, // TODO: Add test value
        transactionEndDate: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        reconId: undefined, // TODO: Add test value
        uploadDate: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        transactionStartDate: undefined, // TODO: Add test value
        transactionEndDate: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /recons        Should create a new recon', async () => {
        const res = await api('POST', '/api/recons', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /recons        Should fail because we want to create a empty recon', async () => {
        const res = await api('POST', '/api/recons', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /recons        Should list of recons with our new create one', async () => {
        const res = await api('GET', '/api/recons', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.reconId).toBe(testData.reconId);
        expect(result.uploadDate).toBe(testData.uploadDate);
        expect(result.status).toBe(testData.status);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.transactionStartDate).toBe(testData.transactionStartDate);
        expect(result.transactionEndDate).toBe(testData.transactionEndDate);
    });

    test('GET       /recons/:id    Should return one recon', async () => {
        const res = await api('GET', `/api/recons/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testData.reconId);
        expect(result.uploadDate).toBe(testData.uploadDate);
        expect(result.status).toBe(testData.status);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.transactionStartDate).toBe(testData.transactionStartDate);
        expect(result.transactionEndDate).toBe(testData.transactionEndDate);
    });

    test('PUT       /recons/:id    Should update the recon', async () => {
        const res = await api('PUT', `/api/recons/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testDataUpdated.reconId);
        expect(result.uploadDate).toBe(testDataUpdated.uploadDate);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.bankAccountNo).toBe(testDataUpdated.bankAccountNo);
        expect(result.transactionStartDate).toBe(testDataUpdated.transactionStartDate);
        expect(result.transactionEndDate).toBe(testDataUpdated.transactionEndDate);
    });

    test('PUT       /recons/:id    Should fail because we want to update the recon with a invalid email', async () => {
        const res = await api('PUT', `/api/recons/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /recons/:id    Should delete the recon', async () => {
        const res = await api('DELETE', `/api/recons/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /recons/:id    Should return with a 404, because we just deleted the recon', async () => {
        const res = await api('GET', `/api/recons/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /recons/:id    Should return with a 404, because we just deleted the recon', async () => {
        const res = await api('DELETE', `/api/recons/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /recons/:id    Should return with a 404, because we just deleted the recon', async () => {
        const res = await api('PUT', `/api/recons/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
