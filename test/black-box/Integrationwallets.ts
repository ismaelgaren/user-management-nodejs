import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/integrationwallets', () => {

    const keys = [
        'walletId', 'integrationId', 'status'
    ];

    const testData = {
        walletId: undefined, // TODO: Add test value
        integrationId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        walletId: undefined, // TODO: Add test value
        integrationId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /integrationwallets        Should create a new integrationwallets', async () => {
        const res = await api('POST', '/api/integrationwallets', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /integrationwallets        Should fail because we want to create a empty integrationwallets', async () => {
        const res = await api('POST', '/api/integrationwallets', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /integrationwallets        Should list of integrationwalletss with our new create one', async () => {
        const res = await api('GET', '/api/integrationwallets', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.walletId).toBe(testData.walletId);
        expect(result.integrationId).toBe(testData.integrationId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /integrationwallets/:id    Should return one integrationwallets', async () => {
        const res = await api('GET', `/api/integrationwallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testData.walletId);
        expect(result.integrationId).toBe(testData.integrationId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /integrationwallets/:id    Should update the integrationwallets', async () => {
        const res = await api('PUT', `/api/integrationwallets/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.integrationId).toBe(testDataUpdated.integrationId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /integrationwallets/:id    Should fail because we want to update the integrationwallets with a invalid email', async () => {
        const res = await api('PUT', `/api/integrationwallets/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /integrationwallets/:id    Should delete the integrationwallets', async () => {
        const res = await api('DELETE', `/api/integrationwallets/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /integrationwallets/:id    Should return with a 404, because we just deleted the integrationwallets', async () => {
        const res = await api('GET', `/api/integrationwallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /integrationwallets/:id    Should return with a 404, because we just deleted the integrationwallets', async () => {
        const res = await api('DELETE', `/api/integrationwallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /integrationwallets/:id    Should return with a 404, because we just deleted the integrationwallets', async () => {
        const res = await api('PUT', `/api/integrationwallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
