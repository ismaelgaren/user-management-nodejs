import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/predictivebalstats', () => {

    const keys = [
        'bankAccountNo', 'totalDebit', 'totalCredit', 'predictiveBalance', 'bankName'
    ];

    const testData = {
        bankAccountNo: undefined, // TODO: Add test value
        totalDebit: undefined, // TODO: Add test value
        totalCredit: undefined, // TODO: Add test value
        predictiveBalance: undefined, // TODO: Add test value
        bankName: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        bankAccountNo: undefined, // TODO: Add test value
        totalDebit: undefined, // TODO: Add test value
        totalCredit: undefined, // TODO: Add test value
        predictiveBalance: undefined, // TODO: Add test value
        bankName: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /predictivebalstats        Should create a new predictivebalstats', async () => {
        const res = await api('POST', '/api/predictivebalstats', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /predictivebalstats        Should fail because we want to create a empty predictivebalstats', async () => {
        const res = await api('POST', '/api/predictivebalstats', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /predictivebalstats        Should list of predictivebalstatss with our new create one', async () => {
        const res = await api('GET', '/api/predictivebalstats', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.totalDebit).toBe(testData.totalDebit);
        expect(result.totalCredit).toBe(testData.totalCredit);
        expect(result.predictiveBalance).toBe(testData.predictiveBalance);
        expect(result.bankName).toBe(testData.bankName);
    });

    test('GET       /predictivebalstats/:id    Should return one predictivebalstats', async () => {
        const res = await api('GET', `/api/predictivebalstats/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.totalDebit).toBe(testData.totalDebit);
        expect(result.totalCredit).toBe(testData.totalCredit);
        expect(result.predictiveBalance).toBe(testData.predictiveBalance);
        expect(result.bankName).toBe(testData.bankName);
    });

    test('PUT       /predictivebalstats/:id    Should update the predictivebalstats', async () => {
        const res = await api('PUT', `/api/predictivebalstats/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.bankAccountNo).toBe(testDataUpdated.bankAccountNo);
        expect(result.totalDebit).toBe(testDataUpdated.totalDebit);
        expect(result.totalCredit).toBe(testDataUpdated.totalCredit);
        expect(result.predictiveBalance).toBe(testDataUpdated.predictiveBalance);
        expect(result.bankName).toBe(testDataUpdated.bankName);
    });

    test('PUT       /predictivebalstats/:id    Should fail because we want to update the predictivebalstats with a invalid email', async () => {
        const res = await api('PUT', `/api/predictivebalstats/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /predictivebalstats/:id    Should delete the predictivebalstats', async () => {
        const res = await api('DELETE', `/api/predictivebalstats/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /predictivebalstats/:id    Should return with a 404, because we just deleted the predictivebalstats', async () => {
        const res = await api('GET', `/api/predictivebalstats/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /predictivebalstats/:id    Should return with a 404, because we just deleted the predictivebalstats', async () => {
        const res = await api('DELETE', `/api/predictivebalstats/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /predictivebalstats/:id    Should return with a 404, because we just deleted the predictivebalstats', async () => {
        const res = await api('PUT', `/api/predictivebalstats/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
