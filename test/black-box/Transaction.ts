import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/transactions', () => {

    const keys = [
        'customerId', 'walletId', 'amount', 'type', 'description', 'deviceId'
    ];

    const testData = {
        customerId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        type: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        deviceId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        customerId: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        type: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        deviceId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /transactions        Should create a new transaction', async () => {
        const res = await api('POST', '/api/transactions', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /transactions        Should fail because we want to create a empty transaction', async () => {
        const res = await api('POST', '/api/transactions', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /transactions        Should list of transactions with our new create one', async () => {
        const res = await api('GET', '/api/transactions', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.customerId).toBe(testData.customerId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.amount).toBe(testData.amount);
        expect(result.type).toBe(testData.type);
        expect(result.description).toBe(testData.description);
        expect(result.deviceId).toBe(testData.deviceId);
    });

    test('GET       /transactions/:id    Should return one transaction', async () => {
        const res = await api('GET', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.customerId).toBe(testData.customerId);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.amount).toBe(testData.amount);
        expect(result.type).toBe(testData.type);
        expect(result.description).toBe(testData.description);
        expect(result.deviceId).toBe(testData.deviceId);
    });

    test('PUT       /transactions/:id    Should update the transaction', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.customerId).toBe(testDataUpdated.customerId);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.type).toBe(testDataUpdated.type);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.deviceId).toBe(testDataUpdated.deviceId);
    });

    test('PUT       /transactions/:id    Should fail because we want to update the transaction with a invalid email', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /transactions/:id    Should delete the transaction', async () => {
        const res = await api('DELETE', `/api/transactions/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /transactions/:id    Should return with a 404, because we just deleted the transaction', async () => {
        const res = await api('GET', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /transactions/:id    Should return with a 404, because we just deleted the transaction', async () => {
        const res = await api('DELETE', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /transactions/:id    Should return with a 404, because we just deleted the transaction', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
