import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/flagitems', () => {

    const keys = [
        'groupId', 'trxId', 'amount', 'trxDate', 'description', 'trxType', 'status'
    ];

    const testData = {
        groupId: undefined, // TODO: Add test value
        trxId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        trxDate: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        groupId: undefined, // TODO: Add test value
        trxId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        trxDate: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /flagitems        Should create a new flagitem', async () => {
        const res = await api('POST', '/api/flagitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /flagitems        Should fail because we want to create a empty flagitem', async () => {
        const res = await api('POST', '/api/flagitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /flagitems        Should list of flagitems with our new create one', async () => {
        const res = await api('GET', '/api/flagitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.groupId).toBe(testData.groupId);
        expect(result.trxId).toBe(testData.trxId);
        expect(result.amount).toBe(testData.amount);
        expect(result.trxDate).toBe(testData.trxDate);
        expect(result.description).toBe(testData.description);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /flagitems/:id    Should return one flagitem', async () => {
        const res = await api('GET', `/api/flagitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testData.groupId);
        expect(result.trxId).toBe(testData.trxId);
        expect(result.amount).toBe(testData.amount);
        expect(result.trxDate).toBe(testData.trxDate);
        expect(result.description).toBe(testData.description);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /flagitems/:id    Should update the flagitem', async () => {
        const res = await api('PUT', `/api/flagitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testDataUpdated.groupId);
        expect(result.trxId).toBe(testDataUpdated.trxId);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.trxDate).toBe(testDataUpdated.trxDate);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.trxType).toBe(testDataUpdated.trxType);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /flagitems/:id    Should fail because we want to update the flagitem with a invalid email', async () => {
        const res = await api('PUT', `/api/flagitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /flagitems/:id    Should delete the flagitem', async () => {
        const res = await api('DELETE', `/api/flagitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /flagitems/:id    Should return with a 404, because we just deleted the flagitem', async () => {
        const res = await api('GET', `/api/flagitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /flagitems/:id    Should return with a 404, because we just deleted the flagitem', async () => {
        const res = await api('DELETE', `/api/flagitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /flagitems/:id    Should return with a 404, because we just deleted the flagitem', async () => {
        const res = await api('PUT', `/api/flagitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
