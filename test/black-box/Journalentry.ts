import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/journalentries', () => {

    const keys = [
        'externalReference', 'walletId', 'transactionType', 'transactionAmount', 'walletBalanceBefore', 'walletBalanceAfter', 'remarks'
    ];

    const testData = {
        externalReference: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        transactionAmount: undefined, // TODO: Add test value
        walletBalanceBefore: undefined, // TODO: Add test value
        walletBalanceAfter: undefined, // TODO: Add test value
        remarks: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        externalReference: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        transactionAmount: undefined, // TODO: Add test value
        walletBalanceBefore: undefined, // TODO: Add test value
        walletBalanceAfter: undefined, // TODO: Add test value
        remarks: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /journalentries        Should create a new journalentry', async () => {
        const res = await api('POST', '/api/journalentries', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /journalentries        Should fail because we want to create a empty journalentry', async () => {
        const res = await api('POST', '/api/journalentries', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /journalentries        Should list of journalentrys with our new create one', async () => {
        const res = await api('GET', '/api/journalentries', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.externalReference).toBe(testData.externalReference);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.transactionAmount).toBe(testData.transactionAmount);
        expect(result.walletBalanceBefore).toBe(testData.walletBalanceBefore);
        expect(result.walletBalanceAfter).toBe(testData.walletBalanceAfter);
        expect(result.remarks).toBe(testData.remarks);
    });

    test('GET       /journalentries/:id    Should return one journalentry', async () => {
        const res = await api('GET', `/api/journalentries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.externalReference).toBe(testData.externalReference);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.transactionAmount).toBe(testData.transactionAmount);
        expect(result.walletBalanceBefore).toBe(testData.walletBalanceBefore);
        expect(result.walletBalanceAfter).toBe(testData.walletBalanceAfter);
        expect(result.remarks).toBe(testData.remarks);
    });

    test('PUT       /journalentries/:id    Should update the journalentry', async () => {
        const res = await api('PUT', `/api/journalentries/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.externalReference).toBe(testDataUpdated.externalReference);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.transactionAmount).toBe(testDataUpdated.transactionAmount);
        expect(result.walletBalanceBefore).toBe(testDataUpdated.walletBalanceBefore);
        expect(result.walletBalanceAfter).toBe(testDataUpdated.walletBalanceAfter);
        expect(result.remarks).toBe(testDataUpdated.remarks);
    });

    test('PUT       /journalentries/:id    Should fail because we want to update the journalentry with a invalid email', async () => {
        const res = await api('PUT', `/api/journalentries/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /journalentries/:id    Should delete the journalentry', async () => {
        const res = await api('DELETE', `/api/journalentries/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /journalentries/:id    Should return with a 404, because we just deleted the journalentry', async () => {
        const res = await api('GET', `/api/journalentries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /journalentries/:id    Should return with a 404, because we just deleted the journalentry', async () => {
        const res = await api('DELETE', `/api/journalentries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /journalentries/:id    Should return with a 404, because we just deleted the journalentry', async () => {
        const res = await api('PUT', `/api/journalentries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
