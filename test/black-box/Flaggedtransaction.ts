import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/flaggedtransactions', () => {

    const keys = [
        'merchantId', 'transactionId', 'flaggedStatus', 'violation', 'policyId'
    ];

    const testData = {
        merchantId: undefined, // TODO: Add test value
        transactionId: undefined, // TODO: Add test value
        flaggedStatus: undefined, // TODO: Add test value
        violation: undefined, // TODO: Add test value
        policyId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        merchantId: undefined, // TODO: Add test value
        transactionId: undefined, // TODO: Add test value
        flaggedStatus: undefined, // TODO: Add test value
        violation: undefined, // TODO: Add test value
        policyId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /flaggedtransactions        Should create a new flaggedtransaction', async () => {
        const res = await api('POST', '/api/flaggedtransactions', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /flaggedtransactions        Should fail because we want to create a empty flaggedtransaction', async () => {
        const res = await api('POST', '/api/flaggedtransactions', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /flaggedtransactions        Should list of flaggedtransactions with our new create one', async () => {
        const res = await api('GET', '/api/flaggedtransactions', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.flaggedStatus).toBe(testData.flaggedStatus);
        expect(result.violation).toBe(testData.violation);
        expect(result.policyId).toBe(testData.policyId);
    });

    test('GET       /flaggedtransactions/:id    Should return one flaggedtransaction', async () => {
        const res = await api('GET', `/api/flaggedtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.flaggedStatus).toBe(testData.flaggedStatus);
        expect(result.violation).toBe(testData.violation);
        expect(result.policyId).toBe(testData.policyId);
    });

    test('PUT       /flaggedtransactions/:id    Should update the flaggedtransaction', async () => {
        const res = await api('PUT', `/api/flaggedtransactions/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.transactionId).toBe(testDataUpdated.transactionId);
        expect(result.flaggedStatus).toBe(testDataUpdated.flaggedStatus);
        expect(result.violation).toBe(testDataUpdated.violation);
        expect(result.policyId).toBe(testDataUpdated.policyId);
    });

    test('PUT       /flaggedtransactions/:id    Should fail because we want to update the flaggedtransaction with a invalid email', async () => {
        const res = await api('PUT', `/api/flaggedtransactions/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /flaggedtransactions/:id    Should delete the flaggedtransaction', async () => {
        const res = await api('DELETE', `/api/flaggedtransactions/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /flaggedtransactions/:id    Should return with a 404, because we just deleted the flaggedtransaction', async () => {
        const res = await api('GET', `/api/flaggedtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /flaggedtransactions/:id    Should return with a 404, because we just deleted the flaggedtransaction', async () => {
        const res = await api('DELETE', `/api/flaggedtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /flaggedtransactions/:id    Should return with a 404, because we just deleted the flaggedtransaction', async () => {
        const res = await api('PUT', `/api/flaggedtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
