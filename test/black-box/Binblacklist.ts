import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/binblacklists', () => {

    const keys = [
        'tenantId', 'country', 'transactionType', 'status'
    ];

    const testData = {
        tenantId: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        tenantId: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        transactionType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /binblacklists        Should create a new binblacklist', async () => {
        const res = await api('POST', '/api/binblacklists', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /binblacklists        Should fail because we want to create a empty binblacklist', async () => {
        const res = await api('POST', '/api/binblacklists', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /binblacklists        Should list of binblacklists with our new create one', async () => {
        const res = await api('GET', '/api/binblacklists', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.country).toBe(testData.country);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /binblacklists/:id    Should return one binblacklist', async () => {
        const res = await api('GET', `/api/binblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.country).toBe(testData.country);
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /binblacklists/:id    Should update the binblacklist', async () => {
        const res = await api('PUT', `/api/binblacklists/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.country).toBe(testDataUpdated.country);
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /binblacklists/:id    Should fail because we want to update the binblacklist with a invalid email', async () => {
        const res = await api('PUT', `/api/binblacklists/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /binblacklists/:id    Should delete the binblacklist', async () => {
        const res = await api('DELETE', `/api/binblacklists/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /binblacklists/:id    Should return with a 404, because we just deleted the binblacklist', async () => {
        const res = await api('GET', `/api/binblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /binblacklists/:id    Should return with a 404, because we just deleted the binblacklist', async () => {
        const res = await api('DELETE', `/api/binblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /binblacklists/:id    Should return with a 404, because we just deleted the binblacklist', async () => {
        const res = await api('PUT', `/api/binblacklists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
