import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/docsauths', () => {

    const keys = [
        'authUser', 'docsId', 'status'
    ];

    const testData = {
        authUser: undefined, // TODO: Add test value
        docsId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        authUser: undefined, // TODO: Add test value
        docsId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /docsauths        Should create a new docsauth', async () => {
        const res = await api('POST', '/api/docsauths', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /docsauths        Should fail because we want to create a empty docsauth', async () => {
        const res = await api('POST', '/api/docsauths', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /docsauths        Should list of docsauths with our new create one', async () => {
        const res = await api('GET', '/api/docsauths', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.authUser).toBe(testData.authUser);
        expect(result.docsId).toBe(testData.docsId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /docsauths/:id    Should return one docsauth', async () => {
        const res = await api('GET', `/api/docsauths/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.authUser).toBe(testData.authUser);
        expect(result.docsId).toBe(testData.docsId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /docsauths/:id    Should update the docsauth', async () => {
        const res = await api('PUT', `/api/docsauths/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.authUser).toBe(testDataUpdated.authUser);
        expect(result.docsId).toBe(testDataUpdated.docsId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /docsauths/:id    Should fail because we want to update the docsauth with a invalid email', async () => {
        const res = await api('PUT', `/api/docsauths/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /docsauths/:id    Should delete the docsauth', async () => {
        const res = await api('DELETE', `/api/docsauths/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /docsauths/:id    Should return with a 404, because we just deleted the docsauth', async () => {
        const res = await api('GET', `/api/docsauths/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /docsauths/:id    Should return with a 404, because we just deleted the docsauth', async () => {
        const res = await api('DELETE', `/api/docsauths/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /docsauths/:id    Should return with a 404, because we just deleted the docsauth', async () => {
        const res = await api('PUT', `/api/docsauths/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
