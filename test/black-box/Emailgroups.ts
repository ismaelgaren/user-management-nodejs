import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/emailgroups', () => {

    const keys = [
        'groupName', 'status'
    ];

    const testData = {
        groupName: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        groupName: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /emailgroups        Should create a new emailgroups', async () => {
        const res = await api('POST', '/api/emailgroups', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /emailgroups        Should fail because we want to create a empty emailgroups', async () => {
        const res = await api('POST', '/api/emailgroups', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /emailgroups        Should list of emailgroupss with our new create one', async () => {
        const res = await api('GET', '/api/emailgroups', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.groupName).toBe(testData.groupName);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /emailgroups/:id    Should return one emailgroups', async () => {
        const res = await api('GET', `/api/emailgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupName).toBe(testData.groupName);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /emailgroups/:id    Should update the emailgroups', async () => {
        const res = await api('PUT', `/api/emailgroups/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupName).toBe(testDataUpdated.groupName);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /emailgroups/:id    Should fail because we want to update the emailgroups with a invalid email', async () => {
        const res = await api('PUT', `/api/emailgroups/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /emailgroups/:id    Should delete the emailgroups', async () => {
        const res = await api('DELETE', `/api/emailgroups/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /emailgroups/:id    Should return with a 404, because we just deleted the emailgroups', async () => {
        const res = await api('GET', `/api/emailgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /emailgroups/:id    Should return with a 404, because we just deleted the emailgroups', async () => {
        const res = await api('DELETE', `/api/emailgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /emailgroups/:id    Should return with a 404, because we just deleted the emailgroups', async () => {
        const res = await api('PUT', `/api/emailgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
