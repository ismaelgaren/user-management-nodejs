import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/suspicious', () => {

    const keys = [
        'trxId', 'reqId', 'trxType', 'paymentType', 'customerName', 'status'
    ];

    const testData = {
        trxId: undefined, // TODO: Add test value
        reqId: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        paymentType: undefined, // TODO: Add test value
        customerName: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        trxId: undefined, // TODO: Add test value
        reqId: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        paymentType: undefined, // TODO: Add test value
        customerName: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /suspicious        Should create a new suspicious', async () => {
        const res = await api('POST', '/api/suspicious', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /suspicious        Should fail because we want to create a empty suspicious', async () => {
        const res = await api('POST', '/api/suspicious', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /suspicious        Should list of suspiciouss with our new create one', async () => {
        const res = await api('GET', '/api/suspicious', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.trxId).toBe(testData.trxId);
        expect(result.reqId).toBe(testData.reqId);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.paymentType).toBe(testData.paymentType);
        expect(result.customerName).toBe(testData.customerName);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /suspicious/:id    Should return one suspicious', async () => {
        const res = await api('GET', `/api/suspicious/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.trxId).toBe(testData.trxId);
        expect(result.reqId).toBe(testData.reqId);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.paymentType).toBe(testData.paymentType);
        expect(result.customerName).toBe(testData.customerName);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /suspicious/:id    Should update the suspicious', async () => {
        const res = await api('PUT', `/api/suspicious/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.trxId).toBe(testDataUpdated.trxId);
        expect(result.reqId).toBe(testDataUpdated.reqId);
        expect(result.trxType).toBe(testDataUpdated.trxType);
        expect(result.paymentType).toBe(testDataUpdated.paymentType);
        expect(result.customerName).toBe(testDataUpdated.customerName);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /suspicious/:id    Should fail because we want to update the suspicious with a invalid email', async () => {
        const res = await api('PUT', `/api/suspicious/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /suspicious/:id    Should delete the suspicious', async () => {
        const res = await api('DELETE', `/api/suspicious/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /suspicious/:id    Should return with a 404, because we just deleted the suspicious', async () => {
        const res = await api('GET', `/api/suspicious/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /suspicious/:id    Should return with a 404, because we just deleted the suspicious', async () => {
        const res = await api('DELETE', `/api/suspicious/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /suspicious/:id    Should return with a 404, because we just deleted the suspicious', async () => {
        const res = await api('PUT', `/api/suspicious/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
