import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/scheduledtasks', () => {

    const keys = [
        'taskName', 'cronDate', 'scheduledDate', 'status'
    ];

    const testData = {
        taskName: undefined, // TODO: Add test value
        cronDate: undefined, // TODO: Add test value
        scheduledDate: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        taskName: undefined, // TODO: Add test value
        cronDate: undefined, // TODO: Add test value
        scheduledDate: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /scheduledtasks        Should create a new scheduledtasks', async () => {
        const res = await api('POST', '/api/scheduledtasks', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /scheduledtasks        Should fail because we want to create a empty scheduledtasks', async () => {
        const res = await api('POST', '/api/scheduledtasks', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /scheduledtasks        Should list of scheduledtaskss with our new create one', async () => {
        const res = await api('GET', '/api/scheduledtasks', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.taskName).toBe(testData.taskName);
        expect(result.cronDate).toBe(testData.cronDate);
        expect(result.scheduledDate).toBe(testData.scheduledDate);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /scheduledtasks/:id    Should return one scheduledtasks', async () => {
        const res = await api('GET', `/api/scheduledtasks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.taskName).toBe(testData.taskName);
        expect(result.cronDate).toBe(testData.cronDate);
        expect(result.scheduledDate).toBe(testData.scheduledDate);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /scheduledtasks/:id    Should update the scheduledtasks', async () => {
        const res = await api('PUT', `/api/scheduledtasks/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.taskName).toBe(testDataUpdated.taskName);
        expect(result.cronDate).toBe(testDataUpdated.cronDate);
        expect(result.scheduledDate).toBe(testDataUpdated.scheduledDate);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /scheduledtasks/:id    Should fail because we want to update the scheduledtasks with a invalid email', async () => {
        const res = await api('PUT', `/api/scheduledtasks/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /scheduledtasks/:id    Should delete the scheduledtasks', async () => {
        const res = await api('DELETE', `/api/scheduledtasks/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /scheduledtasks/:id    Should return with a 404, because we just deleted the scheduledtasks', async () => {
        const res = await api('GET', `/api/scheduledtasks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /scheduledtasks/:id    Should return with a 404, because we just deleted the scheduledtasks', async () => {
        const res = await api('DELETE', `/api/scheduledtasks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /scheduledtasks/:id    Should return with a 404, because we just deleted the scheduledtasks', async () => {
        const res = await api('PUT', `/api/scheduledtasks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
