import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policycardcountryissueritems', () => {

    const keys = [
        'policyItemId', 'iboundParamId', 'condition', 'countryId', 'violationRespCode', 'status', 'policyCardCountryIssuerItems'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        iboundParamId: undefined, // TODO: Add test value
        condition: undefined, // TODO: Add test value
        countryId: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        policyCardCountryIssuerItems: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        iboundParamId: undefined, // TODO: Add test value
        condition: undefined, // TODO: Add test value
        countryId: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        policyCardCountryIssuerItems: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policycardcountryissueritems        Should create a new policycardcountryissueritems', async () => {
        const res = await api('POST', '/api/policycardcountryissueritems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policycardcountryissueritems        Should fail because we want to create a empty policycardcountryissueritems', async () => {
        const res = await api('POST', '/api/policycardcountryissueritems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policycardcountryissueritems        Should list of policycardcountryissueritemss with our new create one', async () => {
        const res = await api('GET', '/api/policycardcountryissueritems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.iboundParamId).toBe(testData.iboundParamId);
        expect(result.condition).toBe(testData.condition);
        expect(result.countryId).toBe(testData.countryId);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
        expect(result.policyCardCountryIssuerItems).toBe(testData.policyCardCountryIssuerItems);
    });

    test('GET       /policycardcountryissueritems/:id    Should return one policycardcountryissueritems', async () => {
        const res = await api('GET', `/api/policycardcountryissueritems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.iboundParamId).toBe(testData.iboundParamId);
        expect(result.condition).toBe(testData.condition);
        expect(result.countryId).toBe(testData.countryId);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
        expect(result.policyCardCountryIssuerItems).toBe(testData.policyCardCountryIssuerItems);
    });

    test('PUT       /policycardcountryissueritems/:id    Should update the policycardcountryissueritems', async () => {
        const res = await api('PUT', `/api/policycardcountryissueritems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.iboundParamId).toBe(testDataUpdated.iboundParamId);
        expect(result.condition).toBe(testDataUpdated.condition);
        expect(result.countryId).toBe(testDataUpdated.countryId);
        expect(result.violationRespCode).toBe(testDataUpdated.violationRespCode);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.policyCardCountryIssuerItems).toBe(testDataUpdated.policyCardCountryIssuerItems);
    });

    test('PUT       /policycardcountryissueritems/:id    Should fail because we want to update the policycardcountryissueritems with a invalid email', async () => {
        const res = await api('PUT', `/api/policycardcountryissueritems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policycardcountryissueritems/:id    Should delete the policycardcountryissueritems', async () => {
        const res = await api('DELETE', `/api/policycardcountryissueritems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policycardcountryissueritems/:id    Should return with a 404, because we just deleted the policycardcountryissueritems', async () => {
        const res = await api('GET', `/api/policycardcountryissueritems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policycardcountryissueritems/:id    Should return with a 404, because we just deleted the policycardcountryissueritems', async () => {
        const res = await api('DELETE', `/api/policycardcountryissueritems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policycardcountryissueritems/:id    Should return with a 404, because we just deleted the policycardcountryissueritems', async () => {
        const res = await api('PUT', `/api/policycardcountryissueritems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
