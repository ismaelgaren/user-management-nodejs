import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/industries', () => {

    const keys = [
        'industry', 'code', 'status'
    ];

    const testData = {
        industry: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        industry: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /industries        Should create a new industry', async () => {
        const res = await api('POST', '/api/industries', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /industries        Should fail because we want to create a empty industry', async () => {
        const res = await api('POST', '/api/industries', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /industries        Should list of industrys with our new create one', async () => {
        const res = await api('GET', '/api/industries', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.industry).toBe(testData.industry);
        expect(result.code).toBe(testData.code);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /industries/:id    Should return one industry', async () => {
        const res = await api('GET', `/api/industries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.industry).toBe(testData.industry);
        expect(result.code).toBe(testData.code);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /industries/:id    Should update the industry', async () => {
        const res = await api('PUT', `/api/industries/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.industry).toBe(testDataUpdated.industry);
        expect(result.code).toBe(testDataUpdated.code);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /industries/:id    Should fail because we want to update the industry with a invalid email', async () => {
        const res = await api('PUT', `/api/industries/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /industries/:id    Should delete the industry', async () => {
        const res = await api('DELETE', `/api/industries/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /industries/:id    Should return with a 404, because we just deleted the industry', async () => {
        const res = await api('GET', `/api/industries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /industries/:id    Should return with a 404, because we just deleted the industry', async () => {
        const res = await api('DELETE', `/api/industries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /industries/:id    Should return with a 404, because we just deleted the industry', async () => {
        const res = await api('PUT', `/api/industries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
