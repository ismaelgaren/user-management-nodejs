import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/paymentrequests', () => {

    const keys = [
        'requestId', 'amount', 'deviceId', 'status'
    ];

    const testData = {
        requestId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        deviceId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        requestId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        deviceId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /paymentrequests        Should create a new paymentrequest', async () => {
        const res = await api('POST', '/api/paymentrequests', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /paymentrequests        Should fail because we want to create a empty paymentrequest', async () => {
        const res = await api('POST', '/api/paymentrequests', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /paymentrequests        Should list of paymentrequests with our new create one', async () => {
        const res = await api('GET', '/api/paymentrequests', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.requestId).toBe(testData.requestId);
        expect(result.amount).toBe(testData.amount);
        expect(result.deviceId).toBe(testData.deviceId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /paymentrequests/:id    Should return one paymentrequest', async () => {
        const res = await api('GET', `/api/paymentrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testData.requestId);
        expect(result.amount).toBe(testData.amount);
        expect(result.deviceId).toBe(testData.deviceId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /paymentrequests/:id    Should update the paymentrequest', async () => {
        const res = await api('PUT', `/api/paymentrequests/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testDataUpdated.requestId);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.deviceId).toBe(testDataUpdated.deviceId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /paymentrequests/:id    Should fail because we want to update the paymentrequest with a invalid email', async () => {
        const res = await api('PUT', `/api/paymentrequests/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /paymentrequests/:id    Should delete the paymentrequest', async () => {
        const res = await api('DELETE', `/api/paymentrequests/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /paymentrequests/:id    Should return with a 404, because we just deleted the paymentrequest', async () => {
        const res = await api('GET', `/api/paymentrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /paymentrequests/:id    Should return with a 404, because we just deleted the paymentrequest', async () => {
        const res = await api('DELETE', `/api/paymentrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /paymentrequests/:id    Should return with a 404, because we just deleted the paymentrequest', async () => {
        const res = await api('PUT', `/api/paymentrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
