import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policycountryitems', () => {

    const keys = [
        'policyItemId', 'status', 'parameter', 'country', 'allowed'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        allowed: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        allowed: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policycountryitems        Should create a new policycountryitem', async () => {
        const res = await api('POST', '/api/policycountryitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policycountryitems        Should fail because we want to create a empty policycountryitem', async () => {
        const res = await api('POST', '/api/policycountryitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policycountryitems        Should list of policycountryitems with our new create one', async () => {
        const res = await api('GET', '/api/policycountryitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.country).toBe(testData.country);
        expect(result.allowed).toBe(testData.allowed);
    });

    test('GET       /policycountryitems/:id    Should return one policycountryitem', async () => {
        const res = await api('GET', `/api/policycountryitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.country).toBe(testData.country);
        expect(result.allowed).toBe(testData.allowed);
    });

    test('PUT       /policycountryitems/:id    Should update the policycountryitem', async () => {
        const res = await api('PUT', `/api/policycountryitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.parameter).toBe(testDataUpdated.parameter);
        expect(result.country).toBe(testDataUpdated.country);
        expect(result.allowed).toBe(testDataUpdated.allowed);
    });

    test('PUT       /policycountryitems/:id    Should fail because we want to update the policycountryitem with a invalid email', async () => {
        const res = await api('PUT', `/api/policycountryitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policycountryitems/:id    Should delete the policycountryitem', async () => {
        const res = await api('DELETE', `/api/policycountryitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policycountryitems/:id    Should return with a 404, because we just deleted the policycountryitem', async () => {
        const res = await api('GET', `/api/policycountryitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policycountryitems/:id    Should return with a 404, because we just deleted the policycountryitem', async () => {
        const res = await api('DELETE', `/api/policycountryitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policycountryitems/:id    Should return with a 404, because we just deleted the policycountryitem', async () => {
        const res = await api('PUT', `/api/policycountryitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
