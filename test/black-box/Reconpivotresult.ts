import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/reconpivotresults', () => {

    const keys = [
        'reconId', 'transactionDate', 'amount', 'bankCount', 'centinelCount', 'difference', 'discrepancyAmount'
    ];

    const testData = {
        reconId: undefined, // TODO: Add test value
        transactionDate: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        bankCount: undefined, // TODO: Add test value
        centinelCount: undefined, // TODO: Add test value
        difference: undefined, // TODO: Add test value
        discrepancyAmount: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        reconId: undefined, // TODO: Add test value
        transactionDate: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        bankCount: undefined, // TODO: Add test value
        centinelCount: undefined, // TODO: Add test value
        difference: undefined, // TODO: Add test value
        discrepancyAmount: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /reconpivotresults        Should create a new reconpivotresult', async () => {
        const res = await api('POST', '/api/reconpivotresults', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /reconpivotresults        Should fail because we want to create a empty reconpivotresult', async () => {
        const res = await api('POST', '/api/reconpivotresults', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /reconpivotresults        Should list of reconpivotresults with our new create one', async () => {
        const res = await api('GET', '/api/reconpivotresults', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.reconId).toBe(testData.reconId);
        expect(result.transactionDate).toBe(testData.transactionDate);
        expect(result.amount).toBe(testData.amount);
        expect(result.bankCount).toBe(testData.bankCount);
        expect(result.centinelCount).toBe(testData.centinelCount);
        expect(result.difference).toBe(testData.difference);
        expect(result.discrepancyAmount).toBe(testData.discrepancyAmount);
    });

    test('GET       /reconpivotresults/:id    Should return one reconpivotresult', async () => {
        const res = await api('GET', `/api/reconpivotresults/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testData.reconId);
        expect(result.transactionDate).toBe(testData.transactionDate);
        expect(result.amount).toBe(testData.amount);
        expect(result.bankCount).toBe(testData.bankCount);
        expect(result.centinelCount).toBe(testData.centinelCount);
        expect(result.difference).toBe(testData.difference);
        expect(result.discrepancyAmount).toBe(testData.discrepancyAmount);
    });

    test('PUT       /reconpivotresults/:id    Should update the reconpivotresult', async () => {
        const res = await api('PUT', `/api/reconpivotresults/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.reconId).toBe(testDataUpdated.reconId);
        expect(result.transactionDate).toBe(testDataUpdated.transactionDate);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.bankCount).toBe(testDataUpdated.bankCount);
        expect(result.centinelCount).toBe(testDataUpdated.centinelCount);
        expect(result.difference).toBe(testDataUpdated.difference);
        expect(result.discrepancyAmount).toBe(testDataUpdated.discrepancyAmount);
    });

    test('PUT       /reconpivotresults/:id    Should fail because we want to update the reconpivotresult with a invalid email', async () => {
        const res = await api('PUT', `/api/reconpivotresults/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /reconpivotresults/:id    Should delete the reconpivotresult', async () => {
        const res = await api('DELETE', `/api/reconpivotresults/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /reconpivotresults/:id    Should return with a 404, because we just deleted the reconpivotresult', async () => {
        const res = await api('GET', `/api/reconpivotresults/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /reconpivotresults/:id    Should return with a 404, because we just deleted the reconpivotresult', async () => {
        const res = await api('DELETE', `/api/reconpivotresults/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /reconpivotresults/:id    Should return with a 404, because we just deleted the reconpivotresult', async () => {
        const res = await api('PUT', `/api/reconpivotresults/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
