import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policyvelocityitems', () => {

    const keys = [
        'policyItemId', 'inboundParamId', 'limitation', 'frequency', 'frequencyUnit', 'violationResponseCode', 'status'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        limitation: undefined, // TODO: Add test value
        frequency: undefined, // TODO: Add test value
        frequencyUnit: undefined, // TODO: Add test value
        violationResponseCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        limitation: undefined, // TODO: Add test value
        frequency: undefined, // TODO: Add test value
        frequencyUnit: undefined, // TODO: Add test value
        violationResponseCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policyvelocityitems        Should create a new policyvelocityitems', async () => {
        const res = await api('POST', '/api/policyvelocityitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policyvelocityitems        Should fail because we want to create a empty policyvelocityitems', async () => {
        const res = await api('POST', '/api/policyvelocityitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policyvelocityitems        Should list of policyvelocityitemss with our new create one', async () => {
        const res = await api('GET', '/api/policyvelocityitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.limitation).toBe(testData.limitation);
        expect(result.frequency).toBe(testData.frequency);
        expect(result.frequencyUnit).toBe(testData.frequencyUnit);
        expect(result.violationResponseCode).toBe(testData.violationResponseCode);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policyvelocityitems/:id    Should return one policyvelocityitems', async () => {
        const res = await api('GET', `/api/policyvelocityitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.limitation).toBe(testData.limitation);
        expect(result.frequency).toBe(testData.frequency);
        expect(result.frequencyUnit).toBe(testData.frequencyUnit);
        expect(result.violationResponseCode).toBe(testData.violationResponseCode);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policyvelocityitems/:id    Should update the policyvelocityitems', async () => {
        const res = await api('PUT', `/api/policyvelocityitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.inboundParamId).toBe(testDataUpdated.inboundParamId);
        expect(result.limitation).toBe(testDataUpdated.limitation);
        expect(result.frequency).toBe(testDataUpdated.frequency);
        expect(result.frequencyUnit).toBe(testDataUpdated.frequencyUnit);
        expect(result.violationResponseCode).toBe(testDataUpdated.violationResponseCode);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policyvelocityitems/:id    Should fail because we want to update the policyvelocityitems with a invalid email', async () => {
        const res = await api('PUT', `/api/policyvelocityitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policyvelocityitems/:id    Should delete the policyvelocityitems', async () => {
        const res = await api('DELETE', `/api/policyvelocityitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policyvelocityitems/:id    Should return with a 404, because we just deleted the policyvelocityitems', async () => {
        const res = await api('GET', `/api/policyvelocityitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policyvelocityitems/:id    Should return with a 404, because we just deleted the policyvelocityitems', async () => {
        const res = await api('DELETE', `/api/policyvelocityitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policyvelocityitems/:id    Should return with a 404, because we just deleted the policyvelocityitems', async () => {
        const res = await api('PUT', `/api/policyvelocityitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
