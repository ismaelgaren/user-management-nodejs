import {encrypt} from '../../../src/api/helpers/encryption';
import {Knex} from '../../../src/config/Database';
import {Tables} from '../../../src/constants/Tables';
import {api} from './api';

export const USER = {
  firstName: 'Paynamics',
  lastName: 'Admin',
  email: 'admin@paynamics.com',
  auth0UserId: 'administrator',
};

// export const createAdminUser = async () => {
//   const knex = Knex();

//   const status = await knex(Tables.Lookups).where({
//     group: 'STATUS',
//     label: 'Active',
//   });

//   const userType = await knex(Tables.Roles).select('roles.*').innerJoin('lookups', 'role_id', 'lookups.id').where({
//       group: 'USER_TYPE',
//       label: 'Administrator',
//   });

//   const auth = await knex(Tables.Auth).insert({
//     status_id: status[0].id,
//     role_id: userType[0].id,
//     account_name: `Administrator`,
//     username: 'administrator',
//     picture: null,
//     password: encrypt('123@123'),
//   });

//   const authB = await knex(Tables.Auth).where({username: 'administrator'});

//   const user = await knex(Tables.Users).insert({
//     status_id: status[0].id,
//     role_id: userType[0].id,
//     auth_0_user_id: auth[0].id,
//     first_name: 'Paygate',
//     last_name: 'Admin',
//     email: 'admin@paynamics.net',
//     picture: null,
//   });
//   await knex.destroy();
//   return user;

// };

export const getToken = async (auth0UserId?: string) => {
  const res = await api('POST', '/api/authenticate', {
    headers: {
      'Content-Type': 'application/json',
    },
    body: {
      username: 'administrator',
      password: '123@123',
    },
  });
  console.log(res.getData()['token']);
  return res.getData()['token'];
};
