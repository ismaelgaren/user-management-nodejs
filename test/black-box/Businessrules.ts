import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/businessrules', () => {

    const keys = [
        'tenantId', 'merchantId', 'policyId', 'status'
    ];

    const testData = {
        tenantId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        tenantId: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        policyId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /businessrules        Should create a new businessrules', async () => {
        const res = await api('POST', '/api/businessrules', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /businessrules        Should fail because we want to create a empty businessrules', async () => {
        const res = await api('POST', '/api/businessrules', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /businessrules        Should list of businessruless with our new create one', async () => {
        const res = await api('GET', '/api/businessrules', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /businessrules/:id    Should return one businessrules', async () => {
        const res = await api('GET', `/api/businessrules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.policyId).toBe(testData.policyId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /businessrules/:id    Should update the businessrules', async () => {
        const res = await api('PUT', `/api/businessrules/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.policyId).toBe(testDataUpdated.policyId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /businessrules/:id    Should fail because we want to update the businessrules with a invalid email', async () => {
        const res = await api('PUT', `/api/businessrules/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /businessrules/:id    Should delete the businessrules', async () => {
        const res = await api('DELETE', `/api/businessrules/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /businessrules/:id    Should return with a 404, because we just deleted the businessrules', async () => {
        const res = await api('GET', `/api/businessrules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /businessrules/:id    Should return with a 404, because we just deleted the businessrules', async () => {
        const res = await api('DELETE', `/api/businessrules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /businessrules/:id    Should return with a 404, because we just deleted the businessrules', async () => {
        const res = await api('PUT', `/api/businessrules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
