import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/pchannels', () => {

    const keys = [
        'pchannelId', 'status', 'name', 'pmethod'
    ];

    const testData = {
        pchannelId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        pmethod: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        pchannelId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        pmethod: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /pchannels        Should create a new pchannels', async () => {
        const res = await api('POST', '/api/pchannels', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /pchannels        Should fail because we want to create a empty pchannels', async () => {
        const res = await api('POST', '/api/pchannels', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /pchannels        Should list of pchannelss with our new create one', async () => {
        const res = await api('GET', '/api/pchannels', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.pchannelId).toBe(testData.pchannelId);
        expect(result.status).toBe(testData.status);
        expect(result.name).toBe(testData.name);
        expect(result.pmethod).toBe(testData.pmethod);
    });

    test('GET       /pchannels/:id    Should return one pchannels', async () => {
        const res = await api('GET', `/api/pchannels/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pchannelId).toBe(testData.pchannelId);
        expect(result.status).toBe(testData.status);
        expect(result.name).toBe(testData.name);
        expect(result.pmethod).toBe(testData.pmethod);
    });

    test('PUT       /pchannels/:id    Should update the pchannels', async () => {
        const res = await api('PUT', `/api/pchannels/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pchannelId).toBe(testDataUpdated.pchannelId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.pmethod).toBe(testDataUpdated.pmethod);
    });

    test('PUT       /pchannels/:id    Should fail because we want to update the pchannels with a invalid email', async () => {
        const res = await api('PUT', `/api/pchannels/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /pchannels/:id    Should delete the pchannels', async () => {
        const res = await api('DELETE', `/api/pchannels/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /pchannels/:id    Should return with a 404, because we just deleted the pchannels', async () => {
        const res = await api('GET', `/api/pchannels/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /pchannels/:id    Should return with a 404, because we just deleted the pchannels', async () => {
        const res = await api('DELETE', `/api/pchannels/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /pchannels/:id    Should return with a 404, because we just deleted the pchannels', async () => {
        const res = await api('PUT', `/api/pchannels/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
