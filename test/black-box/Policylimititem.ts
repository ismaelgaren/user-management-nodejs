import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policylimititems', () => {

    const keys = [
        'policyItemId', 'status', 'parameter', 'maximumValue', 'frequency'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        maximumValue: undefined, // TODO: Add test value
        frequency: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined, // TODO: Add test value
        maximumValue: undefined, // TODO: Add test value
        frequency: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policylimititems        Should create a new policylimititem', async () => {
        const res = await api('POST', '/api/policylimititems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policylimititems        Should fail because we want to create a empty policylimititem', async () => {
        const res = await api('POST', '/api/policylimititems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policylimititems        Should list of policylimititems with our new create one', async () => {
        const res = await api('GET', '/api/policylimititems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.maximumValue).toBe(testData.maximumValue);
        expect(result.frequency).toBe(testData.frequency);
    });

    test('GET       /policylimititems/:id    Should return one policylimititem', async () => {
        const res = await api('GET', `/api/policylimititems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
        expect(result.maximumValue).toBe(testData.maximumValue);
        expect(result.frequency).toBe(testData.frequency);
    });

    test('PUT       /policylimititems/:id    Should update the policylimititem', async () => {
        const res = await api('PUT', `/api/policylimititems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.parameter).toBe(testDataUpdated.parameter);
        expect(result.maximumValue).toBe(testDataUpdated.maximumValue);
        expect(result.frequency).toBe(testDataUpdated.frequency);
    });

    test('PUT       /policylimititems/:id    Should fail because we want to update the policylimititem with a invalid email', async () => {
        const res = await api('PUT', `/api/policylimititems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policylimititems/:id    Should delete the policylimititem', async () => {
        const res = await api('DELETE', `/api/policylimititems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policylimititems/:id    Should return with a 404, because we just deleted the policylimititem', async () => {
        const res = await api('GET', `/api/policylimititems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policylimititems/:id    Should return with a 404, because we just deleted the policylimititem', async () => {
        const res = await api('DELETE', `/api/policylimititems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policylimititems/:id    Should return with a 404, because we just deleted the policylimititem', async () => {
        const res = await api('PUT', `/api/policylimititems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
