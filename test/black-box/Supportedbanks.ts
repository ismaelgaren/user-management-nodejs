import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/supportedbanks', () => {

    const keys = [
        'disbursementMethod', 'name', 'code', 'format', 'savings', 'current', 'cashCard', 'checking', 'corporate', 'status'
    ];

    const testData = {
        disbursementMethod: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        format: undefined, // TODO: Add test value
        savings: undefined, // TODO: Add test value
        current: undefined, // TODO: Add test value
        cashCard: undefined, // TODO: Add test value
        checking: undefined, // TODO: Add test value
        corporate: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        disbursementMethod: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        format: undefined, // TODO: Add test value
        savings: undefined, // TODO: Add test value
        current: undefined, // TODO: Add test value
        cashCard: undefined, // TODO: Add test value
        checking: undefined, // TODO: Add test value
        corporate: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /supportedbanks        Should create a new supportedbanks', async () => {
        const res = await api('POST', '/api/supportedbanks', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /supportedbanks        Should fail because we want to create a empty supportedbanks', async () => {
        const res = await api('POST', '/api/supportedbanks', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /supportedbanks        Should list of supportedbankss with our new create one', async () => {
        const res = await api('GET', '/api/supportedbanks', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.disbursementMethod).toBe(testData.disbursementMethod);
        expect(result.name).toBe(testData.name);
        expect(result.code).toBe(testData.code);
        expect(result.format).toBe(testData.format);
        expect(result.savings).toBe(testData.savings);
        expect(result.current).toBe(testData.current);
        expect(result.cashCard).toBe(testData.cashCard);
        expect(result.checking).toBe(testData.checking);
        expect(result.corporate).toBe(testData.corporate);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /supportedbanks/:id    Should return one supportedbanks', async () => {
        const res = await api('GET', `/api/supportedbanks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.disbursementMethod).toBe(testData.disbursementMethod);
        expect(result.name).toBe(testData.name);
        expect(result.code).toBe(testData.code);
        expect(result.format).toBe(testData.format);
        expect(result.savings).toBe(testData.savings);
        expect(result.current).toBe(testData.current);
        expect(result.cashCard).toBe(testData.cashCard);
        expect(result.checking).toBe(testData.checking);
        expect(result.corporate).toBe(testData.corporate);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /supportedbanks/:id    Should update the supportedbanks', async () => {
        const res = await api('PUT', `/api/supportedbanks/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.disbursementMethod).toBe(testDataUpdated.disbursementMethod);
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.code).toBe(testDataUpdated.code);
        expect(result.format).toBe(testDataUpdated.format);
        expect(result.savings).toBe(testDataUpdated.savings);
        expect(result.current).toBe(testDataUpdated.current);
        expect(result.cashCard).toBe(testDataUpdated.cashCard);
        expect(result.checking).toBe(testDataUpdated.checking);
        expect(result.corporate).toBe(testDataUpdated.corporate);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /supportedbanks/:id    Should fail because we want to update the supportedbanks with a invalid email', async () => {
        const res = await api('PUT', `/api/supportedbanks/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /supportedbanks/:id    Should delete the supportedbanks', async () => {
        const res = await api('DELETE', `/api/supportedbanks/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /supportedbanks/:id    Should return with a 404, because we just deleted the supportedbanks', async () => {
        const res = await api('GET', `/api/supportedbanks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /supportedbanks/:id    Should return with a 404, because we just deleted the supportedbanks', async () => {
        const res = await api('DELETE', `/api/supportedbanks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /supportedbanks/:id    Should return with a 404, because we just deleted the supportedbanks', async () => {
        const res = await api('PUT', `/api/supportedbanks/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
