import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/transactiongroups', () => {

    const keys = [
        'name', 'tenantId'
    ];

    const testData = {
        name: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        name: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /transactiongroups        Should create a new transactiongroup', async () => {
        const res = await api('POST', '/api/transactiongroups', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /transactiongroups        Should fail because we want to create a empty transactiongroup', async () => {
        const res = await api('POST', '/api/transactiongroups', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /transactiongroups        Should list of transactiongroups with our new create one', async () => {
        const res = await api('GET', '/api/transactiongroups', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.name).toBe(testData.name);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('GET       /transactiongroups/:id    Should return one transactiongroup', async () => {
        const res = await api('GET', `/api/transactiongroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testData.name);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('PUT       /transactiongroups/:id    Should update the transactiongroup', async () => {
        const res = await api('PUT', `/api/transactiongroups/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
    });

    test('PUT       /transactiongroups/:id    Should fail because we want to update the transactiongroup with a invalid email', async () => {
        const res = await api('PUT', `/api/transactiongroups/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /transactiongroups/:id    Should delete the transactiongroup', async () => {
        const res = await api('DELETE', `/api/transactiongroups/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /transactiongroups/:id    Should return with a 404, because we just deleted the transactiongroup', async () => {
        const res = await api('GET', `/api/transactiongroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /transactiongroups/:id    Should return with a 404, because we just deleted the transactiongroup', async () => {
        const res = await api('DELETE', `/api/transactiongroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /transactiongroups/:id    Should return with a 404, because we just deleted the transactiongroup', async () => {
        const res = await api('PUT', `/api/transactiongroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
