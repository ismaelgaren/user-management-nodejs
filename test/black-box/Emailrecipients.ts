import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/emailrecipients', () => {

    const keys = [
        'moduleName', 'emailGroup', 'status'
    ];

    const testData = {
        moduleName: undefined, // TODO: Add test value
        emailGroup: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        moduleName: undefined, // TODO: Add test value
        emailGroup: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /emailrecipients        Should create a new emailrecipients', async () => {
        const res = await api('POST', '/api/emailrecipients', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /emailrecipients        Should fail because we want to create a empty emailrecipients', async () => {
        const res = await api('POST', '/api/emailrecipients', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /emailrecipients        Should list of emailrecipientss with our new create one', async () => {
        const res = await api('GET', '/api/emailrecipients', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.moduleName).toBe(testData.moduleName);
        expect(result.emailGroup).toBe(testData.emailGroup);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /emailrecipients/:id    Should return one emailrecipients', async () => {
        const res = await api('GET', `/api/emailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.moduleName).toBe(testData.moduleName);
        expect(result.emailGroup).toBe(testData.emailGroup);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /emailrecipients/:id    Should update the emailrecipients', async () => {
        const res = await api('PUT', `/api/emailrecipients/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.moduleName).toBe(testDataUpdated.moduleName);
        expect(result.emailGroup).toBe(testDataUpdated.emailGroup);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /emailrecipients/:id    Should fail because we want to update the emailrecipients with a invalid email', async () => {
        const res = await api('PUT', `/api/emailrecipients/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /emailrecipients/:id    Should delete the emailrecipients', async () => {
        const res = await api('DELETE', `/api/emailrecipients/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /emailrecipients/:id    Should return with a 404, because we just deleted the emailrecipients', async () => {
        const res = await api('GET', `/api/emailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /emailrecipients/:id    Should return with a 404, because we just deleted the emailrecipients', async () => {
        const res = await api('DELETE', `/api/emailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /emailrecipients/:id    Should return with a 404, because we just deleted the emailrecipients', async () => {
        const res = await api('PUT', `/api/emailrecipients/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
