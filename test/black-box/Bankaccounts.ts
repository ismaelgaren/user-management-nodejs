import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/bankaccounts', () => {

    const keys = [
        'name', 'bankId', 'bankAccountNo'
    ];

    const testData = {
        name: undefined, // TODO: Add test value
        bankId: undefined, // TODO: Add test value
        bankAccountNo: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        name: undefined, // TODO: Add test value
        bankId: undefined, // TODO: Add test value
        bankAccountNo: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /bankaccounts        Should create a new bankaccounts', async () => {
        const res = await api('POST', '/api/bankaccounts', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /bankaccounts        Should fail because we want to create a empty bankaccounts', async () => {
        const res = await api('POST', '/api/bankaccounts', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /bankaccounts        Should list of bankaccountss with our new create one', async () => {
        const res = await api('GET', '/api/bankaccounts', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.name).toBe(testData.name);
        expect(result.bankId).toBe(testData.bankId);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
    });

    test('GET       /bankaccounts/:id    Should return one bankaccounts', async () => {
        const res = await api('GET', `/api/bankaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testData.name);
        expect(result.bankId).toBe(testData.bankId);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
    });

    test('PUT       /bankaccounts/:id    Should update the bankaccounts', async () => {
        const res = await api('PUT', `/api/bankaccounts/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.bankId).toBe(testDataUpdated.bankId);
        expect(result.bankAccountNo).toBe(testDataUpdated.bankAccountNo);
    });

    test('PUT       /bankaccounts/:id    Should fail because we want to update the bankaccounts with a invalid email', async () => {
        const res = await api('PUT', `/api/bankaccounts/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /bankaccounts/:id    Should delete the bankaccounts', async () => {
        const res = await api('DELETE', `/api/bankaccounts/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /bankaccounts/:id    Should return with a 404, because we just deleted the bankaccounts', async () => {
        const res = await api('GET', `/api/bankaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /bankaccounts/:id    Should return with a 404, because we just deleted the bankaccounts', async () => {
        const res = await api('DELETE', `/api/bankaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /bankaccounts/:id    Should return with a 404, because we just deleted the bankaccounts', async () => {
        const res = await api('PUT', `/api/bankaccounts/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
