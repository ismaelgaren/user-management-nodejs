import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/systemsettings', () => {

    const keys = [
        'key', 'value'
    ];

    const testData = {
        key: undefined, // TODO: Add test value
        value: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        key: undefined, // TODO: Add test value
        value: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /systemsettings        Should create a new systemsetting', async () => {
        const res = await api('POST', '/api/systemsettings', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /systemsettings        Should fail because we want to create a empty systemsetting', async () => {
        const res = await api('POST', '/api/systemsettings', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /systemsettings        Should list of systemsettings with our new create one', async () => {
        const res = await api('GET', '/api/systemsettings', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.key).toBe(testData.key);
        expect(result.value).toBe(testData.value);
    });

    test('GET       /systemsettings/:id    Should return one systemsetting', async () => {
        const res = await api('GET', `/api/systemsettings/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.key).toBe(testData.key);
        expect(result.value).toBe(testData.value);
    });

    test('PUT       /systemsettings/:id    Should update the systemsetting', async () => {
        const res = await api('PUT', `/api/systemsettings/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.key).toBe(testDataUpdated.key);
        expect(result.value).toBe(testDataUpdated.value);
    });

    test('PUT       /systemsettings/:id    Should fail because we want to update the systemsetting with a invalid email', async () => {
        const res = await api('PUT', `/api/systemsettings/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /systemsettings/:id    Should delete the systemsetting', async () => {
        const res = await api('DELETE', `/api/systemsettings/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /systemsettings/:id    Should return with a 404, because we just deleted the systemsetting', async () => {
        const res = await api('GET', `/api/systemsettings/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /systemsettings/:id    Should return with a 404, because we just deleted the systemsetting', async () => {
        const res = await api('DELETE', `/api/systemsettings/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /systemsettings/:id    Should return with a 404, because we just deleted the systemsetting', async () => {
        const res = await api('PUT', `/api/systemsettings/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
