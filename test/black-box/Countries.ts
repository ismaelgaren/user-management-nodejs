import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/countries', () => {

    const keys = [
        'name', 'code'
    ];

    const testData = {
        name: undefined, // TODO: Add test value
        code: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        name: undefined, // TODO: Add test value
        code: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /countries        Should create a new countries', async () => {
        const res = await api('POST', '/api/countries', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /countries        Should fail because we want to create a empty countries', async () => {
        const res = await api('POST', '/api/countries', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /countries        Should list of countriess with our new create one', async () => {
        const res = await api('GET', '/api/countries', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.name).toBe(testData.name);
        expect(result.code).toBe(testData.code);
    });

    test('GET       /countries/:id    Should return one countries', async () => {
        const res = await api('GET', `/api/countries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testData.name);
        expect(result.code).toBe(testData.code);
    });

    test('PUT       /countries/:id    Should update the countries', async () => {
        const res = await api('PUT', `/api/countries/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.code).toBe(testDataUpdated.code);
    });

    test('PUT       /countries/:id    Should fail because we want to update the countries with a invalid email', async () => {
        const res = await api('PUT', `/api/countries/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /countries/:id    Should delete the countries', async () => {
        const res = await api('DELETE', `/api/countries/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /countries/:id    Should return with a 404, because we just deleted the countries', async () => {
        const res = await api('GET', `/api/countries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /countries/:id    Should return with a 404, because we just deleted the countries', async () => {
        const res = await api('DELETE', `/api/countries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /countries/:id    Should return with a 404, because we just deleted the countries', async () => {
        const res = await api('PUT', `/api/countries/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
