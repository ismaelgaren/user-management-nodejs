import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/casecomments', () => {

    const keys = [
        'userId', 'status', 'merchantId', 'caseId', 'message', 'type'
    ];

    const testData = {
        userId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        caseId: undefined, // TODO: Add test value
        message: undefined, // TODO: Add test value
        type: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        userId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        caseId: undefined, // TODO: Add test value
        message: undefined, // TODO: Add test value
        type: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /casecomments        Should create a new casecomments', async () => {
        const res = await api('POST', '/api/casecomments', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /casecomments        Should fail because we want to create a empty casecomments', async () => {
        const res = await api('POST', '/api/casecomments', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /casecomments        Should list of casecommentss with our new create one', async () => {
        const res = await api('GET', '/api/casecomments', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.userId).toBe(testData.userId);
        expect(result.status).toBe(testData.status);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.caseId).toBe(testData.caseId);
        expect(result.message).toBe(testData.message);
        expect(result.type).toBe(testData.type);
    });

    test('GET       /casecomments/:id    Should return one casecomments', async () => {
        const res = await api('GET', `/api/casecomments/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.userId).toBe(testData.userId);
        expect(result.status).toBe(testData.status);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.caseId).toBe(testData.caseId);
        expect(result.message).toBe(testData.message);
        expect(result.type).toBe(testData.type);
    });

    test('PUT       /casecomments/:id    Should update the casecomments', async () => {
        const res = await api('PUT', `/api/casecomments/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.userId).toBe(testDataUpdated.userId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.caseId).toBe(testDataUpdated.caseId);
        expect(result.message).toBe(testDataUpdated.message);
        expect(result.type).toBe(testDataUpdated.type);
    });

    test('PUT       /casecomments/:id    Should fail because we want to update the casecomments with a invalid email', async () => {
        const res = await api('PUT', `/api/casecomments/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /casecomments/:id    Should delete the casecomments', async () => {
        const res = await api('DELETE', `/api/casecomments/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /casecomments/:id    Should return with a 404, because we just deleted the casecomments', async () => {
        const res = await api('GET', `/api/casecomments/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /casecomments/:id    Should return with a 404, because we just deleted the casecomments', async () => {
        const res = await api('DELETE', `/api/casecomments/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /casecomments/:id    Should return with a 404, because we just deleted the casecomments', async () => {
        const res = await api('PUT', `/api/casecomments/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
