import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/docsmodules', () => {

    const keys = [
        'docId', 'moduleName'
    ];

    const testData = {
        docId: undefined, // TODO: Add test value
        moduleName: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        docId: undefined, // TODO: Add test value
        moduleName: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /docsmodules        Should create a new docsmodules', async () => {
        const res = await api('POST', '/api/docsmodules', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /docsmodules        Should fail because we want to create a empty docsmodules', async () => {
        const res = await api('POST', '/api/docsmodules', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /docsmodules        Should list of docsmoduless with our new create one', async () => {
        const res = await api('GET', '/api/docsmodules', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.docId).toBe(testData.docId);
        expect(result.moduleName).toBe(testData.moduleName);
    });

    test('GET       /docsmodules/:id    Should return one docsmodules', async () => {
        const res = await api('GET', `/api/docsmodules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.docId).toBe(testData.docId);
        expect(result.moduleName).toBe(testData.moduleName);
    });

    test('PUT       /docsmodules/:id    Should update the docsmodules', async () => {
        const res = await api('PUT', `/api/docsmodules/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.docId).toBe(testDataUpdated.docId);
        expect(result.moduleName).toBe(testDataUpdated.moduleName);
    });

    test('PUT       /docsmodules/:id    Should fail because we want to update the docsmodules with a invalid email', async () => {
        const res = await api('PUT', `/api/docsmodules/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /docsmodules/:id    Should delete the docsmodules', async () => {
        const res = await api('DELETE', `/api/docsmodules/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /docsmodules/:id    Should return with a 404, because we just deleted the docsmodules', async () => {
        const res = await api('GET', `/api/docsmodules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /docsmodules/:id    Should return with a 404, because we just deleted the docsmodules', async () => {
        const res = await api('DELETE', `/api/docsmodules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /docsmodules/:id    Should return with a 404, because we just deleted the docsmodules', async () => {
        const res = await api('PUT', `/api/docsmodules/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
