import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/integrationkeys', () => {

    const keys = [
        'userId', 'key', 'status'
    ];

    const testData = {
        userId: undefined, // TODO: Add test value
        key: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        userId: undefined, // TODO: Add test value
        key: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /integrationkeys        Should create a new integrationkeys', async () => {
        const res = await api('POST', '/api/integrationkeys', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /integrationkeys        Should fail because we want to create a empty integrationkeys', async () => {
        const res = await api('POST', '/api/integrationkeys', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /integrationkeys        Should list of integrationkeyss with our new create one', async () => {
        const res = await api('GET', '/api/integrationkeys', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.userId).toBe(testData.userId);
        expect(result.key).toBe(testData.key);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /integrationkeys/:id    Should return one integrationkeys', async () => {
        const res = await api('GET', `/api/integrationkeys/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.userId).toBe(testData.userId);
        expect(result.key).toBe(testData.key);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /integrationkeys/:id    Should update the integrationkeys', async () => {
        const res = await api('PUT', `/api/integrationkeys/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.userId).toBe(testDataUpdated.userId);
        expect(result.key).toBe(testDataUpdated.key);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /integrationkeys/:id    Should fail because we want to update the integrationkeys with a invalid email', async () => {
        const res = await api('PUT', `/api/integrationkeys/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /integrationkeys/:id    Should delete the integrationkeys', async () => {
        const res = await api('DELETE', `/api/integrationkeys/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /integrationkeys/:id    Should return with a 404, because we just deleted the integrationkeys', async () => {
        const res = await api('GET', `/api/integrationkeys/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /integrationkeys/:id    Should return with a 404, because we just deleted the integrationkeys', async () => {
        const res = await api('DELETE', `/api/integrationkeys/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /integrationkeys/:id    Should return with a 404, because we just deleted the integrationkeys', async () => {
        const res = await api('PUT', `/api/integrationkeys/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
