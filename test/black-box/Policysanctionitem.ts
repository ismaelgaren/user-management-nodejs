import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policysanctionitems', () => {

    const keys = [
        'policyItemId', 'status', 'parameter'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        parameter: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policysanctionitems        Should create a new policysanctionitem', async () => {
        const res = await api('POST', '/api/policysanctionitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policysanctionitems        Should fail because we want to create a empty policysanctionitem', async () => {
        const res = await api('POST', '/api/policysanctionitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policysanctionitems        Should list of policysanctionitems with our new create one', async () => {
        const res = await api('GET', '/api/policysanctionitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
    });

    test('GET       /policysanctionitems/:id    Should return one policysanctionitem', async () => {
        const res = await api('GET', `/api/policysanctionitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.status).toBe(testData.status);
        expect(result.parameter).toBe(testData.parameter);
    });

    test('PUT       /policysanctionitems/:id    Should update the policysanctionitem', async () => {
        const res = await api('PUT', `/api/policysanctionitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.parameter).toBe(testDataUpdated.parameter);
    });

    test('PUT       /policysanctionitems/:id    Should fail because we want to update the policysanctionitem with a invalid email', async () => {
        const res = await api('PUT', `/api/policysanctionitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policysanctionitems/:id    Should delete the policysanctionitem', async () => {
        const res = await api('DELETE', `/api/policysanctionitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policysanctionitems/:id    Should return with a 404, because we just deleted the policysanctionitem', async () => {
        const res = await api('GET', `/api/policysanctionitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policysanctionitems/:id    Should return with a 404, because we just deleted the policysanctionitem', async () => {
        const res = await api('DELETE', `/api/policysanctionitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policysanctionitems/:id    Should return with a 404, because we just deleted the policysanctionitem', async () => {
        const res = await api('PUT', `/api/policysanctionitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
