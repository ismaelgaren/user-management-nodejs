import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policygeoipitems', () => {

    const keys = [
        'policyItemId', 'inboundParamId', 'condition', 'countryId', 'violationRespCode', 'status'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        condition: undefined, // TODO: Add test value
        countryId: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        condition: undefined, // TODO: Add test value
        countryId: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policygeoipitems        Should create a new policygeoipitems', async () => {
        const res = await api('POST', '/api/policygeoipitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policygeoipitems        Should fail because we want to create a empty policygeoipitems', async () => {
        const res = await api('POST', '/api/policygeoipitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policygeoipitems        Should list of policygeoipitemss with our new create one', async () => {
        const res = await api('GET', '/api/policygeoipitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.condition).toBe(testData.condition);
        expect(result.countryId).toBe(testData.countryId);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policygeoipitems/:id    Should return one policygeoipitems', async () => {
        const res = await api('GET', `/api/policygeoipitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.condition).toBe(testData.condition);
        expect(result.countryId).toBe(testData.countryId);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policygeoipitems/:id    Should update the policygeoipitems', async () => {
        const res = await api('PUT', `/api/policygeoipitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.inboundParamId).toBe(testDataUpdated.inboundParamId);
        expect(result.condition).toBe(testDataUpdated.condition);
        expect(result.countryId).toBe(testDataUpdated.countryId);
        expect(result.violationRespCode).toBe(testDataUpdated.violationRespCode);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policygeoipitems/:id    Should fail because we want to update the policygeoipitems with a invalid email', async () => {
        const res = await api('PUT', `/api/policygeoipitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policygeoipitems/:id    Should delete the policygeoipitems', async () => {
        const res = await api('DELETE', `/api/policygeoipitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policygeoipitems/:id    Should return with a 404, because we just deleted the policygeoipitems', async () => {
        const res = await api('GET', `/api/policygeoipitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policygeoipitems/:id    Should return with a 404, because we just deleted the policygeoipitems', async () => {
        const res = await api('DELETE', `/api/policygeoipitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policygeoipitems/:id    Should return with a 404, because we just deleted the policygeoipitems', async () => {
        const res = await api('PUT', `/api/policygeoipitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
