import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policytypes', () => {

    const keys = [
        'label', 'description', 'status'
    ];

    const testData = {
        label: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        label: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policytypes        Should create a new policytype', async () => {
        const res = await api('POST', '/api/policytypes', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policytypes        Should fail because we want to create a empty policytype', async () => {
        const res = await api('POST', '/api/policytypes', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policytypes        Should list of policytypes with our new create one', async () => {
        const res = await api('GET', '/api/policytypes', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.label).toBe(testData.label);
        expect(result.description).toBe(testData.description);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policytypes/:id    Should return one policytype', async () => {
        const res = await api('GET', `/api/policytypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testData.label);
        expect(result.description).toBe(testData.description);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policytypes/:id    Should update the policytype', async () => {
        const res = await api('PUT', `/api/policytypes/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testDataUpdated.label);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policytypes/:id    Should fail because we want to update the policytype with a invalid email', async () => {
        const res = await api('PUT', `/api/policytypes/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policytypes/:id    Should delete the policytype', async () => {
        const res = await api('DELETE', `/api/policytypes/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policytypes/:id    Should return with a 404, because we just deleted the policytype', async () => {
        const res = await api('GET', `/api/policytypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policytypes/:id    Should return with a 404, because we just deleted the policytype', async () => {
        const res = await api('DELETE', `/api/policytypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policytypes/:id    Should return with a 404, because we just deleted the policytype', async () => {
        const res = await api('PUT', `/api/policytypes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
