import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/downloadrequests', () => {

    const keys = [
        'requestToken', 'status'
    ];

    const testData = {
        requestToken: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        requestToken: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /downloadrequests        Should create a new downloadrequest', async () => {
        const res = await api('POST', '/api/downloadrequests', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /downloadrequests        Should fail because we want to create a empty downloadrequest', async () => {
        const res = await api('POST', '/api/downloadrequests', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /downloadrequests        Should list of downloadrequests with our new create one', async () => {
        const res = await api('GET', '/api/downloadrequests', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.requestToken).toBe(testData.requestToken);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /downloadrequests/:id    Should return one downloadrequest', async () => {
        const res = await api('GET', `/api/downloadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestToken).toBe(testData.requestToken);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /downloadrequests/:id    Should update the downloadrequest', async () => {
        const res = await api('PUT', `/api/downloadrequests/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestToken).toBe(testDataUpdated.requestToken);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /downloadrequests/:id    Should fail because we want to update the downloadrequest with a invalid email', async () => {
        const res = await api('PUT', `/api/downloadrequests/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /downloadrequests/:id    Should delete the downloadrequest', async () => {
        const res = await api('DELETE', `/api/downloadrequests/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /downloadrequests/:id    Should return with a 404, because we just deleted the downloadrequest', async () => {
        const res = await api('GET', `/api/downloadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /downloadrequests/:id    Should return with a 404, because we just deleted the downloadrequest', async () => {
        const res = await api('DELETE', `/api/downloadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /downloadrequests/:id    Should return with a 404, because we just deleted the downloadrequest', async () => {
        const res = await api('PUT', `/api/downloadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
