import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/sanctionlists', () => {

    const keys = [
        'dataId', 'firstName', 'secondName', 'thirdName', 'fourthName', 'listType', 'nameOriginalScript', 'country'
    ];

    const testData = {
        dataId: undefined, // TODO: Add test value
        firstName: undefined, // TODO: Add test value
        secondName: undefined, // TODO: Add test value
        thirdName: undefined, // TODO: Add test value
        fourthName: undefined, // TODO: Add test value
        listType: undefined, // TODO: Add test value
        nameOriginalScript: undefined, // TODO: Add test value
        country: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        dataId: undefined, // TODO: Add test value
        firstName: undefined, // TODO: Add test value
        secondName: undefined, // TODO: Add test value
        thirdName: undefined, // TODO: Add test value
        fourthName: undefined, // TODO: Add test value
        listType: undefined, // TODO: Add test value
        nameOriginalScript: undefined, // TODO: Add test value
        country: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /sanctionlists        Should create a new sanctionlist', async () => {
        const res = await api('POST', '/api/sanctionlists', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /sanctionlists        Should fail because we want to create a empty sanctionlist', async () => {
        const res = await api('POST', '/api/sanctionlists', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /sanctionlists        Should list of sanctionlists with our new create one', async () => {
        const res = await api('GET', '/api/sanctionlists', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.dataId).toBe(testData.dataId);
        expect(result.firstName).toBe(testData.firstName);
        expect(result.secondName).toBe(testData.secondName);
        expect(result.thirdName).toBe(testData.thirdName);
        expect(result.fourthName).toBe(testData.fourthName);
        expect(result.listType).toBe(testData.listType);
        expect(result.nameOriginalScript).toBe(testData.nameOriginalScript);
        expect(result.country).toBe(testData.country);
    });

    test('GET       /sanctionlists/:id    Should return one sanctionlist', async () => {
        const res = await api('GET', `/api/sanctionlists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.dataId).toBe(testData.dataId);
        expect(result.firstName).toBe(testData.firstName);
        expect(result.secondName).toBe(testData.secondName);
        expect(result.thirdName).toBe(testData.thirdName);
        expect(result.fourthName).toBe(testData.fourthName);
        expect(result.listType).toBe(testData.listType);
        expect(result.nameOriginalScript).toBe(testData.nameOriginalScript);
        expect(result.country).toBe(testData.country);
    });

    test('PUT       /sanctionlists/:id    Should update the sanctionlist', async () => {
        const res = await api('PUT', `/api/sanctionlists/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.dataId).toBe(testDataUpdated.dataId);
        expect(result.firstName).toBe(testDataUpdated.firstName);
        expect(result.secondName).toBe(testDataUpdated.secondName);
        expect(result.thirdName).toBe(testDataUpdated.thirdName);
        expect(result.fourthName).toBe(testDataUpdated.fourthName);
        expect(result.listType).toBe(testDataUpdated.listType);
        expect(result.nameOriginalScript).toBe(testDataUpdated.nameOriginalScript);
        expect(result.country).toBe(testDataUpdated.country);
    });

    test('PUT       /sanctionlists/:id    Should fail because we want to update the sanctionlist with a invalid email', async () => {
        const res = await api('PUT', `/api/sanctionlists/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /sanctionlists/:id    Should delete the sanctionlist', async () => {
        const res = await api('DELETE', `/api/sanctionlists/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /sanctionlists/:id    Should return with a 404, because we just deleted the sanctionlist', async () => {
        const res = await api('GET', `/api/sanctionlists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /sanctionlists/:id    Should return with a 404, because we just deleted the sanctionlist', async () => {
        const res = await api('DELETE', `/api/sanctionlists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /sanctionlists/:id    Should return with a 404, because we just deleted the sanctionlist', async () => {
        const res = await api('PUT', `/api/sanctionlists/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
