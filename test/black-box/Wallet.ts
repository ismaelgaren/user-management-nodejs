import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/wallets', () => {

    const keys = [
        'customerId', 'balance'
    ];

    const testData = {
        customerId: undefined, // TODO: Add test value
        balance: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        customerId: undefined, // TODO: Add test value
        balance: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /wallets        Should create a new wallet', async () => {
        const res = await api('POST', '/api/wallets', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /wallets        Should fail because we want to create a empty wallet', async () => {
        const res = await api('POST', '/api/wallets', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /wallets        Should list of wallets with our new create one', async () => {
        const res = await api('GET', '/api/wallets', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.customerId).toBe(testData.customerId);
        expect(result.balance).toBe(testData.balance);
    });

    test('GET       /wallets/:id    Should return one wallet', async () => {
        const res = await api('GET', `/api/wallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.customerId).toBe(testData.customerId);
        expect(result.balance).toBe(testData.balance);
    });

    test('PUT       /wallets/:id    Should update the wallet', async () => {
        const res = await api('PUT', `/api/wallets/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.customerId).toBe(testDataUpdated.customerId);
        expect(result.balance).toBe(testDataUpdated.balance);
    });

    test('PUT       /wallets/:id    Should fail because we want to update the wallet with a invalid email', async () => {
        const res = await api('PUT', `/api/wallets/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /wallets/:id    Should delete the wallet', async () => {
        const res = await api('DELETE', `/api/wallets/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /wallets/:id    Should return with a 404, because we just deleted the wallet', async () => {
        const res = await api('GET', `/api/wallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /wallets/:id    Should return with a 404, because we just deleted the wallet', async () => {
        const res = await api('DELETE', `/api/wallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /wallets/:id    Should return with a 404, because we just deleted the wallet', async () => {
        const res = await api('PUT', `/api/wallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
