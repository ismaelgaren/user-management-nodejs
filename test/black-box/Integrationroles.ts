import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/integrationroles', () => {

    const keys = [
        'integrationId', 'canDebit', 'canCredit', 'canViewWallets', 'canEditWallets', 'canViewTrx', 'canViewWithheld', 'walletId', 'integrationRoles'
    ];

    const testData = {
        integrationId: undefined, // TODO: Add test value
        canDebit: undefined, // TODO: Add test value
        canCredit: undefined, // TODO: Add test value
        canViewWallets: undefined, // TODO: Add test value
        canEditWallets: undefined, // TODO: Add test value
        canViewTrx: undefined, // TODO: Add test value
        canViewWithheld: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        integrationRoles: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        integrationId: undefined, // TODO: Add test value
        canDebit: undefined, // TODO: Add test value
        canCredit: undefined, // TODO: Add test value
        canViewWallets: undefined, // TODO: Add test value
        canEditWallets: undefined, // TODO: Add test value
        canViewTrx: undefined, // TODO: Add test value
        canViewWithheld: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        integrationRoles: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /integrationroles        Should create a new integrationroles', async () => {
        const res = await api('POST', '/api/integrationroles', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /integrationroles        Should fail because we want to create a empty integrationroles', async () => {
        const res = await api('POST', '/api/integrationroles', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /integrationroles        Should list of integrationroless with our new create one', async () => {
        const res = await api('GET', '/api/integrationroles', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.integrationId).toBe(testData.integrationId);
        expect(result.canDebit).toBe(testData.canDebit);
        expect(result.canCredit).toBe(testData.canCredit);
        expect(result.canViewWallets).toBe(testData.canViewWallets);
        expect(result.canEditWallets).toBe(testData.canEditWallets);
        expect(result.canViewTrx).toBe(testData.canViewTrx);
        expect(result.canViewWithheld).toBe(testData.canViewWithheld);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.integrationRoles).toBe(testData.integrationRoles);
    });

    test('GET       /integrationroles/:id    Should return one integrationroles', async () => {
        const res = await api('GET', `/api/integrationroles/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.integrationId).toBe(testData.integrationId);
        expect(result.canDebit).toBe(testData.canDebit);
        expect(result.canCredit).toBe(testData.canCredit);
        expect(result.canViewWallets).toBe(testData.canViewWallets);
        expect(result.canEditWallets).toBe(testData.canEditWallets);
        expect(result.canViewTrx).toBe(testData.canViewTrx);
        expect(result.canViewWithheld).toBe(testData.canViewWithheld);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.integrationRoles).toBe(testData.integrationRoles);
    });

    test('PUT       /integrationroles/:id    Should update the integrationroles', async () => {
        const res = await api('PUT', `/api/integrationroles/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.integrationId).toBe(testDataUpdated.integrationId);
        expect(result.canDebit).toBe(testDataUpdated.canDebit);
        expect(result.canCredit).toBe(testDataUpdated.canCredit);
        expect(result.canViewWallets).toBe(testDataUpdated.canViewWallets);
        expect(result.canEditWallets).toBe(testDataUpdated.canEditWallets);
        expect(result.canViewTrx).toBe(testDataUpdated.canViewTrx);
        expect(result.canViewWithheld).toBe(testDataUpdated.canViewWithheld);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.integrationRoles).toBe(testDataUpdated.integrationRoles);
    });

    test('PUT       /integrationroles/:id    Should fail because we want to update the integrationroles with a invalid email', async () => {
        const res = await api('PUT', `/api/integrationroles/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /integrationroles/:id    Should delete the integrationroles', async () => {
        const res = await api('DELETE', `/api/integrationroles/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /integrationroles/:id    Should return with a 404, because we just deleted the integrationroles', async () => {
        const res = await api('GET', `/api/integrationroles/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /integrationroles/:id    Should return with a 404, because we just deleted the integrationroles', async () => {
        const res = await api('DELETE', `/api/integrationroles/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /integrationroles/:id    Should return with a 404, because we just deleted the integrationroles', async () => {
        const res = await api('PUT', `/api/integrationroles/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
