import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/inboundparameters', () => {

    const keys = [
        'transactionType', 'fieldName', 'dataType', 'defaultFormat', 'description', 'status'
    ];

    const testData = {
        transactionType: undefined, // TODO: Add test value
        fieldName: undefined, // TODO: Add test value
        dataType: undefined, // TODO: Add test value
        defaultFormat: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        transactionType: undefined, // TODO: Add test value
        fieldName: undefined, // TODO: Add test value
        dataType: undefined, // TODO: Add test value
        defaultFormat: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /inboundparameters        Should create a new inboundparameters', async () => {
        const res = await api('POST', '/api/inboundparameters', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /inboundparameters        Should fail because we want to create a empty inboundparameters', async () => {
        const res = await api('POST', '/api/inboundparameters', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /inboundparameters        Should list of inboundparameterss with our new create one', async () => {
        const res = await api('GET', '/api/inboundparameters', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.fieldName).toBe(testData.fieldName);
        expect(result.dataType).toBe(testData.dataType);
        expect(result.defaultFormat).toBe(testData.defaultFormat);
        expect(result.description).toBe(testData.description);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /inboundparameters/:id    Should return one inboundparameters', async () => {
        const res = await api('GET', `/api/inboundparameters/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.transactionType).toBe(testData.transactionType);
        expect(result.fieldName).toBe(testData.fieldName);
        expect(result.dataType).toBe(testData.dataType);
        expect(result.defaultFormat).toBe(testData.defaultFormat);
        expect(result.description).toBe(testData.description);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /inboundparameters/:id    Should update the inboundparameters', async () => {
        const res = await api('PUT', `/api/inboundparameters/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.transactionType).toBe(testDataUpdated.transactionType);
        expect(result.fieldName).toBe(testDataUpdated.fieldName);
        expect(result.dataType).toBe(testDataUpdated.dataType);
        expect(result.defaultFormat).toBe(testDataUpdated.defaultFormat);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /inboundparameters/:id    Should fail because we want to update the inboundparameters with a invalid email', async () => {
        const res = await api('PUT', `/api/inboundparameters/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /inboundparameters/:id    Should delete the inboundparameters', async () => {
        const res = await api('DELETE', `/api/inboundparameters/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /inboundparameters/:id    Should return with a 404, because we just deleted the inboundparameters', async () => {
        const res = await api('GET', `/api/inboundparameters/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /inboundparameters/:id    Should return with a 404, because we just deleted the inboundparameters', async () => {
        const res = await api('DELETE', `/api/inboundparameters/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /inboundparameters/:id    Should return with a 404, because we just deleted the inboundparameters', async () => {
        const res = await api('PUT', `/api/inboundparameters/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
