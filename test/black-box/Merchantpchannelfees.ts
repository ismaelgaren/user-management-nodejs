import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/merchantpchannelfees', () => {

    const keys = [
        'pchannel', 'pmethod', 'fee', 'whicheverIsHigherFee', 'whicheverIsHigher', 'merchantId', 'tenantId'
    ];

    const testData = {
        pchannel: undefined, // TODO: Add test value
        pmethod: undefined, // TODO: Add test value
        fee: undefined, // TODO: Add test value
        whicheverIsHigherFee: undefined, // TODO: Add test value
        whicheverIsHigher: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        pchannel: undefined, // TODO: Add test value
        pmethod: undefined, // TODO: Add test value
        fee: undefined, // TODO: Add test value
        whicheverIsHigherFee: undefined, // TODO: Add test value
        whicheverIsHigher: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /merchantpchannelfees        Should create a new merchantpchannelfees', async () => {
        const res = await api('POST', '/api/merchantpchannelfees', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /merchantpchannelfees        Should fail because we want to create a empty merchantpchannelfees', async () => {
        const res = await api('POST', '/api/merchantpchannelfees', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /merchantpchannelfees        Should list of merchantpchannelfeess with our new create one', async () => {
        const res = await api('GET', '/api/merchantpchannelfees', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.pchannel).toBe(testData.pchannel);
        expect(result.pmethod).toBe(testData.pmethod);
        expect(result.fee).toBe(testData.fee);
        expect(result.whicheverIsHigherFee).toBe(testData.whicheverIsHigherFee);
        expect(result.whicheverIsHigher).toBe(testData.whicheverIsHigher);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('GET       /merchantpchannelfees/:id    Should return one merchantpchannelfees', async () => {
        const res = await api('GET', `/api/merchantpchannelfees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pchannel).toBe(testData.pchannel);
        expect(result.pmethod).toBe(testData.pmethod);
        expect(result.fee).toBe(testData.fee);
        expect(result.whicheverIsHigherFee).toBe(testData.whicheverIsHigherFee);
        expect(result.whicheverIsHigher).toBe(testData.whicheverIsHigher);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('PUT       /merchantpchannelfees/:id    Should update the merchantpchannelfees', async () => {
        const res = await api('PUT', `/api/merchantpchannelfees/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pchannel).toBe(testDataUpdated.pchannel);
        expect(result.pmethod).toBe(testDataUpdated.pmethod);
        expect(result.fee).toBe(testDataUpdated.fee);
        expect(result.whicheverIsHigherFee).toBe(testDataUpdated.whicheverIsHigherFee);
        expect(result.whicheverIsHigher).toBe(testDataUpdated.whicheverIsHigher);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
    });

    test('PUT       /merchantpchannelfees/:id    Should fail because we want to update the merchantpchannelfees with a invalid email', async () => {
        const res = await api('PUT', `/api/merchantpchannelfees/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /merchantpchannelfees/:id    Should delete the merchantpchannelfees', async () => {
        const res = await api('DELETE', `/api/merchantpchannelfees/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /merchantpchannelfees/:id    Should return with a 404, because we just deleted the merchantpchannelfees', async () => {
        const res = await api('GET', `/api/merchantpchannelfees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /merchantpchannelfees/:id    Should return with a 404, because we just deleted the merchantpchannelfees', async () => {
        const res = await api('DELETE', `/api/merchantpchannelfees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /merchantpchannelfees/:id    Should return with a 404, because we just deleted the merchantpchannelfees', async () => {
        const res = await api('PUT', `/api/merchantpchannelfees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
