import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/flaggroups', () => {

    const keys = [
        'flagId', 'description', 'groupLabel', 'trxType', 'status'
    ];

    const testData = {
        flagId: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        groupLabel: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        flagId: undefined, // TODO: Add test value
        description: undefined, // TODO: Add test value
        groupLabel: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /flaggroups        Should create a new flaggroup', async () => {
        const res = await api('POST', '/api/flaggroups', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /flaggroups        Should fail because we want to create a empty flaggroup', async () => {
        const res = await api('POST', '/api/flaggroups', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /flaggroups        Should list of flaggroups with our new create one', async () => {
        const res = await api('GET', '/api/flaggroups', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.flagId).toBe(testData.flagId);
        expect(result.description).toBe(testData.description);
        expect(result.groupLabel).toBe(testData.groupLabel);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /flaggroups/:id    Should return one flaggroup', async () => {
        const res = await api('GET', `/api/flaggroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.flagId).toBe(testData.flagId);
        expect(result.description).toBe(testData.description);
        expect(result.groupLabel).toBe(testData.groupLabel);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /flaggroups/:id    Should update the flaggroup', async () => {
        const res = await api('PUT', `/api/flaggroups/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.flagId).toBe(testDataUpdated.flagId);
        expect(result.description).toBe(testDataUpdated.description);
        expect(result.groupLabel).toBe(testDataUpdated.groupLabel);
        expect(result.trxType).toBe(testDataUpdated.trxType);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /flaggroups/:id    Should fail because we want to update the flaggroup with a invalid email', async () => {
        const res = await api('PUT', `/api/flaggroups/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /flaggroups/:id    Should delete the flaggroup', async () => {
        const res = await api('DELETE', `/api/flaggroups/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /flaggroups/:id    Should return with a 404, because we just deleted the flaggroup', async () => {
        const res = await api('GET', `/api/flaggroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /flaggroups/:id    Should return with a 404, because we just deleted the flaggroup', async () => {
        const res = await api('DELETE', `/api/flaggroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /flaggroups/:id    Should return with a 404, because we just deleted the flaggroup', async () => {
        const res = await api('PUT', `/api/flaggroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
