import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/usermanagements', () => {

    const keys = [
        'firstName', 'lastName', 'address', 'postCode', 'phone', 'email', 'username', 'password'
    ];

    const testData = {
        firstName: undefined, // TODO: Add test value
        lastName: undefined, // TODO: Add test value
        address: undefined, // TODO: Add test value
        postCode: undefined, // TODO: Add test value
        phone: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        username: undefined, // TODO: Add test value
        password: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        firstName: undefined, // TODO: Add test value
        lastName: undefined, // TODO: Add test value
        address: undefined, // TODO: Add test value
        postCode: undefined, // TODO: Add test value
        phone: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        username: undefined, // TODO: Add test value
        password: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /usermanagements        Should create a new usermanagement', async () => {
        const res = await api('POST', '/api/usermanagements', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /usermanagements        Should fail because we want to create a empty usermanagement', async () => {
        const res = await api('POST', '/api/usermanagements', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /usermanagements        Should list of usermanagements with our new create one', async () => {
        const res = await api('GET', '/api/usermanagements', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.firstName).toBe(testData.firstName);
        expect(result.lastName).toBe(testData.lastName);
        expect(result.address).toBe(testData.address);
        expect(result.postCode).toBe(testData.postCode);
        expect(result.phone).toBe(testData.phone);
        expect(result.email).toBe(testData.email);
        expect(result.username).toBe(testData.username);
        expect(result.password).toBe(testData.password);
    });

    test('GET       /usermanagements/:id    Should return one usermanagement', async () => {
        const res = await api('GET', `/api/usermanagements/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.firstName).toBe(testData.firstName);
        expect(result.lastName).toBe(testData.lastName);
        expect(result.address).toBe(testData.address);
        expect(result.postCode).toBe(testData.postCode);
        expect(result.phone).toBe(testData.phone);
        expect(result.email).toBe(testData.email);
        expect(result.username).toBe(testData.username);
        expect(result.password).toBe(testData.password);
    });

    test('PUT       /usermanagements/:id    Should update the usermanagement', async () => {
        const res = await api('PUT', `/api/usermanagements/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.firstName).toBe(testDataUpdated.firstName);
        expect(result.lastName).toBe(testDataUpdated.lastName);
        expect(result.address).toBe(testDataUpdated.address);
        expect(result.postCode).toBe(testDataUpdated.postCode);
        expect(result.phone).toBe(testDataUpdated.phone);
        expect(result.email).toBe(testDataUpdated.email);
        expect(result.username).toBe(testDataUpdated.username);
        expect(result.password).toBe(testDataUpdated.password);
    });

    test('PUT       /usermanagements/:id    Should fail because we want to update the usermanagement with a invalid email', async () => {
        const res = await api('PUT', `/api/usermanagements/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /usermanagements/:id    Should delete the usermanagement', async () => {
        const res = await api('DELETE', `/api/usermanagements/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /usermanagements/:id    Should return with a 404, because we just deleted the usermanagement', async () => {
        const res = await api('GET', `/api/usermanagements/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /usermanagements/:id    Should return with a 404, because we just deleted the usermanagement', async () => {
        const res = await api('DELETE', `/api/usermanagements/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /usermanagements/:id    Should return with a 404, because we just deleted the usermanagement', async () => {
        const res = await api('PUT', `/api/usermanagements/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
