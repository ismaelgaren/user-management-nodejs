import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/casecommitees', () => {

    const keys = [
        'merchantId', 'caseId', 'userId', 'status'
    ];

    const testData = {
        merchantId: undefined, // TODO: Add test value
        caseId: undefined, // TODO: Add test value
        userId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        merchantId: undefined, // TODO: Add test value
        caseId: undefined, // TODO: Add test value
        userId: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /casecommitees        Should create a new casecommitee', async () => {
        const res = await api('POST', '/api/casecommitees', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /casecommitees        Should fail because we want to create a empty casecommitee', async () => {
        const res = await api('POST', '/api/casecommitees', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /casecommitees        Should list of casecommitees with our new create one', async () => {
        const res = await api('GET', '/api/casecommitees', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.caseId).toBe(testData.caseId);
        expect(result.userId).toBe(testData.userId);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /casecommitees/:id    Should return one casecommitee', async () => {
        const res = await api('GET', `/api/casecommitees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.caseId).toBe(testData.caseId);
        expect(result.userId).toBe(testData.userId);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /casecommitees/:id    Should update the casecommitee', async () => {
        const res = await api('PUT', `/api/casecommitees/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.caseId).toBe(testDataUpdated.caseId);
        expect(result.userId).toBe(testDataUpdated.userId);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /casecommitees/:id    Should fail because we want to update the casecommitee with a invalid email', async () => {
        const res = await api('PUT', `/api/casecommitees/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /casecommitees/:id    Should delete the casecommitee', async () => {
        const res = await api('DELETE', `/api/casecommitees/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /casecommitees/:id    Should return with a 404, because we just deleted the casecommitee', async () => {
        const res = await api('GET', `/api/casecommitees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /casecommitees/:id    Should return with a 404, because we just deleted the casecommitee', async () => {
        const res = await api('DELETE', `/api/casecommitees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /casecommitees/:id    Should return with a 404, because we just deleted the casecommitee', async () => {
        const res = await api('PUT', `/api/casecommitees/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
