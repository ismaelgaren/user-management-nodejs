import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/disbursementreqs', () => {

    const keys = [
        'mid', 'requestId', 'responseId', 'merchantIp', 'totalAmount', 'notificationUrl', 'responseUrl', 'benFname', 'benMname', 'benLname', 'benAddress1', 'benAddress2', 'benCity', 'benState', 'benCountry', 'benZip', 'benEmail', 'benPhone', 'disbursementAmount', 'currency', 'disbursementMethod', 'bankAccountNo', 'bnBankCode', 'gcashAccountNo', 'walletId', 'walletAccountNo', 'assignedRef', 'expiryDate', 'pickupCenter', 'status', 'statusInfo', 'statusText', 'disbursementInfo', 'fees', 'reason', 'isCorporate', 'mode', 'tenantId'
    ];

    const testData = {
        mid: undefined, // TODO: Add test value
        requestId: undefined, // TODO: Add test value
        responseId: undefined, // TODO: Add test value
        merchantIp: undefined, // TODO: Add test value
        totalAmount: undefined, // TODO: Add test value
        notificationUrl: undefined, // TODO: Add test value
        responseUrl: undefined, // TODO: Add test value
        benFname: undefined, // TODO: Add test value
        benMname: undefined, // TODO: Add test value
        benLname: undefined, // TODO: Add test value
        benAddress1: undefined, // TODO: Add test value
        benAddress2: undefined, // TODO: Add test value
        benCity: undefined, // TODO: Add test value
        benState: undefined, // TODO: Add test value
        benCountry: undefined, // TODO: Add test value
        benZip: undefined, // TODO: Add test value
        benEmail: undefined, // TODO: Add test value
        benPhone: undefined, // TODO: Add test value
        disbursementAmount: undefined, // TODO: Add test value
        currency: undefined, // TODO: Add test value
        disbursementMethod: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        bnBankCode: undefined, // TODO: Add test value
        gcashAccountNo: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        walletAccountNo: undefined, // TODO: Add test value
        assignedRef: undefined, // TODO: Add test value
        expiryDate: undefined, // TODO: Add test value
        pickupCenter: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        statusInfo: undefined, // TODO: Add test value
        statusText: undefined, // TODO: Add test value
        disbursementInfo: undefined, // TODO: Add test value
        fees: undefined, // TODO: Add test value
        reason: undefined, // TODO: Add test value
        isCorporate: undefined, // TODO: Add test value
        mode: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        mid: undefined, // TODO: Add test value
        requestId: undefined, // TODO: Add test value
        responseId: undefined, // TODO: Add test value
        merchantIp: undefined, // TODO: Add test value
        totalAmount: undefined, // TODO: Add test value
        notificationUrl: undefined, // TODO: Add test value
        responseUrl: undefined, // TODO: Add test value
        benFname: undefined, // TODO: Add test value
        benMname: undefined, // TODO: Add test value
        benLname: undefined, // TODO: Add test value
        benAddress1: undefined, // TODO: Add test value
        benAddress2: undefined, // TODO: Add test value
        benCity: undefined, // TODO: Add test value
        benState: undefined, // TODO: Add test value
        benCountry: undefined, // TODO: Add test value
        benZip: undefined, // TODO: Add test value
        benEmail: undefined, // TODO: Add test value
        benPhone: undefined, // TODO: Add test value
        disbursementAmount: undefined, // TODO: Add test value
        currency: undefined, // TODO: Add test value
        disbursementMethod: undefined, // TODO: Add test value
        bankAccountNo: undefined, // TODO: Add test value
        bnBankCode: undefined, // TODO: Add test value
        gcashAccountNo: undefined, // TODO: Add test value
        walletId: undefined, // TODO: Add test value
        walletAccountNo: undefined, // TODO: Add test value
        assignedRef: undefined, // TODO: Add test value
        expiryDate: undefined, // TODO: Add test value
        pickupCenter: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        statusInfo: undefined, // TODO: Add test value
        statusText: undefined, // TODO: Add test value
        disbursementInfo: undefined, // TODO: Add test value
        fees: undefined, // TODO: Add test value
        reason: undefined, // TODO: Add test value
        isCorporate: undefined, // TODO: Add test value
        mode: undefined, // TODO: Add test value
        tenantId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /disbursementreqs        Should create a new disbursementreq', async () => {
        const res = await api('POST', '/api/disbursementreqs', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /disbursementreqs        Should fail because we want to create a empty disbursementreq', async () => {
        const res = await api('POST', '/api/disbursementreqs', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /disbursementreqs        Should list of disbursementreqs with our new create one', async () => {
        const res = await api('GET', '/api/disbursementreqs', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.mid).toBe(testData.mid);
        expect(result.requestId).toBe(testData.requestId);
        expect(result.responseId).toBe(testData.responseId);
        expect(result.merchantIp).toBe(testData.merchantIp);
        expect(result.totalAmount).toBe(testData.totalAmount);
        expect(result.notificationUrl).toBe(testData.notificationUrl);
        expect(result.responseUrl).toBe(testData.responseUrl);
        expect(result.benFname).toBe(testData.benFname);
        expect(result.benMname).toBe(testData.benMname);
        expect(result.benLname).toBe(testData.benLname);
        expect(result.benAddress1).toBe(testData.benAddress1);
        expect(result.benAddress2).toBe(testData.benAddress2);
        expect(result.benCity).toBe(testData.benCity);
        expect(result.benState).toBe(testData.benState);
        expect(result.benCountry).toBe(testData.benCountry);
        expect(result.benZip).toBe(testData.benZip);
        expect(result.benEmail).toBe(testData.benEmail);
        expect(result.benPhone).toBe(testData.benPhone);
        expect(result.disbursementAmount).toBe(testData.disbursementAmount);
        expect(result.currency).toBe(testData.currency);
        expect(result.disbursementMethod).toBe(testData.disbursementMethod);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.bnBankCode).toBe(testData.bnBankCode);
        expect(result.gcashAccountNo).toBe(testData.gcashAccountNo);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.walletAccountNo).toBe(testData.walletAccountNo);
        expect(result.assignedRef).toBe(testData.assignedRef);
        expect(result.expiryDate).toBe(testData.expiryDate);
        expect(result.pickupCenter).toBe(testData.pickupCenter);
        expect(result.status).toBe(testData.status);
        expect(result.statusInfo).toBe(testData.statusInfo);
        expect(result.statusText).toBe(testData.statusText);
        expect(result.disbursementInfo).toBe(testData.disbursementInfo);
        expect(result.fees).toBe(testData.fees);
        expect(result.reason).toBe(testData.reason);
        expect(result.isCorporate).toBe(testData.isCorporate);
        expect(result.mode).toBe(testData.mode);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('GET       /disbursementreqs/:id    Should return one disbursementreq', async () => {
        const res = await api('GET', `/api/disbursementreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.mid).toBe(testData.mid);
        expect(result.requestId).toBe(testData.requestId);
        expect(result.responseId).toBe(testData.responseId);
        expect(result.merchantIp).toBe(testData.merchantIp);
        expect(result.totalAmount).toBe(testData.totalAmount);
        expect(result.notificationUrl).toBe(testData.notificationUrl);
        expect(result.responseUrl).toBe(testData.responseUrl);
        expect(result.benFname).toBe(testData.benFname);
        expect(result.benMname).toBe(testData.benMname);
        expect(result.benLname).toBe(testData.benLname);
        expect(result.benAddress1).toBe(testData.benAddress1);
        expect(result.benAddress2).toBe(testData.benAddress2);
        expect(result.benCity).toBe(testData.benCity);
        expect(result.benState).toBe(testData.benState);
        expect(result.benCountry).toBe(testData.benCountry);
        expect(result.benZip).toBe(testData.benZip);
        expect(result.benEmail).toBe(testData.benEmail);
        expect(result.benPhone).toBe(testData.benPhone);
        expect(result.disbursementAmount).toBe(testData.disbursementAmount);
        expect(result.currency).toBe(testData.currency);
        expect(result.disbursementMethod).toBe(testData.disbursementMethod);
        expect(result.bankAccountNo).toBe(testData.bankAccountNo);
        expect(result.bnBankCode).toBe(testData.bnBankCode);
        expect(result.gcashAccountNo).toBe(testData.gcashAccountNo);
        expect(result.walletId).toBe(testData.walletId);
        expect(result.walletAccountNo).toBe(testData.walletAccountNo);
        expect(result.assignedRef).toBe(testData.assignedRef);
        expect(result.expiryDate).toBe(testData.expiryDate);
        expect(result.pickupCenter).toBe(testData.pickupCenter);
        expect(result.status).toBe(testData.status);
        expect(result.statusInfo).toBe(testData.statusInfo);
        expect(result.statusText).toBe(testData.statusText);
        expect(result.disbursementInfo).toBe(testData.disbursementInfo);
        expect(result.fees).toBe(testData.fees);
        expect(result.reason).toBe(testData.reason);
        expect(result.isCorporate).toBe(testData.isCorporate);
        expect(result.mode).toBe(testData.mode);
        expect(result.tenantId).toBe(testData.tenantId);
    });

    test('PUT       /disbursementreqs/:id    Should update the disbursementreq', async () => {
        const res = await api('PUT', `/api/disbursementreqs/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.mid).toBe(testDataUpdated.mid);
        expect(result.requestId).toBe(testDataUpdated.requestId);
        expect(result.responseId).toBe(testDataUpdated.responseId);
        expect(result.merchantIp).toBe(testDataUpdated.merchantIp);
        expect(result.totalAmount).toBe(testDataUpdated.totalAmount);
        expect(result.notificationUrl).toBe(testDataUpdated.notificationUrl);
        expect(result.responseUrl).toBe(testDataUpdated.responseUrl);
        expect(result.benFname).toBe(testDataUpdated.benFname);
        expect(result.benMname).toBe(testDataUpdated.benMname);
        expect(result.benLname).toBe(testDataUpdated.benLname);
        expect(result.benAddress1).toBe(testDataUpdated.benAddress1);
        expect(result.benAddress2).toBe(testDataUpdated.benAddress2);
        expect(result.benCity).toBe(testDataUpdated.benCity);
        expect(result.benState).toBe(testDataUpdated.benState);
        expect(result.benCountry).toBe(testDataUpdated.benCountry);
        expect(result.benZip).toBe(testDataUpdated.benZip);
        expect(result.benEmail).toBe(testDataUpdated.benEmail);
        expect(result.benPhone).toBe(testDataUpdated.benPhone);
        expect(result.disbursementAmount).toBe(testDataUpdated.disbursementAmount);
        expect(result.currency).toBe(testDataUpdated.currency);
        expect(result.disbursementMethod).toBe(testDataUpdated.disbursementMethod);
        expect(result.bankAccountNo).toBe(testDataUpdated.bankAccountNo);
        expect(result.bnBankCode).toBe(testDataUpdated.bnBankCode);
        expect(result.gcashAccountNo).toBe(testDataUpdated.gcashAccountNo);
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.walletAccountNo).toBe(testDataUpdated.walletAccountNo);
        expect(result.assignedRef).toBe(testDataUpdated.assignedRef);
        expect(result.expiryDate).toBe(testDataUpdated.expiryDate);
        expect(result.pickupCenter).toBe(testDataUpdated.pickupCenter);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.statusInfo).toBe(testDataUpdated.statusInfo);
        expect(result.statusText).toBe(testDataUpdated.statusText);
        expect(result.disbursementInfo).toBe(testDataUpdated.disbursementInfo);
        expect(result.fees).toBe(testDataUpdated.fees);
        expect(result.reason).toBe(testDataUpdated.reason);
        expect(result.isCorporate).toBe(testDataUpdated.isCorporate);
        expect(result.mode).toBe(testDataUpdated.mode);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
    });

    test('PUT       /disbursementreqs/:id    Should fail because we want to update the disbursementreq with a invalid email', async () => {
        const res = await api('PUT', `/api/disbursementreqs/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /disbursementreqs/:id    Should delete the disbursementreq', async () => {
        const res = await api('DELETE', `/api/disbursementreqs/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /disbursementreqs/:id    Should return with a 404, because we just deleted the disbursementreq', async () => {
        const res = await api('GET', `/api/disbursementreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /disbursementreqs/:id    Should return with a 404, because we just deleted the disbursementreq', async () => {
        const res = await api('DELETE', `/api/disbursementreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /disbursementreqs/:id    Should return with a 404, because we just deleted the disbursementreq', async () => {
        const res = await api('PUT', `/api/disbursementreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
