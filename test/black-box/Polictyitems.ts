import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/polictyitems', () => {

    const keys = [
        'policyId', 'policyType', 'status'
    ];

    const testData = {
        policyId: undefined, // TODO: Add test value
        policyType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyId: undefined, // TODO: Add test value
        policyType: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /polictyitems        Should create a new polictyitems', async () => {
        const res = await api('POST', '/api/polictyitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /polictyitems        Should fail because we want to create a empty polictyitems', async () => {
        const res = await api('POST', '/api/polictyitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /polictyitems        Should list of polictyitemss with our new create one', async () => {
        const res = await api('GET', '/api/polictyitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyId).toBe(testData.policyId);
        expect(result.policyType).toBe(testData.policyType);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /polictyitems/:id    Should return one polictyitems', async () => {
        const res = await api('GET', `/api/polictyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyId).toBe(testData.policyId);
        expect(result.policyType).toBe(testData.policyType);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /polictyitems/:id    Should update the polictyitems', async () => {
        const res = await api('PUT', `/api/polictyitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyId).toBe(testDataUpdated.policyId);
        expect(result.policyType).toBe(testDataUpdated.policyType);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /polictyitems/:id    Should fail because we want to update the polictyitems with a invalid email', async () => {
        const res = await api('PUT', `/api/polictyitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /polictyitems/:id    Should delete the polictyitems', async () => {
        const res = await api('DELETE', `/api/polictyitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /polictyitems/:id    Should return with a 404, because we just deleted the polictyitems', async () => {
        const res = await api('GET', `/api/polictyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /polictyitems/:id    Should return with a 404, because we just deleted the polictyitems', async () => {
        const res = await api('DELETE', `/api/polictyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /polictyitems/:id    Should return with a 404, because we just deleted the polictyitems', async () => {
        const res = await api('PUT', `/api/polictyitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
