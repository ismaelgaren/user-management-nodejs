import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/emailitems', () => {

    const keys = [
        'groupId', 'email', 'status'
    ];

    const testData = {
        groupId: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        groupId: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /emailitems        Should create a new emailitems', async () => {
        const res = await api('POST', '/api/emailitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /emailitems        Should fail because we want to create a empty emailitems', async () => {
        const res = await api('POST', '/api/emailitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /emailitems        Should list of emailitemss with our new create one', async () => {
        const res = await api('GET', '/api/emailitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.groupId).toBe(testData.groupId);
        expect(result.email).toBe(testData.email);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /emailitems/:id    Should return one emailitems', async () => {
        const res = await api('GET', `/api/emailitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testData.groupId);
        expect(result.email).toBe(testData.email);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /emailitems/:id    Should update the emailitems', async () => {
        const res = await api('PUT', `/api/emailitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testDataUpdated.groupId);
        expect(result.email).toBe(testDataUpdated.email);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /emailitems/:id    Should fail because we want to update the emailitems with a invalid email', async () => {
        const res = await api('PUT', `/api/emailitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /emailitems/:id    Should delete the emailitems', async () => {
        const res = await api('DELETE', `/api/emailitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /emailitems/:id    Should return with a 404, because we just deleted the emailitems', async () => {
        const res = await api('GET', `/api/emailitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /emailitems/:id    Should return with a 404, because we just deleted the emailitems', async () => {
        const res = await api('DELETE', `/api/emailitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /emailitems/:id    Should return with a 404, because we just deleted the emailitems', async () => {
        const res = await api('PUT', `/api/emailitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
