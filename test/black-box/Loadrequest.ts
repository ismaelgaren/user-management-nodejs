import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/loadrequests', () => {

    const keys = [
        'requestId', 'amount'
    ];

    const testData = {
        requestId: undefined, // TODO: Add test value
        amount: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        requestId: undefined, // TODO: Add test value
        amount: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /loadrequests        Should create a new loadrequest', async () => {
        const res = await api('POST', '/api/loadrequests', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /loadrequests        Should fail because we want to create a empty loadrequest', async () => {
        const res = await api('POST', '/api/loadrequests', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /loadrequests        Should list of loadrequests with our new create one', async () => {
        const res = await api('GET', '/api/loadrequests', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.requestId).toBe(testData.requestId);
        expect(result.amount).toBe(testData.amount);
    });

    test('GET       /loadrequests/:id    Should return one loadrequest', async () => {
        const res = await api('GET', `/api/loadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testData.requestId);
        expect(result.amount).toBe(testData.amount);
    });

    test('PUT       /loadrequests/:id    Should update the loadrequest', async () => {
        const res = await api('PUT', `/api/loadrequests/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.requestId).toBe(testDataUpdated.requestId);
        expect(result.amount).toBe(testDataUpdated.amount);
    });

    test('PUT       /loadrequests/:id    Should fail because we want to update the loadrequest with a invalid email', async () => {
        const res = await api('PUT', `/api/loadrequests/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /loadrequests/:id    Should delete the loadrequest', async () => {
        const res = await api('DELETE', `/api/loadrequests/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /loadrequests/:id    Should return with a 404, because we just deleted the loadrequest', async () => {
        const res = await api('GET', `/api/loadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /loadrequests/:id    Should return with a 404, because we just deleted the loadrequest', async () => {
        const res = await api('DELETE', `/api/loadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /loadrequests/:id    Should return with a 404, because we just deleted the loadrequest', async () => {
        const res = await api('PUT', `/api/loadrequests/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
