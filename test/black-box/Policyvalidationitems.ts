import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policyvalidationitems', () => {

    const keys = [
        'policyItemId', 'inboundParamId', 'required', 'notNull', 'violateRequiredRespCode', 'violateNotNullRespCode', 'status'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        required: undefined, // TODO: Add test value
        notNull: undefined, // TODO: Add test value
        violateRequiredRespCode: undefined, // TODO: Add test value
        violateNotNullRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        required: undefined, // TODO: Add test value
        notNull: undefined, // TODO: Add test value
        violateRequiredRespCode: undefined, // TODO: Add test value
        violateNotNullRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policyvalidationitems        Should create a new policyvalidationitems', async () => {
        const res = await api('POST', '/api/policyvalidationitems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policyvalidationitems        Should fail because we want to create a empty policyvalidationitems', async () => {
        const res = await api('POST', '/api/policyvalidationitems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policyvalidationitems        Should list of policyvalidationitemss with our new create one', async () => {
        const res = await api('GET', '/api/policyvalidationitems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.required).toBe(testData.required);
        expect(result.notNull).toBe(testData.notNull);
        expect(result.violateRequiredRespCode).toBe(testData.violateRequiredRespCode);
        expect(result.violateNotNullRespCode).toBe(testData.violateNotNullRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policyvalidationitems/:id    Should return one policyvalidationitems', async () => {
        const res = await api('GET', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.required).toBe(testData.required);
        expect(result.notNull).toBe(testData.notNull);
        expect(result.violateRequiredRespCode).toBe(testData.violateRequiredRespCode);
        expect(result.violateNotNullRespCode).toBe(testData.violateNotNullRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policyvalidationitems/:id    Should update the policyvalidationitems', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.inboundParamId).toBe(testDataUpdated.inboundParamId);
        expect(result.required).toBe(testDataUpdated.required);
        expect(result.notNull).toBe(testDataUpdated.notNull);
        expect(result.violateRequiredRespCode).toBe(testDataUpdated.violateRequiredRespCode);
        expect(result.violateNotNullRespCode).toBe(testDataUpdated.violateNotNullRespCode);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policyvalidationitems/:id    Should fail because we want to update the policyvalidationitems with a invalid email', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policyvalidationitems/:id    Should delete the policyvalidationitems', async () => {
        const res = await api('DELETE', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitems', async () => {
        const res = await api('GET', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitems', async () => {
        const res = await api('DELETE', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policyvalidationitems/:id    Should return with a 404, because we just deleted the policyvalidationitems', async () => {
        const res = await api('PUT', `/api/policyvalidationitems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
