import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/transactions', () => {

    const keys = [
        'test',
    ];

    const testData = {
        test: undefined, // TODO: Add test value
    };

    const testDataUpdated = {
        test: undefined, // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token,
        };
    });

    test('POST      /transactions        Should create a new transactions', async () => {
        const res = await api('POST', '/api/transactions', {
            token,
            body: testData,
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /transactions        Should fail because we want to create a empty transactions', async () => {
        const res = await api('POST', '/api/transactions', {
            token,
            body: {},
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /transactions        Should list of transactionss with our new create one', async () => {
        const res = await api('GET', '/api/transactions', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.test).toBe(testData.test);
    });

    test('GET       /transactions/:id    Should return one transactions', async () => {
        const res = await api('GET', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.test).toBe(testData.test);
    });

    test('PUT       /transactions/:id    Should update the transactions', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, {
            token,
            body: testDataUpdated,
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.test).toBe(testDataUpdated.test);
    });

    test('PUT       /transactions/:id    Should fail because we want to update the transactions with a invalid email', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, {
            token,
            body: {
                email: 'abc',
            },
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /transactions/:id    Should delete the transactions', async () => {
        const res = await api('DELETE', `/api/transactions/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /transactions/:id    Should return with a 404, because we just deleted the transactions', async () => {
        const res = await api('GET', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /transactions/:id    Should return with a 404, because we just deleted the transactions', async () => {
        const res = await api('DELETE', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /transactions/:id    Should return with a 404, because we just deleted the transactions', async () => {
        const res = await api('PUT', `/api/transactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
