import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/cardtransactions', () => {

    const keys = [
        'gatewayId', 'requestId', 'originalTrxId', 'originalReqId', 'mid', 'midName', 'merchantId', 'amount', 'currency', 'trxType', 'responseTime', 'procrRespId', 'procRespCode', 'prcoAuthCode', 'procId', 'maskedCard', 'cardType', 'cardholderName', 'fname', 'lname', 'mname', 'customerIp', 'state', 'country', 'city', 'address1', 'address2', 'email', 'phone', 'postal', 'paymentType', 'binCountry', 'ipCountry', 'responseCode', 'responseMsg', 'transactionId'
    ];

    const testData = {
        gatewayId: undefined, // TODO: Add test value
        requestId: undefined, // TODO: Add test value
        originalTrxId: undefined, // TODO: Add test value
        originalReqId: undefined, // TODO: Add test value
        mid: undefined, // TODO: Add test value
        midName: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        currency: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        responseTime: undefined, // TODO: Add test value
        procrRespId: undefined, // TODO: Add test value
        procRespCode: undefined, // TODO: Add test value
        prcoAuthCode: undefined, // TODO: Add test value
        procId: undefined, // TODO: Add test value
        maskedCard: undefined, // TODO: Add test value
        cardType: undefined, // TODO: Add test value
        cardholderName: undefined, // TODO: Add test value
        fname: undefined, // TODO: Add test value
        lname: undefined, // TODO: Add test value
        mname: undefined, // TODO: Add test value
        customerIp: undefined, // TODO: Add test value
        state: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        city: undefined, // TODO: Add test value
        address1: undefined, // TODO: Add test value
        address2: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        phone: undefined, // TODO: Add test value
        postal: undefined, // TODO: Add test value
        paymentType: undefined, // TODO: Add test value
        binCountry: undefined, // TODO: Add test value
        ipCountry: undefined, // TODO: Add test value
        responseCode: undefined, // TODO: Add test value
        responseMsg: undefined, // TODO: Add test value
        transactionId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        gatewayId: undefined, // TODO: Add test value
        requestId: undefined, // TODO: Add test value
        originalTrxId: undefined, // TODO: Add test value
        originalReqId: undefined, // TODO: Add test value
        mid: undefined, // TODO: Add test value
        midName: undefined, // TODO: Add test value
        merchantId: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        currency: undefined, // TODO: Add test value
        trxType: undefined, // TODO: Add test value
        responseTime: undefined, // TODO: Add test value
        procrRespId: undefined, // TODO: Add test value
        procRespCode: undefined, // TODO: Add test value
        prcoAuthCode: undefined, // TODO: Add test value
        procId: undefined, // TODO: Add test value
        maskedCard: undefined, // TODO: Add test value
        cardType: undefined, // TODO: Add test value
        cardholderName: undefined, // TODO: Add test value
        fname: undefined, // TODO: Add test value
        lname: undefined, // TODO: Add test value
        mname: undefined, // TODO: Add test value
        customerIp: undefined, // TODO: Add test value
        state: undefined, // TODO: Add test value
        country: undefined, // TODO: Add test value
        city: undefined, // TODO: Add test value
        address1: undefined, // TODO: Add test value
        address2: undefined, // TODO: Add test value
        email: undefined, // TODO: Add test value
        phone: undefined, // TODO: Add test value
        postal: undefined, // TODO: Add test value
        paymentType: undefined, // TODO: Add test value
        binCountry: undefined, // TODO: Add test value
        ipCountry: undefined, // TODO: Add test value
        responseCode: undefined, // TODO: Add test value
        responseMsg: undefined, // TODO: Add test value
        transactionId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /cardtransactions        Should create a new cardtransaction', async () => {
        const res = await api('POST', '/api/cardtransactions', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /cardtransactions        Should fail because we want to create a empty cardtransaction', async () => {
        const res = await api('POST', '/api/cardtransactions', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /cardtransactions        Should list of cardtransactions with our new create one', async () => {
        const res = await api('GET', '/api/cardtransactions', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.gatewayId).toBe(testData.gatewayId);
        expect(result.requestId).toBe(testData.requestId);
        expect(result.originalTrxId).toBe(testData.originalTrxId);
        expect(result.originalReqId).toBe(testData.originalReqId);
        expect(result.mid).toBe(testData.mid);
        expect(result.midName).toBe(testData.midName);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.amount).toBe(testData.amount);
        expect(result.currency).toBe(testData.currency);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.responseTime).toBe(testData.responseTime);
        expect(result.procrRespId).toBe(testData.procrRespId);
        expect(result.procRespCode).toBe(testData.procRespCode);
        expect(result.prcoAuthCode).toBe(testData.prcoAuthCode);
        expect(result.procId).toBe(testData.procId);
        expect(result.maskedCard).toBe(testData.maskedCard);
        expect(result.cardType).toBe(testData.cardType);
        expect(result.cardholderName).toBe(testData.cardholderName);
        expect(result.fname).toBe(testData.fname);
        expect(result.lname).toBe(testData.lname);
        expect(result.mname).toBe(testData.mname);
        expect(result.customerIp).toBe(testData.customerIp);
        expect(result.state).toBe(testData.state);
        expect(result.country).toBe(testData.country);
        expect(result.city).toBe(testData.city);
        expect(result.address1).toBe(testData.address1);
        expect(result.address2).toBe(testData.address2);
        expect(result.email).toBe(testData.email);
        expect(result.phone).toBe(testData.phone);
        expect(result.postal).toBe(testData.postal);
        expect(result.paymentType).toBe(testData.paymentType);
        expect(result.binCountry).toBe(testData.binCountry);
        expect(result.ipCountry).toBe(testData.ipCountry);
        expect(result.responseCode).toBe(testData.responseCode);
        expect(result.responseMsg).toBe(testData.responseMsg);
        expect(result.transactionId).toBe(testData.transactionId);
    });

    test('GET       /cardtransactions/:id    Should return one cardtransaction', async () => {
        const res = await api('GET', `/api/cardtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.gatewayId).toBe(testData.gatewayId);
        expect(result.requestId).toBe(testData.requestId);
        expect(result.originalTrxId).toBe(testData.originalTrxId);
        expect(result.originalReqId).toBe(testData.originalReqId);
        expect(result.mid).toBe(testData.mid);
        expect(result.midName).toBe(testData.midName);
        expect(result.merchantId).toBe(testData.merchantId);
        expect(result.amount).toBe(testData.amount);
        expect(result.currency).toBe(testData.currency);
        expect(result.trxType).toBe(testData.trxType);
        expect(result.responseTime).toBe(testData.responseTime);
        expect(result.procrRespId).toBe(testData.procrRespId);
        expect(result.procRespCode).toBe(testData.procRespCode);
        expect(result.prcoAuthCode).toBe(testData.prcoAuthCode);
        expect(result.procId).toBe(testData.procId);
        expect(result.maskedCard).toBe(testData.maskedCard);
        expect(result.cardType).toBe(testData.cardType);
        expect(result.cardholderName).toBe(testData.cardholderName);
        expect(result.fname).toBe(testData.fname);
        expect(result.lname).toBe(testData.lname);
        expect(result.mname).toBe(testData.mname);
        expect(result.customerIp).toBe(testData.customerIp);
        expect(result.state).toBe(testData.state);
        expect(result.country).toBe(testData.country);
        expect(result.city).toBe(testData.city);
        expect(result.address1).toBe(testData.address1);
        expect(result.address2).toBe(testData.address2);
        expect(result.email).toBe(testData.email);
        expect(result.phone).toBe(testData.phone);
        expect(result.postal).toBe(testData.postal);
        expect(result.paymentType).toBe(testData.paymentType);
        expect(result.binCountry).toBe(testData.binCountry);
        expect(result.ipCountry).toBe(testData.ipCountry);
        expect(result.responseCode).toBe(testData.responseCode);
        expect(result.responseMsg).toBe(testData.responseMsg);
        expect(result.transactionId).toBe(testData.transactionId);
    });

    test('PUT       /cardtransactions/:id    Should update the cardtransaction', async () => {
        const res = await api('PUT', `/api/cardtransactions/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.gatewayId).toBe(testDataUpdated.gatewayId);
        expect(result.requestId).toBe(testDataUpdated.requestId);
        expect(result.originalTrxId).toBe(testDataUpdated.originalTrxId);
        expect(result.originalReqId).toBe(testDataUpdated.originalReqId);
        expect(result.mid).toBe(testDataUpdated.mid);
        expect(result.midName).toBe(testDataUpdated.midName);
        expect(result.merchantId).toBe(testDataUpdated.merchantId);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.currency).toBe(testDataUpdated.currency);
        expect(result.trxType).toBe(testDataUpdated.trxType);
        expect(result.responseTime).toBe(testDataUpdated.responseTime);
        expect(result.procrRespId).toBe(testDataUpdated.procrRespId);
        expect(result.procRespCode).toBe(testDataUpdated.procRespCode);
        expect(result.prcoAuthCode).toBe(testDataUpdated.prcoAuthCode);
        expect(result.procId).toBe(testDataUpdated.procId);
        expect(result.maskedCard).toBe(testDataUpdated.maskedCard);
        expect(result.cardType).toBe(testDataUpdated.cardType);
        expect(result.cardholderName).toBe(testDataUpdated.cardholderName);
        expect(result.fname).toBe(testDataUpdated.fname);
        expect(result.lname).toBe(testDataUpdated.lname);
        expect(result.mname).toBe(testDataUpdated.mname);
        expect(result.customerIp).toBe(testDataUpdated.customerIp);
        expect(result.state).toBe(testDataUpdated.state);
        expect(result.country).toBe(testDataUpdated.country);
        expect(result.city).toBe(testDataUpdated.city);
        expect(result.address1).toBe(testDataUpdated.address1);
        expect(result.address2).toBe(testDataUpdated.address2);
        expect(result.email).toBe(testDataUpdated.email);
        expect(result.phone).toBe(testDataUpdated.phone);
        expect(result.postal).toBe(testDataUpdated.postal);
        expect(result.paymentType).toBe(testDataUpdated.paymentType);
        expect(result.binCountry).toBe(testDataUpdated.binCountry);
        expect(result.ipCountry).toBe(testDataUpdated.ipCountry);
        expect(result.responseCode).toBe(testDataUpdated.responseCode);
        expect(result.responseMsg).toBe(testDataUpdated.responseMsg);
        expect(result.transactionId).toBe(testDataUpdated.transactionId);
    });

    test('PUT       /cardtransactions/:id    Should fail because we want to update the cardtransaction with a invalid email', async () => {
        const res = await api('PUT', `/api/cardtransactions/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /cardtransactions/:id    Should delete the cardtransaction', async () => {
        const res = await api('DELETE', `/api/cardtransactions/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /cardtransactions/:id    Should return with a 404, because we just deleted the cardtransaction', async () => {
        const res = await api('GET', `/api/cardtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /cardtransactions/:id    Should return with a 404, because we just deleted the cardtransaction', async () => {
        const res = await api('DELETE', `/api/cardtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /cardtransactions/:id    Should return with a 404, because we just deleted the cardtransaction', async () => {
        const res = await api('PUT', `/api/cardtransactions/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
