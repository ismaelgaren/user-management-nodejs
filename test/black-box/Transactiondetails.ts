import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/transactiondetails', () => {

    const keys = [
        'transactionId', 'itemId', 'itemCode', 'itemDescription', 'amount', 'quantity', 'total', 'quantityAfterPurchase'
    ];

    const testData = {
        transactionId: undefined, // TODO: Add test value
        itemId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        itemDescription: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        quantity: undefined, // TODO: Add test value
        total: undefined, // TODO: Add test value
        quantityAfterPurchase: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        transactionId: undefined, // TODO: Add test value
        itemId: undefined, // TODO: Add test value
        itemCode: undefined, // TODO: Add test value
        itemDescription: undefined, // TODO: Add test value
        amount: undefined, // TODO: Add test value
        quantity: undefined, // TODO: Add test value
        total: undefined, // TODO: Add test value
        quantityAfterPurchase: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /transactiondetails        Should create a new transactiondetails', async () => {
        const res = await api('POST', '/api/transactiondetails', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /transactiondetails        Should fail because we want to create a empty transactiondetails', async () => {
        const res = await api('POST', '/api/transactiondetails', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /transactiondetails        Should list of transactiondetailss with our new create one', async () => {
        const res = await api('GET', '/api/transactiondetails', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.itemId).toBe(testData.itemId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.itemDescription).toBe(testData.itemDescription);
        expect(result.amount).toBe(testData.amount);
        expect(result.quantity).toBe(testData.quantity);
        expect(result.total).toBe(testData.total);
        expect(result.quantityAfterPurchase).toBe(testData.quantityAfterPurchase);
    });

    test('GET       /transactiondetails/:id    Should return one transactiondetails', async () => {
        const res = await api('GET', `/api/transactiondetails/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.transactionId).toBe(testData.transactionId);
        expect(result.itemId).toBe(testData.itemId);
        expect(result.itemCode).toBe(testData.itemCode);
        expect(result.itemDescription).toBe(testData.itemDescription);
        expect(result.amount).toBe(testData.amount);
        expect(result.quantity).toBe(testData.quantity);
        expect(result.total).toBe(testData.total);
        expect(result.quantityAfterPurchase).toBe(testData.quantityAfterPurchase);
    });

    test('PUT       /transactiondetails/:id    Should update the transactiondetails', async () => {
        const res = await api('PUT', `/api/transactiondetails/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.transactionId).toBe(testDataUpdated.transactionId);
        expect(result.itemId).toBe(testDataUpdated.itemId);
        expect(result.itemCode).toBe(testDataUpdated.itemCode);
        expect(result.itemDescription).toBe(testDataUpdated.itemDescription);
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.quantity).toBe(testDataUpdated.quantity);
        expect(result.total).toBe(testDataUpdated.total);
        expect(result.quantityAfterPurchase).toBe(testDataUpdated.quantityAfterPurchase);
    });

    test('PUT       /transactiondetails/:id    Should fail because we want to update the transactiondetails with a invalid email', async () => {
        const res = await api('PUT', `/api/transactiondetails/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /transactiondetails/:id    Should delete the transactiondetails', async () => {
        const res = await api('DELETE', `/api/transactiondetails/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /transactiondetails/:id    Should return with a 404, because we just deleted the transactiondetails', async () => {
        const res = await api('GET', `/api/transactiondetails/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /transactiondetails/:id    Should return with a 404, because we just deleted the transactiondetails', async () => {
        const res = await api('DELETE', `/api/transactiondetails/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /transactiondetails/:id    Should return with a 404, because we just deleted the transactiondetails', async () => {
        const res = await api('PUT', `/api/transactiondetails/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
