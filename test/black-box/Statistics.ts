import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/statistics', () => {

    const keys = [
        'statisticsTypeId', 'value'
    ];

    const testData = {
        statisticsTypeId: undefined, // TODO: Add test value
        value: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        statisticsTypeId: undefined, // TODO: Add test value
        value: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /statistics        Should create a new statistics', async () => {
        const res = await api('POST', '/api/statistics', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /statistics        Should fail because we want to create a empty statistics', async () => {
        const res = await api('POST', '/api/statistics', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /statistics        Should list of statisticss with our new create one', async () => {
        const res = await api('GET', '/api/statistics', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.statisticsTypeId).toBe(testData.statisticsTypeId);
        expect(result.value).toBe(testData.value);
    });

    test('GET       /statistics/:id    Should return one statistics', async () => {
        const res = await api('GET', `/api/statistics/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.statisticsTypeId).toBe(testData.statisticsTypeId);
        expect(result.value).toBe(testData.value);
    });

    test('PUT       /statistics/:id    Should update the statistics', async () => {
        const res = await api('PUT', `/api/statistics/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.statisticsTypeId).toBe(testDataUpdated.statisticsTypeId);
        expect(result.value).toBe(testDataUpdated.value);
    });

    test('PUT       /statistics/:id    Should fail because we want to update the statistics with a invalid email', async () => {
        const res = await api('PUT', `/api/statistics/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /statistics/:id    Should delete the statistics', async () => {
        const res = await api('DELETE', `/api/statistics/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /statistics/:id    Should return with a 404, because we just deleted the statistics', async () => {
        const res = await api('GET', `/api/statistics/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /statistics/:id    Should return with a 404, because we just deleted the statistics', async () => {
        const res = await api('DELETE', `/api/statistics/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /statistics/:id    Should return with a 404, because we just deleted the statistics', async () => {
        const res = await api('PUT', `/api/statistics/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
