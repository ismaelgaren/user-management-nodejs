import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/centineltransferreqs', () => {

    const keys = [
        'amount', 'sourceWalletId', 'destinationWalletId', 'signature', 'externalRef', 'remarks', 'transactionGroup'
    ];

    const testData = {
        amount: undefined, // TODO: Add test value
        sourceWalletId: undefined, // TODO: Add test value
        destinationWalletId: undefined, // TODO: Add test value
        signature: undefined, // TODO: Add test value
        externalRef: undefined, // TODO: Add test value
        remarks: undefined, // TODO: Add test value
        transactionGroup: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        amount: undefined, // TODO: Add test value
        sourceWalletId: undefined, // TODO: Add test value
        destinationWalletId: undefined, // TODO: Add test value
        signature: undefined, // TODO: Add test value
        externalRef: undefined, // TODO: Add test value
        remarks: undefined, // TODO: Add test value
        transactionGroup: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /centineltransferreqs        Should create a new centineltransferreq', async () => {
        const res = await api('POST', '/api/centineltransferreqs', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /centineltransferreqs        Should fail because we want to create a empty centineltransferreq', async () => {
        const res = await api('POST', '/api/centineltransferreqs', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /centineltransferreqs        Should list of centineltransferreqs with our new create one', async () => {
        const res = await api('GET', '/api/centineltransferreqs', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.amount).toBe(testData.amount);
        expect(result.sourceWalletId).toBe(testData.sourceWalletId);
        expect(result.destinationWalletId).toBe(testData.destinationWalletId);
        expect(result.signature).toBe(testData.signature);
        expect(result.externalRef).toBe(testData.externalRef);
        expect(result.remarks).toBe(testData.remarks);
        expect(result.transactionGroup).toBe(testData.transactionGroup);
    });

    test('GET       /centineltransferreqs/:id    Should return one centineltransferreq', async () => {
        const res = await api('GET', `/api/centineltransferreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.amount).toBe(testData.amount);
        expect(result.sourceWalletId).toBe(testData.sourceWalletId);
        expect(result.destinationWalletId).toBe(testData.destinationWalletId);
        expect(result.signature).toBe(testData.signature);
        expect(result.externalRef).toBe(testData.externalRef);
        expect(result.remarks).toBe(testData.remarks);
        expect(result.transactionGroup).toBe(testData.transactionGroup);
    });

    test('PUT       /centineltransferreqs/:id    Should update the centineltransferreq', async () => {
        const res = await api('PUT', `/api/centineltransferreqs/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.amount).toBe(testDataUpdated.amount);
        expect(result.sourceWalletId).toBe(testDataUpdated.sourceWalletId);
        expect(result.destinationWalletId).toBe(testDataUpdated.destinationWalletId);
        expect(result.signature).toBe(testDataUpdated.signature);
        expect(result.externalRef).toBe(testDataUpdated.externalRef);
        expect(result.remarks).toBe(testDataUpdated.remarks);
        expect(result.transactionGroup).toBe(testDataUpdated.transactionGroup);
    });

    test('PUT       /centineltransferreqs/:id    Should fail because we want to update the centineltransferreq with a invalid email', async () => {
        const res = await api('PUT', `/api/centineltransferreqs/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /centineltransferreqs/:id    Should delete the centineltransferreq', async () => {
        const res = await api('DELETE', `/api/centineltransferreqs/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /centineltransferreqs/:id    Should return with a 404, because we just deleted the centineltransferreq', async () => {
        const res = await api('GET', `/api/centineltransferreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /centineltransferreqs/:id    Should return with a 404, because we just deleted the centineltransferreq', async () => {
        const res = await api('DELETE', `/api/centineltransferreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /centineltransferreqs/:id    Should return with a 404, because we just deleted the centineltransferreq', async () => {
        const res = await api('PUT', `/api/centineltransferreqs/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
