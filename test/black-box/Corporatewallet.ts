import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/corporatewallets', () => {

    const keys = [
        'walletId', 'tenantId', 'balance', 'currencyId', 'status', 'name', 'threshold'
    ];

    const testData = {
        walletId: undefined, // TODO: Add test value
        tenantId: undefined, // TODO: Add test value
        balance: undefined, // TODO: Add test value
        currencyId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        threshold: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        walletId: undefined, // TODO: Add test value
        tenantId: undefined, // TODO: Add test value
        balance: undefined, // TODO: Add test value
        currencyId: undefined, // TODO: Add test value
        status: undefined, // TODO: Add test value
        name: undefined, // TODO: Add test value
        threshold: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /corporatewallets        Should create a new corporatewallet', async () => {
        const res = await api('POST', '/api/corporatewallets', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /corporatewallets        Should fail because we want to create a empty corporatewallet', async () => {
        const res = await api('POST', '/api/corporatewallets', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /corporatewallets        Should list of corporatewallets with our new create one', async () => {
        const res = await api('GET', '/api/corporatewallets', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.walletId).toBe(testData.walletId);
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.balance).toBe(testData.balance);
        expect(result.currencyId).toBe(testData.currencyId);
        expect(result.status).toBe(testData.status);
        expect(result.name).toBe(testData.name);
        expect(result.threshold).toBe(testData.threshold);
    });

    test('GET       /corporatewallets/:id    Should return one corporatewallet', async () => {
        const res = await api('GET', `/api/corporatewallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testData.walletId);
        expect(result.tenantId).toBe(testData.tenantId);
        expect(result.balance).toBe(testData.balance);
        expect(result.currencyId).toBe(testData.currencyId);
        expect(result.status).toBe(testData.status);
        expect(result.name).toBe(testData.name);
        expect(result.threshold).toBe(testData.threshold);
    });

    test('PUT       /corporatewallets/:id    Should update the corporatewallet', async () => {
        const res = await api('PUT', `/api/corporatewallets/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.walletId).toBe(testDataUpdated.walletId);
        expect(result.tenantId).toBe(testDataUpdated.tenantId);
        expect(result.balance).toBe(testDataUpdated.balance);
        expect(result.currencyId).toBe(testDataUpdated.currencyId);
        expect(result.status).toBe(testDataUpdated.status);
        expect(result.name).toBe(testDataUpdated.name);
        expect(result.threshold).toBe(testDataUpdated.threshold);
    });

    test('PUT       /corporatewallets/:id    Should fail because we want to update the corporatewallet with a invalid email', async () => {
        const res = await api('PUT', `/api/corporatewallets/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /corporatewallets/:id    Should delete the corporatewallet', async () => {
        const res = await api('DELETE', `/api/corporatewallets/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /corporatewallets/:id    Should return with a 404, because we just deleted the corporatewallet', async () => {
        const res = await api('GET', `/api/corporatewallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /corporatewallets/:id    Should return with a 404, because we just deleted the corporatewallet', async () => {
        const res = await api('DELETE', `/api/corporatewallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /corporatewallets/:id    Should return with a 404, because we just deleted the corporatewallet', async () => {
        const res = await api('PUT', `/api/corporatewallets/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
