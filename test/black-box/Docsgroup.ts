import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/docsgroups', () => {

    const keys = [
        'groupId', 'groupName'
    ];

    const testData = {
        groupId: undefined, // TODO: Add test value
        groupName: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        groupId: undefined, // TODO: Add test value
        groupName: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /docsgroups        Should create a new docsgroup', async () => {
        const res = await api('POST', '/api/docsgroups', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /docsgroups        Should fail because we want to create a empty docsgroup', async () => {
        const res = await api('POST', '/api/docsgroups', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /docsgroups        Should list of docsgroups with our new create one', async () => {
        const res = await api('GET', '/api/docsgroups', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.groupId).toBe(testData.groupId);
        expect(result.groupName).toBe(testData.groupName);
    });

    test('GET       /docsgroups/:id    Should return one docsgroup', async () => {
        const res = await api('GET', `/api/docsgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testData.groupId);
        expect(result.groupName).toBe(testData.groupName);
    });

    test('PUT       /docsgroups/:id    Should update the docsgroup', async () => {
        const res = await api('PUT', `/api/docsgroups/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.groupId).toBe(testDataUpdated.groupId);
        expect(result.groupName).toBe(testDataUpdated.groupName);
    });

    test('PUT       /docsgroups/:id    Should fail because we want to update the docsgroup with a invalid email', async () => {
        const res = await api('PUT', `/api/docsgroups/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /docsgroups/:id    Should delete the docsgroup', async () => {
        const res = await api('DELETE', `/api/docsgroups/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /docsgroups/:id    Should return with a 404, because we just deleted the docsgroup', async () => {
        const res = await api('GET', `/api/docsgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /docsgroups/:id    Should return with a 404, because we just deleted the docsgroup', async () => {
        const res = await api('DELETE', `/api/docsgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /docsgroups/:id    Should return with a 404, because we just deleted the docsgroup', async () => {
        const res = await api('PUT', `/api/docsgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
