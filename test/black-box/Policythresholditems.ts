import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/policythresholditems', () => {

    const keys = [
        'policyItemId', 'inboundParamId', 'maxValuePerTrx', 'maxValuePerDay', 'violationRespCode', 'status'
    ];

    const testData = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        maxValuePerTrx: undefined, // TODO: Add test value
        maxValuePerDay: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        policyItemId: undefined, // TODO: Add test value
        inboundParamId: undefined, // TODO: Add test value
        maxValuePerTrx: undefined, // TODO: Add test value
        maxValuePerDay: undefined, // TODO: Add test value
        violationRespCode: undefined, // TODO: Add test value
        status: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /policythresholditems        Should create a new policythresholditems', async () => {
        const res = await api('POST', '/api/policythresholditems', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /policythresholditems        Should fail because we want to create a empty policythresholditems', async () => {
        const res = await api('POST', '/api/policythresholditems', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /policythresholditems        Should list of policythresholditemss with our new create one', async () => {
        const res = await api('GET', '/api/policythresholditems', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.maxValuePerTrx).toBe(testData.maxValuePerTrx);
        expect(result.maxValuePerDay).toBe(testData.maxValuePerDay);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('GET       /policythresholditems/:id    Should return one policythresholditems', async () => {
        const res = await api('GET', `/api/policythresholditems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testData.policyItemId);
        expect(result.inboundParamId).toBe(testData.inboundParamId);
        expect(result.maxValuePerTrx).toBe(testData.maxValuePerTrx);
        expect(result.maxValuePerDay).toBe(testData.maxValuePerDay);
        expect(result.violationRespCode).toBe(testData.violationRespCode);
        expect(result.status).toBe(testData.status);
    });

    test('PUT       /policythresholditems/:id    Should update the policythresholditems', async () => {
        const res = await api('PUT', `/api/policythresholditems/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.policyItemId).toBe(testDataUpdated.policyItemId);
        expect(result.inboundParamId).toBe(testDataUpdated.inboundParamId);
        expect(result.maxValuePerTrx).toBe(testDataUpdated.maxValuePerTrx);
        expect(result.maxValuePerDay).toBe(testDataUpdated.maxValuePerDay);
        expect(result.violationRespCode).toBe(testDataUpdated.violationRespCode);
        expect(result.status).toBe(testDataUpdated.status);
    });

    test('PUT       /policythresholditems/:id    Should fail because we want to update the policythresholditems with a invalid email', async () => {
        const res = await api('PUT', `/api/policythresholditems/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /policythresholditems/:id    Should delete the policythresholditems', async () => {
        const res = await api('DELETE', `/api/policythresholditems/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /policythresholditems/:id    Should return with a 404, because we just deleted the policythresholditems', async () => {
        const res = await api('GET', `/api/policythresholditems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /policythresholditems/:id    Should return with a 404, because we just deleted the policythresholditems', async () => {
        const res = await api('DELETE', `/api/policythresholditems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /policythresholditems/:id    Should return with a 404, because we just deleted the policythresholditems', async () => {
        const res = await api('PUT', `/api/policythresholditems/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
