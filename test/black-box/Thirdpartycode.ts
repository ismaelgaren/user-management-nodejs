import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/thirdpartycodes', () => {

    const keys = [
        'pnxCode', 'code', 'thirdPartyOwner', 'description'
    ];

    const testData = {
        pnxCode: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        thirdPartyOwner: undefined, // TODO: Add test value
        description: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        pnxCode: undefined, // TODO: Add test value
        code: undefined, // TODO: Add test value
        thirdPartyOwner: undefined, // TODO: Add test value
        description: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /thirdpartycodes        Should create a new thirdpartycode', async () => {
        const res = await api('POST', '/api/thirdpartycodes', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /thirdpartycodes        Should fail because we want to create a empty thirdpartycode', async () => {
        const res = await api('POST', '/api/thirdpartycodes', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /thirdpartycodes        Should list of thirdpartycodes with our new create one', async () => {
        const res = await api('GET', '/api/thirdpartycodes', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.pnxCode).toBe(testData.pnxCode);
        expect(result.code).toBe(testData.code);
        expect(result.thirdPartyOwner).toBe(testData.thirdPartyOwner);
        expect(result.description).toBe(testData.description);
    });

    test('GET       /thirdpartycodes/:id    Should return one thirdpartycode', async () => {
        const res = await api('GET', `/api/thirdpartycodes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pnxCode).toBe(testData.pnxCode);
        expect(result.code).toBe(testData.code);
        expect(result.thirdPartyOwner).toBe(testData.thirdPartyOwner);
        expect(result.description).toBe(testData.description);
    });

    test('PUT       /thirdpartycodes/:id    Should update the thirdpartycode', async () => {
        const res = await api('PUT', `/api/thirdpartycodes/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.pnxCode).toBe(testDataUpdated.pnxCode);
        expect(result.code).toBe(testDataUpdated.code);
        expect(result.thirdPartyOwner).toBe(testDataUpdated.thirdPartyOwner);
        expect(result.description).toBe(testDataUpdated.description);
    });

    test('PUT       /thirdpartycodes/:id    Should fail because we want to update the thirdpartycode with a invalid email', async () => {
        const res = await api('PUT', `/api/thirdpartycodes/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /thirdpartycodes/:id    Should delete the thirdpartycode', async () => {
        const res = await api('DELETE', `/api/thirdpartycodes/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /thirdpartycodes/:id    Should return with a 404, because we just deleted the thirdpartycode', async () => {
        const res = await api('GET', `/api/thirdpartycodes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /thirdpartycodes/:id    Should return with a 404, because we just deleted the thirdpartycode', async () => {
        const res = await api('DELETE', `/api/thirdpartycodes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /thirdpartycodes/:id    Should return with a 404, because we just deleted the thirdpartycode', async () => {
        const res = await api('PUT', `/api/thirdpartycodes/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
