import { api } from './lib/api';
import { DatabaseResetCommand } from '../../src/console/DatabaseResetCommand';
import { createAdminUser, getToken } from './lib/auth';


describe('/walletgroups', () => {

    const keys = [
        'label', 'bankAccountId'
    ];

    const testData = {
        label: undefined, // TODO: Add test value
        bankAccountId: undefined // TODO: Add test value
    };

    const testDataUpdated = {
        label: undefined, // TODO: Add test value
        bankAccountId: undefined // TODO: Add test value
    };

    let token;
    let auth;
    let createdId;
    beforeAll(async () => {
        const command = new DatabaseResetCommand();
        await command.run();
        await createAdminUser();
        token = getToken();
        auth = {
            token
        };
    });

    test('POST      /walletgroups        Should create a new walletgroup', async () => {
        const res = await api('POST', '/api/walletgroups', {
            token,
            body: testData
        });
        res.expectJson();
        res.expectStatusCode(201);
        res.expectData(keys);
        createdId = res.getData()['id'];
    });

    test('POST      /walletgroups        Should fail because we want to create a empty walletgroup', async () => {
        const res = await api('POST', '/api/walletgroups', {
            token,
            body: {}
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('GET       /walletgroups        Should list of walletgroups with our new create one', async () => {
        const res = await api('GET', '/api/walletgroups', auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);
        const data = res.getData<any[]>();
        expect(data.length).toBe(2);

        const result = data[1];
        expect(result.label).toBe(testData.label);
        expect(result.bankAccountId).toBe(testData.bankAccountId);
    });

    test('GET       /walletgroups/:id    Should return one walletgroup', async () => {
        const res = await api('GET', `/api/walletgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testData.label);
        expect(result.bankAccountId).toBe(testData.bankAccountId);
    });

    test('PUT       /walletgroups/:id    Should update the walletgroup', async () => {
        const res = await api('PUT', `/api/walletgroups/${createdId}`, {
            token,
            body: testDataUpdated
        });
        res.expectJson();
        res.expectStatusCode(200);
        res.expectData(keys);

        const result: any = res.getData();
        expect(result.label).toBe(testDataUpdated.label);
        expect(result.bankAccountId).toBe(testDataUpdated.bankAccountId);
    });

    test('PUT       /walletgroups/:id    Should fail because we want to update the walletgroup with a invalid email', async () => {
        const res = await api('PUT', `/api/walletgroups/${createdId}`, {
            token,
            body: {
                email: 'abc'
            }
        });
        res.expectJson();
        res.expectStatusCode(400);
    });

    test('DELETE    /walletgroups/:id    Should delete the walletgroup', async () => {
        const res = await api('DELETE', `/api/walletgroups/${createdId}`, auth);
        res.expectStatusCode(200);
    });

    /**
     * 404 - NotFound Testing
     */
    test('GET       /walletgroups/:id    Should return with a 404, because we just deleted the walletgroup', async () => {
        const res = await api('GET', `/api/walletgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('DELETE    /walletgroups/:id    Should return with a 404, because we just deleted the walletgroup', async () => {
        const res = await api('DELETE', `/api/walletgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

    test('PUT       /walletgroups/:id    Should return with a 404, because we just deleted the walletgroup', async () => {
        const res = await api('PUT', `/api/walletgroups/${createdId}`, auth);
        res.expectJson();
        res.expectStatusCode(404);
    });

});
