FROM node:8.17.0-jessie
WORKDIR /app
ADD ./package*.json /app/
ADD ./yarn.lock /app/
RUN yarn install
ADD . /app/
RUN yarn run build
CMD ["./start.sh"]