/* tslint:disable */
// Type definitions for bookshelfjs v0.9.3
// Project: http://bookshelfjs.org/
// Definitions by: Andrew Schurman <http://github.com/arcticwaters>, Vesa Poikajärvi <https://github.com/vesse>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.3

import Knex = require('knex');
import knex = require('knex');
import BlueBird = require('bluebird');
import Lodash = require('lodash');
import createError = require('create-error');

interface Bookshelf extends Bookshelf.Events<any> {
  VERSION: string;
  knex: knex;
  Model: typeof Bookshelf.Model;
  Collection: typeof Bookshelf.Collection;

  plugin(name: string | string[] | Function, options?: any): Bookshelf;
  transaction<T>(callback: (transaction: knex.Transaction) => Promise<T>): BlueBird<T>;
}

declare function Bookshelf(knex: knex): Bookshelf;

declare namespace Bookshelf {
  type SortOrder = 'ASC'|'asc'|'DESC'|'desc';

  abstract class Events<T> {
    public on(event?: string, callback?: EventFunction<T>, context?: any): void;
    public off(event?: string): void;
    public trigger(event?: string, ...args: any[]): void;
    public triggerThen(name: string, ...args: any[]): BlueBird<any>;
    public once(event: string, callback: EventFunction<T>, context?: any): void;
  }

  interface IModelBase {
    /** Should be declared as a getter instead of a plain property. */
    hasTimestamps?: boolean | string[];
    /** Should be declared as a getter instead of a plain property. Should be required, but cannot have abstract properties yet. */
    tableName?: string;
  }

  interface ModelBase<T extends Model<any>> extends IModelBase { }
  abstract class ModelBase<T extends Model<any>> extends Events<T | Collection<T>> {
    /** If overriding, must use a getter instead of a plain property. */
    public idAttribute: string;

    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/base/model.js#L178
    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/base/model.js#L213
    public id: any;

    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/base/model.js#L28
    public attributes: any;

    constructor(attributes?: any, options?: ModelOptions);

    public clear(): T;
    public clone(): T;
    public escape(attribute: string): string;
    public format(attributes: any): any;
    public get(attribute: string): any;
    public has(attribute: string): boolean;
    public hasChanged(attribute?: string): boolean;
    public isNew(): boolean;
    public parse(response: Object): Object;
    public previousAttributes(): any;
    public previous(attribute: string): any;
    public related<R extends Model<any>>(relation: string): R | Collection<R>;
    public serialize(options?: SerializeOptions): any;
    public set(attribute?: { [key: string]: any }, options?: SetOptions): T;
    public set(attribute: string, value?: any, options?: SetOptions): T;
    public timestamp(options?: TimestampOptions): any;
    public toJSON(options?: SerializeOptions): any;
    public unset(attribute: string): T;

    // lodash methods
    public invert<R extends {}>(): R;
    public keys(): string[];
    public omit<R extends {}>(predicate?: Lodash.ObjectIterator<any, boolean>, thisArg?: any): R;
    public omit<R extends {}>(...attributes: string[]): R;
    public pairs(): any[][];
    public pick<R extends {}>(predicate?: Lodash.ObjectIterator<any, boolean>, thisArg?: any): R;
    public pick<R extends {}>(...attributes: string[]): R;
    public values(): any[];
  }

  class Model<T extends Model<any>> extends ModelBase<T> {
    public static collection<T extends Model<any>>(models?: T[], options?: CollectionOptions<T>): Collection<T>;
    public static count(column?: string, options?: SyncOptions): BlueBird<number>;
    /** @deprecated use Typescript classes */
    public static extend<T extends Model<any>>(prototypeProperties?: any, classProperties?: any): Function; // should return a type
    public static fetchAll<T extends Model<any>>():
     BlueBird<Collection<T>>;
    /** @deprecated should use `new` objects instead. */
    public static forge<T>(attributes?: any, options?: ModelOptions): T;
    public static where<T>(properties: { [key: string]: any }): T;
    public static where<T>(key: string, operatorOrValue: string | number | boolean, valueIfOperator?: string | number | boolean): T;
    // Pagination Plugin
    public static fetchPage<T extends Model<any>>(options: any): BlueBird<Collection<T>>;
    public belongsTo<R extends Model<any>>(target: { new (...args: any[]): R }, foreignKey?: string, foreignKeyTarget?: string): R;
    public belongsToMany<R extends Model<any>>(target: { new (...args: any[]): R }, table?: string, foreignKey?: string, otherKey?: string): Collection<R>;
    public count(column?: string, options?: SyncOptions): BlueBird<number>;
    public destroy(options?: DestroyOptions): BlueBird<T>;
    public fetch(options?: FetchOptions): BlueBird<T>;
    public fetchById(id?: string): BlueBird<T>;
    public fetchAll(options?: FetchAllOptions): BlueBird<Collection<T>>;
    public hasMany<R extends Model<any>>(target: { new (...args: any[]): R }, foreignKey?: string, foreignKeyTarget?: string): Collection<R>;
    public hasOne<R extends Model<any>>(target: { new (...args: any[]): R }, foreignKey?: string, foreignKeyTarget?: string): R;
    public load(relations: string | string[], options?: LoadOptions): BlueBird<T>;
    public morphMany<R extends Model<any>>(target: { new (...args: any[]): R }, name?: string, columnNames?: string[], morphValue?: string): Collection<R>;
    public morphOne<R extends Model<any>>(target: { new (...args: any[]): R }, name?: string, columnNames?: string[], morphValue?: string): R;
    public morphTo(name: string, columnNames?: string[], ...target: Array<typeof Model>): T;
    public morphTo(name: string, ...target: Array<typeof Model>): T;
    public orderBy(column: string, order?: SortOrder): T;

    // Declaration order matters otherwise TypeScript gets confused between query() and query(...query: string[])
    public query(): Knex.QueryBuilder;
    public query(callback: (qb: Knex.QueryBuilder) => void): T;
    public query(...query: string[]): T;
    public query(query: { [key: string]: any }): T;

    public refresh(options?: FetchOptions): BlueBird<T>;
    public resetQuery(): T;
    public save(key?: string, val?: any, options?: SaveOptions): BlueBird<T>;
    public save(attrs?: { [key: string]: any }, options?: SaveOptions): BlueBird<T>;
    public through<R extends Model<any>>(interim: typeof Model, throughForeignKey?: string, otherKey?: string): R;
    public where(properties: { [key: string]: any }): T;
    public where(key: string, operatorOrValue: string | number | boolean, valueIfOperator?: string | number | boolean): T;

    // Pagination Plugin
    public fetchPage<T extends Model<any>>(options: any): BlueBird<Collection<T>>;

    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/errors.js
    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/model.js#L1280
    public static NotFoundError: createError.Error<Error>;
    public static NoRowsUpdatedError: createError.Error<Error>;
    public static NoRowsDeletedError: createError.Error<Error>;
  }

  abstract class CollectionBase<T extends Model<any>> extends Events<T> {
    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/base/collection.js#L573
    public length: number;

    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/base/collection.js#L21
    constructor(models?: T[], options?: CollectionOptions<T>);

    public add(models: T[] | Array<{ [key: string]: any }>, options?: CollectionAddOptions): Collection<T>;
    public at(index: number): T;
    public clone(): Collection<T>;
    public fetch(options?: CollectionFetchOptions): BlueBird<Collection<T>>;
    public findWhere(match: { [key: string]: any }): T;
    public get(id: any): T;
    public invokeThen(name: string, ...args: any[]): BlueBird<any>;
    public parse(response: any): any;
    public pluck(attribute: string): any[];
    public pop(): void;
    public push(model: any): Collection<T>;
    public reduceThen<R>(iterator: (prev: R, cur: T, idx: number, array: T[]) => R, initialValue: R, context: any): BlueBird<R>;
    public remove(model: T, options?: EventOptions): T;
    public remove(model: T[], options?: EventOptions): T[];
    public reset(model: any[], options?: CollectionAddOptions): T[];
    public serialize(options?: SerializeOptions): any[];
    public set(models: T[] | Array<{ [key: string]: any }>, options?: CollectionSetOptions): Collection<T>;
    public shift(options?: EventOptions): void;
    public slice(begin?: number, end?: number): void;
    public toJSON(options?: SerializeOptions): any[];
    public unshift(model: any, options?: CollectionAddOptions): void;
    public where(match: { [key: string]: any }, firstOnly: boolean): T | Collection<T>;

    // lodash methods
    public all(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): boolean;
    public all<R extends {}>(predicate?: R): boolean;
    public any(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): boolean;
    public any<R extends {}>(predicate?: R): boolean;
    public chain(): Lodash.LoDashExplicitObjectWrapper<T>;
    public collect(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T[];
    public collect<R extends {}>(predicate?: R): T[];
    public contains(value: any, fromIndex?: number): boolean;
    public countBy(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): Lodash.Dictionary<number>;
    public countBy<R extends {}>(predicate?: R): Lodash.Dictionary<number>;
    public detect(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T;
    public detect<R extends {}>(predicate?: R): T;
    public difference(...values: T[]): T[];
    public drop(n?: number): T[];
    public each(callback?: Lodash.ListIterator<T, void>, thisArg?: any): Lodash.List<T>;
    public each(callback?: Lodash.DictionaryIterator<T, void>, thisArg?: any): Lodash.Dictionary<T>;
    public each(callback?: Lodash.ObjectIterator<T, void>, thisArg?: any): T;
    public every(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): boolean;
    public every<R extends {}>(predicate?: R): boolean;
    public filter(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T[];
    public filter<R extends {}>(predicate?: R): T[];
    public find(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T;
    public find<R extends {}>(predicate?: R): T;
    public first(): T;
    public foldl<R>(callback?: Lodash.MemoIterator<T, R>, accumulator?: R, thisArg?: any): R;
    public foldr<R>(callback?: Lodash.MemoIterator<T, R>, accumulator?: R, thisArg?: any): R;
    public forEach(callback?: Lodash.ListIterator<T, void>, thisArg?: any): Lodash.List<T>;
    public forEach(callback?: Lodash.DictionaryIterator<T, void>, thisArg?: any): Lodash.Dictionary<T>;
    public forEach(callback?: Lodash.ObjectIterator<T, void>, thisArg?: any): T;
    public groupBy(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): Lodash.Dictionary<T[]>;
    public groupBy<R extends {}>(predicate?: R): Lodash.Dictionary<T[]>;
    public head(): T;
    public include(value: any, fromIndex?: number): boolean;
    public indexOf(value: any, fromIndex?: number): number;
    public initial(): T[];
    public inject<R>(callback?: Lodash.MemoIterator<T, R>, accumulator?: R, thisArg?: any): R;
    public invoke(methodName: string | Function, ...args: any[]): any;
    public isEmpty(): boolean;
    public keys(): string[];
    public last(): T;
    public lastIndexOf(value: any, fromIndex?: number): number;

    // See https://github.com/DefinitelyTyped/DefinitelyTyped/blob/1ec3d51/lodash/lodash-3.10.d.ts#L7119
    // See https://github.com/Microsoft/TypeScript/blob/v1.8.10/lib/lib.core.es7.d.ts#L1122
    public map<U>(predicate?: Lodash.ListIterator<T, U> | string, thisArg?: any): U[];
    public map<U>(predicate?: Lodash.DictionaryIterator<T, U> | string, thisArg?: any): U[];
    public map<U>(predicate?: string): U[];

    public max(predicate?: Lodash.ListIterator<T, boolean> | string, thisArg?: any): T;
    public max<R extends {}>(predicate?: R): T;
    public min(predicate?: Lodash.ListIterator<T, boolean> | string, thisArg?: any): T;
    public min<R extends {}>(predicate?: R): T;
    public reduce<R>(callback?: Lodash.MemoIterator<T, R>, accumulator?: R, thisArg?: any): R;
    public reduceRight<R>(callback?: Lodash.MemoIterator<T, R>, accumulator?: R, thisArg?: any): R;
    public reject(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T[];
    public reject<R extends {}>(predicate?: R): T[];
    public rest(): T[];
    public select(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T[];
    public select<R extends {}>(predicate?: R): T[];
    public shuffle(): T[];
    public size(): number;
    public some(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): boolean;
    public some<R extends {}>(predicate?: R): boolean;
    public sortBy(predicate?: Lodash.ListIterator<T, boolean> | Lodash.DictionaryIterator<T, boolean> | string, thisArg?: any): T[];
    public sortBy<R extends {}>(predicate?: R): T[];
    public tail(): T[];
    public take(n?: number): T[];
    public toArray(): T[];
    public without(...values: any[]): T[];
  }

  class Collection<T extends Model<any>> extends CollectionBase<T> {
    /** @deprecated use Typescript classes */
    public static extend<T>(prototypeProperties?: any, classProperties?: any): Function;
    /** @deprecated should use `new` objects instead. */
    public static forge<T>(attributes?: any, options?: ModelOptions): T;

    public attach(ids: any | any[], options?: SyncOptions): BlueBird<Collection<T>>;
    public count(column?: string, options?: SyncOptions): BlueBird<number>;
    public create(model: { [key: string]: any }, options?: CollectionCreateOptions): BlueBird<T>;
    public detach(ids: any[], options?: SyncOptions): BlueBird<any>;
    public detach(options?: SyncOptions): BlueBird<any>;
    public fetchOne(options?: CollectionFetchOneOptions): BlueBird<T>;
    public load(relations: string | string[], options?: SyncOptions): BlueBird<Collection<T>>;
    public orderBy(column: string, order?: SortOrder): T;

    // Declaration order matters otherwise TypeScript gets confused between query() and query(...query: string[])
    public query(): Knex.QueryBuilder;
    public query(callback: (qb: Knex.QueryBuilder) => void): Collection<T>;
    public query(...query: string[]): Collection<T>;
    public query(query: { [key: string]: any }): Collection<T>;

    public resetQuery(): Collection<T>;
    public through<R extends Model<any>>(interim: typeof Model, throughForeignKey?: string, otherKey?: string): Collection<R>;
    public updatePivot(attributes: any, options?: PivotOptions): BlueBird<number>;
    public withPivot(columns: string[]): Collection<T>;

    // Pagination plugin
    public fetchPage<T extends Model<any>>(options: any): BlueBird<Collection<T>>;
    public pagination: any;

    // See https://github.com/tgriesser/bookshelf/blob/0.9.4/src/collection.js#L389
    public static EmptyError: createError.Error<Error>;
  }

  interface ModelOptions {
    tableName?: string;
    hasTimestamps?: boolean;
    parse?: boolean;
  }

  interface LoadOptions extends SyncOptions {
    withRelated: Array<string | WithRelatedQuery>;
  }

  interface FetchOptions extends SyncOptions {
    require?: boolean;
    columns?: string | string[];
    withRelated?: Array<string | WithRelatedQuery>;
  }

  interface WithRelatedQuery {
    [index: string]: (query: Knex.QueryBuilder) => Knex.QueryBuilder;
  }

  interface FetchAllOptions extends FetchOptions {
  }

  interface SaveOptions extends SyncOptions {
    method?: string;
    defaults?: string;
    patch?: boolean;
    require?: boolean;
  }

  interface DestroyOptions extends SyncOptions {
    require?: boolean;
  }

  interface SerializeOptions {
    shallow?: boolean;
    omitPivot?: boolean;
  }

  interface SetOptions {
    unset?: boolean;
  }

  interface TimestampOptions {
    method?: string;
  }

  interface SyncOptions {
    transacting?: Knex.Transaction;
    debug?: boolean;
  }

  interface CollectionOptions<T> {
    comparator?: boolean | string | ((a: T, b: T) => number);
  }

  interface CollectionAddOptions extends EventOptions {
    at?: number;
    merge?: boolean;
  }

  interface CollectionFetchOptions {
    require?: boolean;
    withRelated?: string | string[];
  }

  interface CollectionFetchOneOptions {
    require?: boolean;
    columns?: string | string[];
  }

  interface CollectionSetOptions extends EventOptions {
    add?: boolean;
    remove?: boolean;
    merge?: boolean;
  }

  interface PivotOptions {
    query?: Function | any;
    require?: boolean;
  }

  interface EventOptions {
    silent?: boolean;
  }

  type EventFunction<T> = (model: T, attrs: any, options: any) => Promise<any> | void;

  interface CollectionCreateOptions extends ModelOptions, SyncOptions, CollectionAddOptions, SaveOptions { }
}

export = Bookshelf;

