declare module 'resources' {

    interface Usermanagement {
        firstName: string;
        lastName: string;
        address: string;
        postCode: string;
        phone: string;
        email: string;
        username: string;
        password: string;
    }

}
