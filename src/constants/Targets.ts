/**
 * constants.Targets
 * ------------------------------------------------
 *
 * This is for our IOC so have a unique key/target for
 * our controllers, services and repositories
 *
 * This file is generated with the task `$ npm run console update:targets`.
 */

export const Targets = {
    Model:     {
        Auth: 'Auth',
        Usermanagement: 'Usermanagement',
    },
    Repository:     {
        AuthRepository: 'AuthRepository',
        UsermanagementRepository: 'UsermanagementRepository',
    },
    Service:     {
        AuthService: 'AuthService',
        UsermanagementService: 'UsermanagementService',
    },
    Middleware:     {
        AuthenticateMiddleware: 'AuthenticateMiddleware',
        ValidateintegrationMiddleware: 'ValidateintegrationMiddleware',
    },
    Listener:     {
        ForbiddenactionListener: 'ForbiddenactionListener',
        user: {
            UserAuthenticatedListener: 'UserAuthenticatedListener',
            UserCreatedListener: 'UserCreatedListener',
        },
    },
    Controller:     {
        AuthenticateController: 'AuthenticateController',
        UsermanagementController: 'UsermanagementController',
    },
};
