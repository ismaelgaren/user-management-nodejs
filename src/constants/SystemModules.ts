/**
 * constants.SystemModules
 * ------------------------------------------------
 *
 * This variable has all the controller route names, with
 * its corresponsding module alias on system_modules table
 * these constants will help to determine if the current
 * user requesting on API is valid to POST, PUT, GET, and
 * DELETE on that module
 */

export const SystemModules = {
    auditlogs: 'BLLR_AUDIT_LOGS', // settings
    lookups: 'BLLR_LOOKUPS', // settings
    rolesauthorization: 'BLLR_ROLES_AUTHORIZATION', // settings
    countries: 'BLLR_COUNTRIES', // settings
    roles: 'BLLR_ROLES_AUTHORIZATION', // settings
    users: 'BLLR_USERS', // settings
    merchants: 'BLLR_BR_MERCHANT', // settings
    responsecodes: 'POS_RESPONSE_CODE', // settings
    // policytypes: 'BLLR_INBOUND_PARAMS', // settings
    // policytypes: 'BLLR_FRAUD_DASHBOARD', // transactions
    // policytypes: 'BLLR_FRAUD_ALL', // transactions
    // policytypes: 'BLLR_FRAUD_ALL', // transactions
    // policytypes: 'BLLR_FRAUD_DETECTED', // transactions
    // policytypes: 'BLLR_FRAUD_DISPUTES', // transactions
    // policytypes: 'BLLR_POLICIES', // policies
    // policytypes: 'BLLR_BUSINESS_RULES', // policies
};

