import { inject, named } from 'inversify';
import { controller, httpGet, httpPost, httpPut, requestBody, requestHeaders, requestParam, response } from 'inversify-express-utils';
import { app } from '../../app';
import { Targets, Types } from '../../constants';
import { UsermanagementService } from '../services/UsermanagementService';

// Get middlewares
const authenticate = app.IoC.getNamed<interfaces.Middleware>(Types.Middleware, Targets.Middleware.AuthenticateMiddleware);
const validateIntegration = app.IoC.getNamed<interfaces.Middleware>(Types.Middleware, Targets.Middleware.ValidateintegrationMiddleware);

@controller('/usermanagements')
export class UsermanagementController {

    constructor( @inject(Types.Service) @named(Targets.Service.UsermanagementService) private usermanagementService: UsermanagementService) { }

    @httpGet('/', authenticate.use)
    public async findAll(
        @response() res: myExpress.Response,
        @requestHeaders() headers: {
            range: string;
        },
    ): Promise < any > {
        const range = headers.range ? headers.range.split('-') : [];
        const [page, pageSize] = range;
        const lookups = await this.usermanagementService.findAll({
            page,
            pageSize,
        });
        return res.found({
            pagination: lookups.pagination,
            result: lookups.toJSON(),
        });
    }

    @httpPost('/add_user', validateIntegration.use)
    public async addUser( @response() res: myExpress.Response, @requestBody() body: any): Promise<any> {
        const addNewUser = await this.usermanagementService.addUser(body);
        return addNewUser;
    }

    @httpPut('/edit_user/:id', authenticate.use)
    public async editUser(@response() res: myExpress.Response, @requestParam('id') id: string, @requestBody() body: any): Promise<any> {
        const updateUser = await this.usermanagementService.editUser(id, body);
        return updateUser;
    }

    @httpPut('/delete_user/:id', authenticate.use)
    public async deleteUser(@response() res: myExpress.Response, @requestParam('id') id: string, @requestBody() body: any): Promise<any> {
        const deleteUsers = await this.usermanagementService.deleteUser(id, body);
        return deleteUsers;
    }
}
