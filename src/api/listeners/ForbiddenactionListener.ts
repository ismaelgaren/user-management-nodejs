import { inject, named } from 'inversify';
import { Core, Types } from '../../constants';
import { Logger as LoggerType } from '../../core/Logger';


export class ForbiddenactionListener implements interfaces.Listener {

    public static Event = Symbol('ForbiddenactionListenerListener');

    public log: LoggerType;

    constructor(
        @inject(Types.Core) @named(Core.Logger) Logger: typeof LoggerType,
    ) {
        this.log = new Logger(__filename);
    }

    public act(log: any): void {
        this.log.info('Receive event ForbiddenactionListener', log);
    }

}
