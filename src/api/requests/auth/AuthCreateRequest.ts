import { IsNotEmpty } from 'class-validator';
import { RequestBody } from '../../../core/api/RequestBody';

/**
 * This class is used for create request. Create a new instance
 * with the json body and than call .validate() to check if
 * all criteria are given
 *
 * @export
 * @class UserCreateRequest
 * @extends {RequestBody}
 */
export class AuthCreateRequest extends RequestBody {

    @IsNotEmpty({message: 'Account Name is required'})
    public accountName: string;

    @IsNotEmpty({message: 'Username is required'})
    public username: string;

    public picture: string;

    @IsNotEmpty({message: 'Password is required'})
    public password: string;

    @IsNotEmpty({message: 'Status is required'})
    public statusId: string;
    @IsNotEmpty({message: 'Role is required'})
    public roleId: string;
    @IsNotEmpty({message: 'Documentation Access is required'})
    public docsAccessId: string;
}

