import { IsNotEmpty } from 'class-validator';
import { RequestBody } from '../../../core/api/RequestBody';

/**
 * This class is used for update request. Create a new instance
 * with the json body and than call .validate() to check if
 * all criteria are given
 *
 * @export
 * @class UserUpdateRequest
 * @extends {RequestBody}
 */
export class AuthUpdateRequest extends RequestBody {

    @IsNotEmpty({message: 'Account Name is required'})
    public accountName: string;

    @IsNotEmpty({message: 'Username is required'})
    public username: string;

    public picture: string;

    public password: string;

    @IsNotEmpty({message: 'Status is required'})
    public statusId: string;
    @IsNotEmpty({message: 'Role is required'})
    public roleId: string;
    @IsNotEmpty({message: 'Documentation Access is required'})
    public docsAccessId: string;
    /**
     * We override the validate method so we can skip the missing
     * properties.
     */
    public validate(): Promise<void> {
        return super.validate(true);
    }

}

