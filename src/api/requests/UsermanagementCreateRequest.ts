import { IsNotEmpty } from 'class-validator';
import { RequestBody } from '../../core/api/RequestBody';


export class UsermanagementCreateRequest extends RequestBody {

    @IsNotEmpty()
    public firstName: string;

    @IsNotEmpty()
    public lastName: string;

    @IsNotEmpty()
    public address: string;

    @IsNotEmpty()
    public postCode: string;

    @IsNotEmpty()
    public phone: string;

    @IsNotEmpty()
    public email: string;

    @IsNotEmpty()
    public username: string;

    @IsNotEmpty()
    public password: string;

}

