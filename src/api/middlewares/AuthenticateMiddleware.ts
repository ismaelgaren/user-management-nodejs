// import http from 'http';
import {
  inject,
  named,
} from 'inversify';
import {
  Core,
  Types,
} from '../../constants';
import {
  Logger as LoggerType,
} from '../../core/Logger';
import { Usermanagement } from '../models/Usermanagement';

export class AuthenticateMiddleware implements interfaces.Middleware {

  public log: LoggerType;

  constructor(
    @inject(Types.Core) @named(Core.Logger) Logger: typeof LoggerType,
  ) {
    this.log = new Logger(__filename);
  }

  public use = async (req: myExpress.Request, res: myExpress.Response, next: myExpress.NextFunction): Promise<void> => {
    const authorization = req.headers.authorization;
    const parts = `${authorization}`.split(' ');
    const credentials = new Buffer(parts[1], 'base64').toString();
    const index = credentials.indexOf(':');
    const username = credentials.slice(0, index);
    const password = credentials.slice(index + 1);
    console.log('AUTH', username, password);
    const checkUserExist = await Usermanagement.fetchByUsername(username);
    if (checkUserExist) {
      return next();
    } else {
      const errResponse = {
        success: false,
        message: 'Invalid auth',
      };
      res.status(401).send(errResponse);
    }
  }
}
