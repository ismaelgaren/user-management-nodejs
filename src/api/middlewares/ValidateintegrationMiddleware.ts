import { inject, named } from 'inversify';
import * as moment from 'moment-timezone';
import { Core, Types } from '../../constants';
import { Logger as LoggerType } from '../../core/Logger';
import { Usermanagement } from '../models/Usermanagement';

interface ResponseParameter {
    response_code?: string;
    response_message?: string;
    response_advise?: string;
    response_id: string;
    request_id: string;
    timestamp: string;
}

export class ValidateintegrationMiddleware implements interfaces.Middleware {

    public log: LoggerType;

    constructor(
        @inject(Types.Core) @named(Core.Logger) Logger: typeof LoggerType,
    ) {
        this.log = new Logger(__filename);
    }

    public use = async (req: myExpress.Request, res: myExpress.Response, next: myExpress.NextFunction): Promise<void> => {
        console.log('CHECK REQ', req.url);
        console.log('CHECK BODY', req.body);
        if (req.url === '/usermanagements/add_user') {
            const checkUsername = await Usermanagement.fetchByUsername(req.body.username);
            if (checkUsername) {
                const error = 'User already exist';
                return this.sendError(error, res);
            }
        }
        next();
    }

    private sendError(error: any, res: myExpress.Response): any {
        const finalResp = {
            success: false,
            data: error,
        };
        res.status(400).send(finalResp);
    }

}
