import * as Bookshelf from 'bookshelf';
import { inject, named } from 'inversify';
import { Core, Targets, Types } from '../../constants';
import { Request, Validate } from '../../core/api/Validate';
import { Logger as LoggerType } from '../../core/Logger';
import { NotFoundException } from '../exceptions/NotFoundException';
import { Usermanagement } from '../models/Usermanagement';
import { UsermanagementRepository } from '../repositories/UsermanagementRepository';
import { UsermanagementCreateRequest } from '../requests/UsermanagementCreateRequest';
import { UsermanagementUpdateRequest } from '../requests/UsermanagementUpdateRequest';


export class UsermanagementService {

    public log: LoggerType;

    constructor(
        @inject(Types.Repository) @named(Targets.Repository.UsermanagementRepository) public usermanagementRepo: UsermanagementRepository,
        @inject(Types.Core) @named(Core.Logger) public Logger: typeof LoggerType,
    ) {
        this.log = new Logger(__filename);
    }

    public async findAll(options: any): Promise<Bookshelf.Collection<Usermanagement>> {
        return this.usermanagementRepo.findAll(options);
    }

    public async addUser(body: any): Promise<Usermanagement> {
        // If the request body was valid we will create the usermanagement
        const usermanagement = await this.usermanagementRepo.addUser(body);
        return usermanagement;
    }

    public async editUser(id: any, body: any): Promise<any> {
        // If the request body was valid we will create the usermanagement
        const usermanagement = await this.usermanagementRepo.editUser(id, body);
        return usermanagement;
    }

    public async deleteUser(id: any, body: any): Promise<any> {
        // If the request body was valid we will create the usermanagement
        const usermanagement = await this.usermanagementRepo.deleteUser(id, body);
        return usermanagement;
    }
}
