import * as  crypto from 'crypto';
import * as AES from 'crypto-js/aes';
import * as Base64 from 'crypto-js/enc-base64';
import * as encUtf8 from 'crypto-js/enc-utf8';
import * as HmacSHA512 from 'crypto-js/hmac-sha512';
import * as _ from 'lodash';
import * as lz from 'lz-string';
import * as PowerRadix from 'power-radix';
import * as validator from 'validator';
// import * as  util from 'util';

export const encrypt = (password): string => {
  const hashed = Base64.stringify(HmacSHA512(password, process.env.enki));
  // console.log(hashed);
  return hashed;
};

export const verify = (hashed, password): boolean => {
  // console.log(hashed, password, encrypt(password));
  // return scrypt.verifyKdfSync(Buffer.from(hashed, 'hex'), password);
  return hashed === encrypt(password);
};

// Encrypt
export const enc = (str: string): string => AES.encrypt(str, process.env.enki).toString();

// Sign
export const getsign = (obj: any, key: string): string => {
  return getsignature(obj, key);
};

// Sign Sha1
export const getsha1 = (paramString: string): string => {
  try {
      const specimen = paramString;
      const hash = crypto.createHash('sha1').update(specimen, 'utf8').digest('hex');
      return hash;
  } catch (err) {
    console.log(err);
    return '';
  }

};

// Decrypt
export const dec = (str: string): string => AES.decrypt(str.toString(), process.env.enki).toString(encUtf8);

// Compress
export const zip = (str: string): string => lz.compressToUTF16(str);
export const zipb64 = (str: string): string => lz.compressToBase64(str);
// Decompress
export const unzip = (str: string): string => lz.decompressFromUTF16(Buffer.from(str, 'utf-16').toString());
export const unzipb64 = (str: string): string => lz.decompressFromBase64(str);

const chars = ('PYFGCRLAOEUIDHTNSQJKXBMWVZpyfgcrlaoeuidhtnsqjkxbmwvz1234567890'.split(''));

export function encode(id: any): string | null {
  if (id == null) {
    return null;
  }

  if (!validator.isUUID(id)) {
    return id;
    // throw new Error(`redundant encoding for '${id}'`)
  }

  const uid = id.replace(/-/g, '').toUpperCase();
  const pr = new PowerRadix(uid, 16);
  const text = pr.toString(chars);
  return text;
}

export function decode(text ?: any): string | null {
  if (!text) {
    return null;
  }

  if (validator.isUUID(text)) {
    return text;
  }

  // Weird edge case
  if (text === 'me') {
    return text;
  }

  try {
    // Decode the shortened ID to a UUID
    const pr = new PowerRadix(text, chars);
    // console.log(pr, bytesToUuid(pr));
    const uid = _.padStart(pr.toString(16), 32, '0');
    // console.log(uid);
    const uuidTxt = unparse(parse(uid));
    return encode(uuidTxt) !== text ? text : uuidTxt;
  } catch (err) {
    // console.log(err);
    // Failed to parse the shortened UUID
    return null;
  }
}

function getsignature(paramString: string, key: string): string {
  try {
      const specimen = paramString + key;
      const hash = crypto.createHash('sha512').update(specimen, 'utf8').digest('hex');
      return hash;
  } catch (err) {
    console.log(err);
    return '';
  }

}

const byteToHex: string[] = [];
const hexToByte = {};
for (let i = 0; i < 256; i++) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
  hexToByte[byteToHex[i]] = i;
}

function parse(s: any, buf ?: any, offset ?: any): string {
  const i = (buf && offset) || 0;
  let ii = 0;

  buf = buf || [];
  s.toLowerCase().replace(/[0-9a-f]{2}/g, (oct): any => {
    if (ii < 16) { // Don't overflow!
      buf[i + ii++] = hexToByte[oct];
    }
  });

  // Zero out remaining bytes if string was short
  while (ii < 16) {
    buf[i + ii++] = 0;
  }

  return buf;
}

// **`unparse()` - Convert UUID byte array (ala parse()) into a string**
function unparse(buf: any, offset ?: any): string {
  let i = offset || 0;
  const bth = byteToHex;
  return `${bth[buf[i++]]}${bth[buf[i++]]}${bth[buf[i++]] + bth[buf[i++]]}-${bth[buf[i++]]}${bth[buf[i++]]}-${bth[buf[i++]]}${bth[buf[i++]]}-${bth[buf[i++]]}${bth[buf[i++]]}-${bth[buf[i++]]}${bth[buf[i++]]}${bth[buf[i++]]}${bth[buf[i++]]}${bth[buf[i++]]}${bth[buf[i++]]}`;
}
