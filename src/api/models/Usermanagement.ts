import { Bookshelf } from '../../config/Database';
import { decode, encode } from '../helpers/encryption';

export class Usermanagement extends Bookshelf.Model<Usermanagement> {

    public static forgeNew(attributes: any): Usermanagement {
        return this.forge<Usermanagement>({
            ...attributes,
            // status: decode(attributes.status),
        });
    }

    public static forgeById(attributes: any): Usermanagement {
        console.log('AttR', attributes);
        return this.forge<Usermanagement>({
            id: decode(attributes.id),
        });
    }

    public static forgeByIdForDel(id: string): Usermanagement {
        return this.forge<Usermanagement>({
            id: decode(id),
        });
    }

    public static async fetchById(value: string): Promise<Usermanagement> {
        return await Usermanagement.where<Usermanagement>({
            id: decode(value),
        }).fetch({
            // withRelated: ['statusInfo'],
        });
    }

    public static async find(options: any): Promise<any> {
        options = Object.assign({}, options, {
            // withRelated: [ 'fkNameHere' ],
        });
        const clsModel = new Usermanagement();
        // const status = decode(statusId);
        return clsModel.query((qb) => {
            // qb.whereNotIn('status', [`${status}`]);
            qb.where('status', 'ACTIVE');

        }).fetchPage<Usermanagement>(options);
    }

    public static async fetchByUsername(value: string): Promise<any> {
        const result = await Usermanagement.where<Usermanagement>({
            username: value,
            status: 'ACTIVE',
        }).fetch();
        if (result) {
            return true;
        } else {
            return false;
        }
    }

    public get tableName(): string { return 'usermgmt'; }
    public get hasTimestamps(): boolean { return true; }

    public get Id(): string { return this.get('id'); }
    public set Id(value: string) { this.set('id', value); }

    public get FirstName(): string { return this.get('first_name'); }
    public set FirstName(value: string) { this.set('first_name', value); }

    public get LastName(): string { return this.get('last_name'); }
    public set LastName(value: string) { this.set('last_name', value); }

    public get Address(): string { return this.get('address'); }
    public set Address(value: string) { this.set('address', value); }

    public get PostCode(): string { return this.get('post_code'); }
    public set PostCode(value: string) { this.set('post_code', value); }

    public get Phone(): string { return this.get('phone'); }
    public set Phone(value: string) { this.set('phone', value); }

    public get Email(): string { return this.get('email'); }
    public set Email(value: string) { this.set('email', value); }

    public get Username(): string { return this.get('username'); }
    public set Username(value: string) { this.set('username', value); }

    public get Password(): string { return this.get('password'); }
    public set Password(value: string) { this.set('password', value); }


    public hidden = ['id'];

    // FOREIGN KEY TEMPLATE
    // public fkName(): fkModelHere {
    //    return this.belongsTo(fkModelHere, 'external field name here', 'internal field name here');
    //  }

    public parse(attrs: any): any {
        attrs = super.parse(attrs);
        attrs.Id = encode(attrs.id);
        return attrs;
    }

    public format(attrs: any): any {
        if (attrs.Id) {
            attrs.id = decode(attrs.Id);
        }
        return super.format(attrs);
    }

}
