import * as Bookshelf from 'bookshelf';
import { inject, named } from 'inversify';
import { Targets, Types } from '../../constants';
import { DatabaseException } from '../exceptions/DatabaseException';
import { decode, encrypt } from '../helpers/encryption';
import { Usermanagement } from '../models/Usermanagement';


export class UsermanagementRepository {

    constructor(
        @inject(Types.Model) @named(Targets.Model.Usermanagement) public UsermanagementModel: typeof Usermanagement,
    ) { }

    public async findAll(options: any): Promise<Bookshelf.Collection<Usermanagement>> {
        console.log('[FETCH-USER]: Start fetching user');
        const list = await this.UsermanagementModel.find(options);
        console.log('[FETCH-USER]: Done fetching user');
        return list as Bookshelf.Collection<Usermanagement>;
    }

    public async addUser(data: any): Promise<any> {
        try {
            console.log('[ADD-USER]: Start adding user', data);
            const newBody = {
                firstName: data.firstName,
                lastName: data.lastName,
                address: data.address,
                postCode: data.postCode,
                phone: data.phone,
                email: data.email,
                username: data.username,
                password: encrypt(data.password),
                status: 'ACTIVE',
            };
            console.log('[ADD-USER]: Composed body', newBody);
            const newUser = this.UsermanagementModel.forgeNew(newBody);
            newUser.save();
            console.log('[ADD-USER]: Done adding user');
            return {
                success: true,
                message: 'Successfully added user',
            };
        } catch (e) {
            console.log(`[ADD-USER]: Error on ${e}`);
            return {
                success: false,
                message: 'Failed to add user',
            };
        }
    }

    public async editUser(id: string, data: any): Promise<any> {
        console.log('[EDIT-USER]: Start editing user', data);
        const usermanagement = this.UsermanagementModel.forgeById({ id });
        try {
            const newData = {
                ...data,
                id: decode(id),
            };
            console.log(newData);
            const usermanagementUpdated = await usermanagement.save(newData, { patch: true });
            const resp = (await this.UsermanagementModel.fetchById(usermanagementUpdated.id)).toJSON();
            console.log('[EDIT-USER]: Done editing user', resp);
            return {
                success: true,
                message: 'Successfully updated user',
                data: resp,
            };
        } catch (error) {
            console.log('[EDIT-USER]: Error on editing user', error);
            throw new DatabaseException('Could not update the usermanagement!', error);
        }
    }

    public async deleteUser(id: string, data: any): Promise<any> {
        console.log('[DELETE-USER]: Start deleting user', data);
        data.status = 'INACTIVE';
        data.id = id;
        console.log('CHECK DATA', data);
        try {
            const newData = {
                status: data.status,
                id: decode(data.id),
            };
            const userDelete = Usermanagement.forgeById(data);
            const usermanagementUpdated = await userDelete.save(newData, { patch: true });
            console.log('[DELETE-USER]: Done deleting user', data);
            return {
                success: true,
                message: 'Successfully deleted user',
            };
        } catch (error) {
            console.log('[DELETE-USER]: Error on deleting user', error);
            throw new DatabaseException('Could not delete the merchantwithdrawalaccount!', error);
        }
    }
}
