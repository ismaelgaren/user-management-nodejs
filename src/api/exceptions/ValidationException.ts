/**
 * ValidationException
 * ----------------------------------------
 *
 * This should be used when we validate
 * the request payload, so we can response with a 400 (Bad Request)
 */

import {
  Exception,
} from '../../core/api/Exception';


export class ValidationException extends Exception {
  constructor(text: string, errors: any) {
    // console.log('ERRORS: ', JSON.stringify(errors));
    let info = '';
    try {
      info = errors.map((e) => ({
        property: e.property,
        constraints: e.children && e.children.length ? e.children.map(c => ({
          property: c.property,
          constraints: c.constraints,
        })) : e.constraints,
      }));
    } catch (err) {
      info = err;
    }
    super(400, text, info);
  }
}
