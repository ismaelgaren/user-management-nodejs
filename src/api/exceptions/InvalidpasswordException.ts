import { Exception } from '../../core/api/Exception';


export class InvalidpasswordException extends Exception {
    constructor() {
        // super(400, text, error);
        super(401, 'InvalidPassword', 'Invalid Password');
    }
}
