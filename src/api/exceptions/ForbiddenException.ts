
import {
    Exception,
  } from '../../core/api/Exception';

  export class ForbiddenException extends Exception {

    constructor(
      action ?: string,
      module ?: string,
    ) {
      super(403, 'Forbidden', `Method ${action} in ${module} is forbidden`);
    }
  }
