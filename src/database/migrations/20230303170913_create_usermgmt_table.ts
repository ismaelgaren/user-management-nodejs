import * as Knex from 'knex';


exports.up = (db: Knex): Promise<any> => {
    return Promise.all([
        db.schema.createTable('usermgmt', (table: Knex.CreateTableBuilder) => {
            table.uuid('id').primary().defaultTo(db.raw('uuid_generate_v4()'));

            table.string('first_name').notNullable();
            table.string('last_name').notNullable();
            table.string('address').notNullable();
            table.string('post_code').notNullable();
            table.string('phone').notNullable();
            table.string('email').notNullable();
            table.string('username').notNullable();
            table.string('password').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(db.fn.now());
            table.timestamp('updated_at');
            table.timestamp('deleted_at');

            table.uuid('last_updated_by').nullable();
            table.uuid('created_by').nullable();
            table.uuid('deleted_by').nullable();
        }),
    ]);
};

exports.down = (db: Knex): Promise<any> => {
    return Promise.all([
        db.schema.dropTable('usermgmt'),
    ]);
};
