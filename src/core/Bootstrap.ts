// import * as http from 'http';
import * as cluster from 'cluster';
import * as express from 'express';
import {
  InversifyExpressServer,
} from 'inversify-express-utils';
import * as WebSocket from 'ws';
// import { Mercury } from '../api/models/Mercury';
import {
  exceptionHandler,
} from './api/exceptionHandler';
import {
  extendExpressResponse,
} from './api/extendExpressResponse';
import {
  ApiInfo,
} from './ApiInfo';
import {
  ApiMonitor,
} from './ApiMonitor';
import {
  Environment,
} from './helpers/Environment';
import {
  IoC,
} from './IoC';
import {
  Logger,
} from './Logger';
import {
  Server,
} from './Server';
import {
  SwaggerUI,
} from './SwaggerUI';

export class Bootstrap {

  public log: Logger = new Logger(__filename);

  public defineExpressApp(app: express.Application): express.Application {
    app.set('host', process.env.APP_HOST);
    app.set('port', Server.normalizePort(parseInt(process.env.PORT || '', 10) || parseInt(process.env.APP_PORT || '', 10) || 3000));
    return app;
  }

  public setupMonitor(app: express.Application): void {
    const apiMonitor = new ApiMonitor();
    apiMonitor.setup(app);
  }

  public setupCoreTools(app: express.Application): void {
    const apiInfo = new ApiInfo();
    apiInfo.setup(app);

    const swaggerUI = new SwaggerUI();
    swaggerUI.setup(app);
  }

  public startServer(app: express.Application): any {
    console.log('CLUSTER.ISMASTER', cluster.isMaster);
    let numWorkers = require('os').cpus().length;
    try {
      if (cluster.isMaster) {
          // tslint:disable-next-line:prefer-template
          console.log('Master cluster setting up ' + numWorkers + ' workers...');

          if (process.env.NODE_ENV === 'production') {
            numWorkers = 3;
            for (let i = 0; i < numWorkers; i++) {
              cluster.fork();
            }
          } else {
            cluster.fork();
          }

          process.on('message', (msg) => {
            console.log('CLUSTER MSG', msg);
          });

          const clusterGlobal = cluster;
          cluster.on('online', () => {
              this.log.debug(``);
              this.log.debug(`Aloha, your app is ready on ${app.get('host')}:${app.get('port')}${process.env.APP_URL_PREFIX}`);
              this.log.debug(`To shut it down, press <CTRL> + C at any time.`);
              this.log.debug(``);
              this.log.debug('-------------------------------------------------------');
              this.log.debug(`Environment       : ${Environment.getNodeEnv()}`);
              this.log.debug(`Version           : ${Environment.getPkg().version}`);
              this.log.debug(`CPU Core          : ${numWorkers}`);
              let workerCount = '';
              // tslint:disable-next-line:forin
              for (const id in clusterGlobal.workers) {
                workerCount = id;
              }
              this.log.debug(`Parallel Workers  : ${workerCount}`);
              const thisGlobal = this;
              // tslint:disable-next-line:forin
              for (const id in clusterGlobal.workers) {
                thisGlobal.log.debug(`  Worker ID       : ${clusterGlobal.workers[id]!.process.pid}`);
                if (clusterGlobal.workers[id] !== undefined) {
                  clusterGlobal.workers[id]!.on('message', (msg) => {
                    console.log('CLUSTER MSG', msg);
                    // if (msg.cmd && msg.cmd === 'notifyRequest') {
                    //   numReqs += 1;
                    // }
                  });
                }
              }
              this.log.debug(``);
              if (Environment.isTruthy(process.env.API_INFO_ENABLED || 'false')) {
                this.log.debug(`API Info     : ${app.get('host')}:${app.get('port')}${ApiInfo.getRoute()}`);
              }
              if (Environment.isTruthy(process.env.SWAGGER_ENABLED || 'false')) {
                this.log.debug(`Swagger      : ${app.get('host')}:${app.get('port')}${SwaggerUI.getRoute()}`);
              }
              if (Environment.isTruthy(process.env.MONITOR_ENABLED || 'false')) {
                this.log.debug(`Monitor      : ${app.get('host')}:${app.get('port')}${ApiMonitor.getRoute()}`);
              }
              this.log.debug('-------------------------------------------------------');
              this.log.debug('');


              // Server.mqttConnect();
              // console.log('Worker ' + worker.process.pid + ' is online');
          });

          cluster.on('exit', (worker, code, signal) => {
            try {
              console.log('Worker ', worker);
              // tslint:disable-next-line:prefer-template
              console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
              console.log('Starting a new worker');
              cluster.fork();
            } catch (err) {
              // err
            }
          });

          cluster.on('message', (msg) => {
              console.log('CLUSTER MSG', msg);
          });


          // Server.startSchedules();
          // Server.queueing();
          // Server.startWSServer();


          const wss = new WebSocket.Server({ port: 8080 });
          wss.on('connection', ws => {
            console.log('WEB SOCKET');
            ws.on('message', message => {
              console.log(`Received message => ${message}`);
            });
            ws.send('ho!');
          });
          wss.on('error', error => {
            console.log('ERROR', error);
          });
          // let numReqs = 0;

          // // tslint:disable-next-line:forin
          // for (const id in cluster.workers) {
          //   // console.log('CLUSTER WORKER', cluster.workers[id]);
          //   if (cluster.workers[id] !== undefined) {
          //     cluster.workers[id]!.on('message', (msg) => {
          //       console.log('CLUSTER MSG', msg);
          //       if (msg.cmd && msg.cmd === 'notifyRequest') {
          //         numReqs += 1;
          //       }
          //     });
          //   }
          // }

      } else {
        return app.listen(app.get('port'));
      }
    } catch (err) {
      // err
    }
  }

  public setupInversifyExpressServer(app: express.Application, ioc: IoC): InversifyExpressServer {
    const inversifyExpressServer = new InversifyExpressServer(ioc.container, undefined, {
      rootPath: process.env.APP_URL_PREFIX || '',
    }, app);
    inversifyExpressServer.setConfig((a) => {
      a.use(extendExpressResponse);
    });
    inversifyExpressServer.setErrorConfig((a) => a.use(exceptionHandler));
    return inversifyExpressServer;
  }

  public bindInversifyExpressServer(app: express.Application, inversifyExpressServer: InversifyExpressServer): express.Application {
    try {
      app = inversifyExpressServer.build();
    } catch (e) {
      this.log.error(e.message);
      process.exit(1);
    }
    return app;
  }

}
