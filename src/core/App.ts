// import * as SocketIO from 'socket.io';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as http from 'http';
import {
  Container,
} from 'inversify';
import {
  InversifyExpressServer,
} from 'inversify-express-utils';
import * as WebSocket from 'ws';
import {
  AppConfig,
} from '../config/AppConfig';
import {
  LoggerConfig,
} from '../config/LoggerConfig';
import {
  Bootstrap,
} from './Bootstrap';
import {
  IoC,
} from './IoC';
import {
  Logger,
} from './Logger';
import {
  Server,
} from './Server';

const app = express();

export interface Configurable {
  configure(app: App): void;
}

export class App {

  private express: express.Application = express();
  private server: Server;
  // private socketIO: SocketIO.Server;
  private inversifyExpressServer: InversifyExpressServer;
  private ioc: IoC = new IoC();
  private log: Logger = new Logger(__filename);
  private bootstrapApp = new Bootstrap();
  private configurations: Configurable[] = [];

  constructor() {
    // It also loads the .env file into the 'process.env' variable.
    dotenv.config();
    // Configure the logger, because we need it already.
    const loggerConfig = new LoggerConfig();
    loggerConfig.configure();
    // Create express app
    this.log.info('Defining app...');
    this.bootstrapApp.defineExpressApp(this.express);
  }

  get IoC(): Container {
    return this.ioc.container;
  }

  get Express(): express.Application {
    return this.express;
  }

  get Server(): Server {
    return this.server;
  }

  public Logger(scope: string): Logger {
    return new Logger(scope || __filename);
  }

  public configure(configurations: Configurable): void {
    this.configurations.push(configurations);
  }

  public async bootstrap(): Promise < void > {
    this.log.info('Configuring app...');
    // Add express monitor app
    this.bootstrapApp.setupMonitor(this.express);
    // Configure the app config for all the middlewares
    const appConfig = new AppConfig();
    appConfig.configure(this);
    // Configure all custom configurations
    this.configurations.forEach((c) => c.configure(this));
    // Setup the ioc of inversify
    this.log.info('Binding IoC modules...');
    await this.ioc.bindModules();
    this.log.info('Setting up IoC...');
    this.inversifyExpressServer = this.bootstrapApp.setupInversifyExpressServer(this.express, this.ioc);
    this.express = this.bootstrapApp.bindInversifyExpressServer(this.express, this.inversifyExpressServer);
    this.bootstrapApp.setupCoreTools(this.express);
    this.log.info('Starting app...');
    this.server = new Server(this.bootstrapApp.startServer(this.express));
    this.server.use(this.express);
    // this.socketIO = SocketIO(3030);
    // this.socketIO.of('/main').on('connection', (client) => {
    //   this.log.info('Client connection opened');
    //   // Success!  Now listen to messages to be received
    //   client.on('message', (event) => this.log.info('Received message from client!', event));
    //   client.on('disconnect', (event) => this.log.info('Client has disconnected'));
    //   client.on('action', (action) => {
    //     if (action.type === 'server/hello') {
    //       this.log.info('Got hello data!', action.data);
    //       client.emit('action', {type: 'message', data: 'good day!'});
    //     }
    //   });
    //   client.on('wtf', (data) => {
    //     this.log.info(data);
    //     client.emit('action', {type: 'message', data: 'good day!'});
    //   });
    //   // setInterval(() => {
    //   //   client.emit('action', {type: 'message', data: 'good day!'});
    //   //   this.log.info('EMIT');
    //   // }, 1000);
    // });
    this.log.info('App is ready!');
  }

}
