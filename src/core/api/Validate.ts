/**
 * core.api.Validate
 * ------------------------------------------------
 *
 * Those annotations are used to simplify the use of request (payload)
 * validation. The '@Request(RequestBodyClass)' annotation defines the
 * the validation rules with his parameter and the '@Validate' runs all
 * the given validation classes.
 */
// import {
//   validate
// } from 'class-validator';
import { transformAndValidate } from 'class-transformer-validator';
import 'reflect-metadata';
import {
  RequestBody,
} from './RequestBody';

import {
  ValidationException,
} from '../../api/exceptions/ValidationException';

const requestMetadataKey = Symbol('ValidateRequest');

interface RequestParameter {
  request: typeof RequestBody;
  index: number;
}

/**
 * Request annotation marks the parameters, which should be validated as a RequestBody.
 *
 * @param request
 */
export const Request = (requestBody: typeof RequestBody) => (target: object, propertyKey: string | symbol, parameterIndex: number): any => {
  const existingRequestParameters: RequestParameter[] = Reflect.getOwnMetadata(requestMetadataKey, target, propertyKey) || [];
  existingRequestParameters.push({
    request: requestBody,
    index: parameterIndex,
  });
  Reflect.defineMetadata(requestMetadataKey, existingRequestParameters, target, propertyKey);
};

/**
 * Validate annotation builds the given RequestBodies and validates them
 *
 * @param target
 * @param propertyName
 * @param descriptor
 */
export const Validate = () => (target: any, propertyName: string, descriptor: TypedPropertyDescriptor < any > ): any => {
  const method = descriptor.value;
  descriptor.value = async function(...args: any[]): Promise < any > {
    try {
      const requestParameters: RequestParameter[] = Reflect.getOwnMetadata(requestMetadataKey, target, propertyName);
      if (requestParameters.length > 0) {
        for (const requestParameter of requestParameters) {
          const requestBody = new requestParameter.request(args[requestParameter.index]);
          await transformAndValidate(requestParameter.request, requestBody);
        }
      }
      return method && method.apply(this, args);
    } catch (errors) {
      if (errors && errors.length > 0) {
        throw new ValidationException('Request body is not valid', errors);
      }
    }
  };

  return descriptor;
};

export const ValidateMerchantRequest = () => (target: any, propertyName: string, descriptor: TypedPropertyDescriptor < any > ): any => {
  return null;
};
