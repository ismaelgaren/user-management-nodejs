/**
 * core.Server
 * ------------------------------------
 *
 * The Server class is responsible to listen to http server
 * events and to react to it.
 */
import * as express from 'express';
import * as http from 'http';
import * as mqtt from 'mqtt';
import * as WebSocket from 'ws';
// import {
//   ApiInfo,
// } from './ApiInfo';
// import {
//   Environment,
// } from './helpers/Environment';
import {
  Logger,
} from './Logger';

export class Server {

  /**
   * Normalize port for the express application
   *
   * @param {string} port
   * @returns {(number | string | boolean)}
   *
   * @memberof Server
   */
  public static client: any = mqtt.connect('tcp://165.227.177.91:1883', {clientId: `${process.env.QUEUE_CLIENT_ID}`});
  public static clients = {};
  // public static client: any = mqtt.connect('https://165.227.177.91:1883', {clientId: `${process.env.QUEUE_CLIENT_ID}`});
  public static normalizePort(port: number): number | string | boolean {
    const parsedPort = port;
    if (isNaN(parsedPort)) { // named pipe
      return port;
    }
    if (parsedPort >= 0) { // port number
      return parsedPort;
    }
    return false;
  }

  private log = new Logger(__filename);

  constructor(public httpServer: http.Server) {}

  /**
   * Listen to the given http server
   *
   * @param {http.Server} httpServer
   * @param {express.Application} app
   *
   * @memberof Server
   */
  public use(app: express.Application): void {
    try {
      this.httpServer.on('listening', () => {
        this.onStartUp(app);
      });
      this.httpServer.on('error', (error) => {
        this.onError(error);
      });
    } catch (err) {
      // err
    }
  }

  public static startWSServer(): void {
    // initialize the WebSocket server instance
    const wss = new WebSocket.Server({ port: 8550});
    wss.on('connection', (ws: WebSocket) => {
      ws.on('message', (message: any) => {
        console.log('CLIENTS', this.clients);
        const obj = JSON.parse(message);
        console.log('MESSAGE: ', obj);
        if (obj.id) {
          this.clients[`${obj.id}`] = ws;
        } else if(obj.to) {
          const client: WebSocket = this.clients[`${obj.to}`];
          console.log('TARGET CLIENT', client);
          if (client) {
            client.send(message);
          }
        }
      });
    });
  }


  /**
   * This is called when the server has started and is ready.
   *
   *
   * @memberof Server
   */
  public async onStartUp(app: express.Application): Promise<void> {
    // startup
    // await MqttService.getInstance().connectMqtt();
  }



  /**
   * This is called when the server throws an error like the given
   * port is already used
   *
   * @param {*} error
   *
   * @memberof Server
   */
  public onError(error: any): void {
    if (error.syscall !== 'listen') {
      throw error;
    }
    switch (error.code) {
      case 'EACCES':
        this.log.error(`The Server requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        this.log.error(`Port is already in use or blocked by the os`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

}
